FROM python:3.7

RUN groupadd -r fastapi && useradd -r -g fastapi fastapi
COPY requirements.txt requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip \
    -i https://mirrors.aliyun.com/pypi/simple/
RUN pip install -i https://mirrors.aliyun.com/pypi/simple/ \
    --trusted-host mirrors.aliyun.com \
    -r requirements.txt
COPY ./docs /docs
COPY ./docs_src /docs_src
RUN chown -R fastapi:fastapi /docs
RUN chown -R fastapi:fastapi /docs_src
WORKDIR /docs/en
USER fastapi

CMD [ "mkdocs", "serve", "-a", "0.0.0.0:8000" ]
