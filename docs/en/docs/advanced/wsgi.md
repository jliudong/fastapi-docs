# 包括 WSGI-Flask，Django等

您可以使用[子应用程序-代理后面，挂载](./sub-applications.md){.internal-link target=_blank}来挂载WSGI应用程序。

为此，您可以使用 `WSGIMiddleware` 并将其包装到WSGI应用程序中，例如Flask，Django等。

## 使用 `WSGIMiddleware`

您需要导入`WSGIMiddleware`。

然后将WSGI（例如Flask）应用与中间件包装在一起。

然后将其安装在路径下。

```Python hl_lines="2 3  22"
{!../../../docs_src/wsgi/tutorial001.py!}
```

## 查看结果

现在，Flask应用程序将处理路径 `/v1/` 下的每个请求。

其余的将由 **FastAPI** 处理。

如果您使用Uvicorn运行它，然后转到 <a href="http://localhost:8000/v1/" class="external-link" target="_blank">http://localhost:8000/v1/</a> 您将看到Flask的回复：

```txt
Hello, World from Flask!
```

如果您转到<a href="http://localhost:8000/v2" class="external-link" target="_blank">http://localhost:8000/v2</a>，则会看到 FastAPI的响应：

```JSON
{
    "message": "Hello World"
}
```
