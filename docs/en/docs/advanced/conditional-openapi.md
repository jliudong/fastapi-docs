# 有条件的OpenAPI

如果需要，可以根据环境使用设置和环境变量来有条件地配置OpenAPI，甚至完全禁用它。

## 关于安全性，API和文档

在生产环境中隐藏文档用户界面*不应*是保护API的方法。

这不会为您的API添加任何额外的安全性，*path operations* 仍将在可用的地方可用。

如果您的代码中存在安全漏洞，则该漏洞仍然存在。

隐藏文档只会使您更加难以理解如何与您的API交互，并且可能会使您更难以在生产环境中对其进行调试。 可以将其简单地视为<a href="https://en.wikipedia.org/wiki/Security_through_obscurity" class="external-link" target="_blank">通过隐蔽性实现安全性</a>的形式。

如果您想保护自己的API，可以做一些更好的事情，例如：

* 确保为请求正文和响应定义了明确的Pydantic模型。
* 使用依赖项配置任何必需的权限和角色。
* 切勿存储纯文本密码，仅存储密码哈希。
* 实现并使用众所周知的加密工具，例如Passlib和JWT令牌等。
* 在需要的地方使用OAuth2范围添加更多粒度权限控件。
* ...等

但是，您可能有一个非常特定的用例，在该情况下，您确实需要针对某些环境（例如，用于生产环境）或根据环境变量的配置来禁用API文档。

## 来自设置和环境变量的条件OpenAPI

您可以轻松地使用相同的Pydantic设置来配置生成的OpenAPI和docs UI。

例如：

```Python hl_lines="6  11"
{!../../../docs_src/conditional_openapi/tutorial001.py!}
```

在这里，我们声明设置 `openapi_url`，其默认值为 `/openapi.json`。

然后我们在创建 `FastAPI` 应用程序时使用它。

然后，您可以通过将环境变量 `OPENAPI_URL` 设置为空字符串来禁用OpenAPI（包括UI文档），例如：

<div class="termy">

```console
$ OPENAPI_URL= uvicorn main:app

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

然后，如果您转到 `/openapi.json`，`/docs` 或 `/redoc` 的URL，您将仅收到 `404 Not Found` 错误，例如：

```JSON
{
    "detail": "Not Found"
}
```
