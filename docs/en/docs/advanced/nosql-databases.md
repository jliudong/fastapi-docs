# NoSQL（分布式/大数据）数据库

**FastAPI** 还可以与任何 <abbr title="分布式数据库 (大数据), 也称 'Not Only SQL'">NoSQL</abbr>集成。

在这里，我们将看到一个使用 **<a href="https://www.couchbase.com/" class="external-link" target="_blank"> Couchbase </a>**（<abbr title =“此处的文档是指具有键和值的JSON对象（字典），这些值也可以是其他JSON对象，数组（列表），数字，字符串，布尔值等。”>文档</abbr> 基于NoSQL数据库。

您可以使其适应任何其他NoSQL数据库，例如：

* **MongoDB**
* **Cassandra**
* **CouchDB**
* **ArangoDB**
* **ElasticSearch**, 等.

!!! 提示
    有一个官方的项目生成器，带有 **FastAPI** 和 **Couchbase**，它们都基于 **Docker**，包括一个前端和更多工具： <a href="https://github.com/tiangolo/full-stack-fastapi-couchbase" class="external-link" target="_blank">https://github.com/tiangolo/full-stack-fastapi-couchbase</a>

## 导入Couchbase组件

现在，仅关注进口：

```Python hl_lines="6 7 8"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

## 定义一个常量，用作“文档类型”

稍后我们将在文档中将其用作固定字段 `type`。

Couchbase不需要此操作，但是这是一个很好的实践，它将对您有所帮助。

```Python hl_lines="10"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

## 添加一个函数来获取 `Bucket`

在 **Couchbase** 中，存储桶是一组文档，可以是不同的类型。

它们通常都与同一应用程序相关。

关系数据库世界中的类比将是“数据库”（特定的数据库，而不是数据库服务器）。

**MongoDB** 中的类比将是一个“集合”。

在代码中，`Bucket` 代表与数据库通信的主要入口点。

该实用程序功能将：

* 连接到 **Couchbase** 部署（可能是一台计算机）。
    * 为超时设置默认值。
* 在进行中进行身份验证。
* 获取一个 `Bucket` 实例。
    * 为超时设置默认值。
* 把它返还。

```Python hl_lines="13 14 15 16 17 18 19 20 21 22"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

## 创建Pydantic模型

由于 **Couchbase** “文档”实际上只是 “JSON对象”，因此我们可以使用Pydantic对其进行建模。

### 用户模型

首先，让我们创建一个 `User` 模型：

```Python hl_lines="25 26 27 28 29"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

我们将在 *path operation function* 中使用此模型，因此，我们不包含 `hashed_password`。

### `UserInDB` 模型

现在，让我们创建一个UserInDB模型。

这将具有实际存储在数据库中的数据。

我们不是将其创建为Pydantic的 `BaseModel` 的子类，而是将其创建为我们自己的 `User` 的子类，因为它将具有 `User` 的所有属性以及其他一些属性：

```Python hl_lines="32 33 34"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

!!! 注意
    注意，我们有一个 `hashed_password` 和一个 `type` 字段将被存储在数据库中。

    但这不是普通的 `User` 模型的一部分（我们将在 *path operation* 中返回该模型）。

## 获取用户

现在创建一个函数，该函数将：

* 输入用户名。
* 从中生成文档ID。
* 获取具有该ID的文档。
* 将文档内容放入 `UserInDB` 模型中。

通过创建一个仅用于从 `username` （或任何其他参数）获取用户的函数，而与 *path operation function* 无关，则可以更轻松地在多个部分重复使用它，并添加<abbr title="用代码编写的自动测试，用于检查另一段代码是否正常工作。">对其进行单元测试</abbr>：

```Python hl_lines="37 38 39 40 41 42 43"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

### f-strings

如果您不熟悉 `f"userprofile::{username}"` ，它是Python“<a href="https://docs.python.org/3/glossary.html#term-f-string" class="external-link" target="_blank">f-string</a>”。

字符串中放在 `{}` 里面的任何变量都将被扩展/注入到字符串中。

### `dict` 开箱

如果您不熟悉 `UserInDB(**result.value)`，<a href="https://docs.python.org/3/glossary.html#term-argument" class="external-link" target="_blank">使用 `dict` "unpacking"</a>。

它将在 `result.value` 处获取 `dict` ，并获取其每个键和值，并将它们作为键值传递给 `UserInDB` 作为关键字参数。

因此，如果 `dict` 包含：

```Python
{
    "username": "johndoe",
    "hashed_password": "some_hash",
}
```

它将通过以下方式传递给 `UserInDB`：

```Python
UserInDB(username="johndoe", hashed_password="some_hash")
```

## 创建您的 **FastAPI** 代码

### 创建 `FastAPI` 应用

```Python hl_lines="47"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

### 创建 *path operation function*

由于我们的代码正在调用Couchbase，因此我们没有使用<a href="https://docs.couchbase.com/python-sdk/2.5/async-programming.html#asyncio-python-3-5" class="external-link" target="_blank">实验性的Python <code>await</code>支持</a>，我们应该使用普通的 `def` 而不是 `async def` 声明函数。

另外，Couchbase建议不要在 <abbr title="程序正在执行一系列代码，而同时或间隔执行，可能还会执行其他代码。">多线程程序中</ abbr> 使用单一 `Bucket`，因此，我们可以直接获取存储桶并将其传递给我们的实用程序函数：

```Python hl_lines="50 51 52 53 54"
{!../../../docs_src/nosql_databases/tutorial001.py!}
```

## 回顾

您可以使用其标准软件包集成任何第三方NoSQL数据库。

这同样适用于任何其他外部工具，系统或API。
