# 异步 SQL (关系型) 数据库

您还可以使用 **FastAPI** <a href="https://github.com/encode/databases" class="external-link" target="_blank">`encode/databases`</a> 通过`async` 和 `await` 方式连接数据库。

兼容以下数据库：

* PostgreSQL
* MySQL
* SQLite

在此示例中，我们将使用 **SQLite**，因为它使用单个文件并且Python集成了支持。 因此，您可以复制此示例并按原样运行它。

稍后，对于您的生产应用程序，您可能想要使用 **PostgreSQL** 之类的数据库服务器。

!!! 提示
    您可以采用有关SQLAlchemy ORM（[SQL（关系）数据库](../tutorial/sql-databases.md){.internal-link target=_blank})这一节的想法，例如使用实用程序函数在 数据库，与您的 **FastAPI** 代码无关。

    本部分不应用这些建议，等同于<a href="https://www.starlette.io/database/" class="external-link" target="_blank"> Starlette </a>。

## 导入并设置 `SQLAlchemy`

* 导入SQLAlchemy。
* 创建一个 `metadata` 对象。
* 使用 `metadata` 对象创建表 `notes` 。

```Python hl_lines="4 14 16 17 18 19 20 21 22"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

!!! 提示
    请注意，所有这些代码都是纯SQLAlchemy Core。

    `databases` 在这里还没有做任何事情。

## 导入并设置“数据库”

* 导入 `databases`。
* 创建一个 `DATABASE_URL`。
* 创建一个 `database` 对象。

```Python hl_lines="3 9 12"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

!!! 提示
    如果您要连接到其他数据库（例如PostgreSQL），则需要更改 `DATABASE_URL`。

## 创建表

在这种情况下，我们将在同一Python文件中创建表，但是在生产中，您可能希望使用Alembic创建它们，并与迁移集成等。

在这里，此部分将在启动 **FastAPI** 应用程序之前直接运行。

* 创建一个 `engine`。
* 从 `metadata` 对象创建所有表。

```Python hl_lines="25 26 27 28"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

## 创建模型

创建Pydantic模型用于：

*要创建的注释（`NoteIn`）。
*要返回的便笺（`Note`）。

```Python hl_lines="31 32 33 36 37 38 39"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

通过创建这些Pydantic模型，将对输入数据进行验证，序列化（转换）和注释（记录）。

因此，您将能够在交互式API文档中看到全部内容。

##连接和断开连接

*创建您的 `FastAPI` 应用程序。
*创建事件处理程序以连接数据库并与数据库断开连接。

```Python hl_lines="42 45 46 47 50 51 52"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

##阅读笔记

创建 *path operation function* 以读取注释：

```Python hl_lines="55 56 57 58"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

!!! 注意
    注意，当我们使用 `await` 与数据库通信时，*path operation function* 是用 `async` 声明的。

### 注意 `response_model=List[Note]`

它使用 `typing.List`。

该文档将输出数据记录（并验证，序列化，过滤），作为 `Note` 的 `list`。

##创建笔记

创建 *path operation function* 以创建注释：

```Python hl_lines="61 62 63 64 65"
{!../../../docs_src/async_sql_databases/tutorial001.py!}
```

!!! 注意
    注意，当我们使用 `await` 与数据库通信时，*path operation function* 是用 `async` 声明的。

### 关于 `{**note.dict(), "id": last_record_id}`

`note` 是一个Pydantic `Note` 对象。

`note.dict()` 返回一个 `dict` 及其数据，类似于：

```Python
{
    "text": "Some note",
    "completed": False,
}
```

但没有 `id` 字段。

因此，我们创建了一个新的 `dict` ，其中包含来自 `note.dict()` 的键值对，其中包括：

```Python
{**note.dict()}
```

`**note.dict()` 直接“解包”键值对，因此，`{**note.dict()}` 或多或少是 `note.dict()` 的副本。

然后，我们扩展该副本dict，添加另一个键值对：`"id": last_record_id` ：

```Python
{**note.dict(), "id": last_record_id}
```

因此，返回的最终结果将类似于：

```Python
{
    "id": 1,
    "text": "Some note",
    "completed": False,
}
```

## 检查结果

您可以按原样复制此代码，并在<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a> 查看。

在这里，您可以看到所有API记录在案并与之交互：

<img src="/img/tutorial/async-sql-databases/image01.png">

## 获取更多信息

您可以从以下地址获取更多信息You can read more about <a href="https://github.com/encode/databases" class="external-link" target="_blank">`encode/databases` Github page上</a>。
