# 测试事件：启动-关闭

当您需要事件处理程序（`startup` 和 `shutdown`）在测试中运行时，可以将 `TestClient` 和 `with` 语句一起使用：

```Python hl_lines="9 10 11 12 20 21 22 23 24"
{!../../../docs_src/app_testing/tutorial003.py!}
```
