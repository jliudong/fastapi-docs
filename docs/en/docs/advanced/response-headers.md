# 响应 Headers

## 使用 `Response` 参数

您可以在 *path operation function* 中声明类型为 `Response` 的参数（就像对cookie一样）。

然后，您可以在该 *temporal* 响应对象中设置header。

```Python hl_lines="1  7 8"
{!../../../docs_src/response_headers/tutorial002.py!}
```

然后，您可以像往常一样返回所需的任何对象（`dict`，数据库模型等）。

而且，如果您声明了 `response_model`，它仍将用于过滤和转换您返回的对象。

**FastAPI** 将使用该 *temporal* 响应来提取header（还包括cookie和状态代码），并将它们放入包含您返回的值的最终响应中，并通过任何 `response_model` 进行过滤。

您还可以在依赖项中声明 `Response` 参数，并在依赖项中设置header（和cookie）。

## 直接返回 `Response`

当您直接返回 `Response` 时，您也可以添加标题。

按照[直接返回响应](response-directly.md){.internal-link target=_blank}中所述创建响应，并将标头作为附加参数传递：

```Python hl_lines="10 11 12"
{!../../../docs_src/response_headers/tutorial001.py!}
```

!!! 请注意“技术细节”
    您也可以使用 `from from starlette.responses import Response` 或 `from from starlette.responses import JSONResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。

    由于 `Response` 可经常用于设置header和cookie，**FastAPI** 也可在 `fastapi.Response` 中提供它。

## 自定义标题

请记住，可以添加<a href="https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers" class="external-link" target="_blank"> 使用“X-”前缀</a>。

但是，如果您希望浏览器中的客户端能够看到自定义标头，则需要将其添加到CORS配置（在[CORS（跨源资源共享）]中了解更多信息(../tutorial/cors.md){.internal-link target=_blank}），使用<a href="https://www.starlette.io/middleware/#corsmiddleware" class="external-link" target="_blank">Starlette's CORS docs</a>。
