# 事件：启动 - 关闭

您可以定义在应用程序启动之前或在应用程序关闭时需要执行的事件处理程序（函数）。

这些函数可以用 `async def` 或普通的 `def` 声明。

!!! 警告
    仅执行主应用程序的事件处理程序，而不执行[子应用程序-装载](./sub-applications.md){.internal-link target=_blank}的事件处理程序。

## `startup` 事件

要添加应在应用程序启动之前运行的功能，请使用事件 `"startup"` 对其进行声明：

```Python hl_lines="8"
{!../../../docs_src/events/tutorial001.py!}
```

在这种情况下，`startup` 事件处理程序函数将使用一些值初始化“数据库”项（仅是 `dict`）。

您可以添加多个事件处理函数。

在所有 `startup` 事件处理程序完成之前，您的应用程序将不会开始接收请求。

## `shutdown` 事件

要添加应在应用程序关闭时运行的功能，请使用事件 `shutdown` 声明它：

```Python hl_lines="6"
{!../../../docs_src/events/tutorial002.py!}
```

在这里，`shutdown` 事件处理函数将把文本行 `"Application shutdown"` 写到文件 `log.txt` 中。

!!! 信息
    在`open()`函数中，`mode="a"` 表示“append”，因此，该行将在该文件上的内容之后添加，而不会覆盖先前的内容。

!!! 提示
    注意，在这种情况下，我们使用了与文件交互的标准Python `open()`函数。

    因此，它涉及I/O（输入/输出），它需要“等待”将事物写入磁盘。

    但是 `open()` 不使用 `async` 和 `await`。

    因此，我们使用标准的 `def` 而不是 `async def` 声明事件处理函数。

!!! 信息
    您可以在下面的链接文档读到更多关于事件句柄的内容 <a href="https://www.starlette.io/events/" class="external-link" target="_blank">Starlette's  Events' docs</a>。
