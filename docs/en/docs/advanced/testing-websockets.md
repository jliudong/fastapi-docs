# 测试WebSockets

您可以使用相同的 `TestClient` 来测试WebSocket。

为此，可以在 `with` 语句中使用 `TestClient`，并连接到WebSocket：

```Python hl_lines="27 28 29 30 31"
{!../../../docs_src/app_testing/tutorial002.py!}
```
