# 响应 Cookies

## 使用 `Response` 参数

您可以在 *path operation function* 中声明类型为 `Response` 的参数。

然后，您可以在该 *temporal* 响应对象中设置cookie。

```Python hl_lines="1  8 9"
{!../../../docs_src/response_cookies/tutorial002.py!}
```

然后，您可以像往常一样返回所需的任何对象（`dict`，数据库模型等）。

而且，如果您声明了 `response_model`，它仍将用于过滤和转换您返回的对象。

**FastAPI** 将使用该 *temporal* 响应来提取cookie（还包括标头和状态代码），并将其放入包含您返回的值的最终响应中，并通过任何 `response_model` 进行过滤。

您还可以在依赖项中声明 `Response` 参数，并在依赖项中设置Cookie（和标头）。

## 直接返回 `Response`

您也可以在代码中直接返回 `Response` 时创建Cookie。

为此，您可以按照[直接返回响应](response-directly.md){.internal-link target=_blank}中所述创建响应。

然后在其中设置Cookies，然后将其返回：

```Python hl_lines="10 11 12"
{!../../../docs_src/response_cookies/tutorial001.py!}
```

!!! 提示
    请记住，如果您直接返回响应而不是使用 `Response` 参数，则FastAPI将直接返回响应。

    因此，您将必须确保数据类型正确。例如，如果您要返回 `JSONResponse`，则它与JSON兼容。

    同样，您没有发送任何应该由 `response_model` 过滤的数据。

### 更多信息

!!! 请注意“技术细节”
    您也可以使用 `from starlette.responses import Response` 或 `from starlette.responses import JSONResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。

    由于 `Response` 可经常用于设置header和cookie，**FastAPI** 也可在 `fastapi.Response` 中提供它。

要查看所有可用的参数和选项，请查看Starlette中的<a href="https://www.starlette.io/responses/#set-cookie" class="external-link" target="_blank">文档</a>。
