# 直接返回响应

创建 **FastAPI** *path operation*时，通常可以从中返回任何数据：一个 `dict`, 一个 `list`，Pydantic模型，数据库模型等。

默认情况下，**FastAPI** 将使用[JSON兼容编码器]（(../tutorial/encoder.md){.internal-link target=_blank}中所述的 `jsonable_encoder` 将返回值自动转换为JSON。

然后，在幕后，它将与JSON兼容的数据（例如 `dict` ）放入 `JSONResponse` 内部，该数据将用于向客户端发送响应。

但是您可以直接从 *path operations* 返回 `JSONResponse`。

例如，返回自定义标题或cookie可能很有用。

## 返回一个 `Response`

实际上，您可以返回任何 `Response` 或其任何子类。

!!! 提示
    `JSONResponse` 本身是 `Response` 的子类。

当您返回 `Response` 时，**FastAPI** 将直接传递它。

它不会使用Pydantic模型进行任何数据转换，也不会将内容转换为任何类型，等等。

这给您很大的灵活性。 您可以返回任何数据类型，覆盖任何数据声明或验证等。

## 在 `Response` 中使用 `jsonable_encoder`

由于 **FastAPI** 不会对您返回的 `Response` 进行任何更改，因此您必须确保其内容已准备就绪。

例如，您不能将Pydantic模型放在 `JSONResponse` 中，而不先将其转换为所有数据类型（如`datetime`, `UUID` 等）都转换为JSON兼容类型的 `dict`。

在这种情况下，您可以使用 `jsonable_encoder` 来转换数据，然后再将其传递给响应：

```Python hl_lines="4 6 20 21"
{!../../../docs_src/response_directly/tutorial001.py!}
```

!!! 请注意“技术细节”
    您还可以使用 `from starlette.responses import JSONResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。

## 返回自定义 `Response`

上面的示例显示了您需要的所有部分，但还不是很有用，因为您可以直接返回 `item`，而 **FastAPI**会为您将其放入 `JSONResponse` 中，将其转换为 `dict` 等。默认情况下都是如此。

现在，让我们看看如何使用它来返回自定义响应。

假设您要返回<a href="https://en.wikipedia.org/wiki/XML" class="external-link" target="_blank"> XML </a>响应。

您可以将您的XML内容放在字符串中，放在 `Response` 中，然后返回：

```Python hl_lines="1  18"
{!../../../docs_src/response_directly/tutorial002.py!}
```

## 注意

当您直接返回 `Response` 时，其数据不会被验证，转换（序列化）或自动记录。

但是您仍然可以按照 [OpenAPI中的其他响应](additional-responses.md){.internal-link target=_blank} 中的说明对其进行记录。

您可以在后面的部分中看到如何使用/声明这些自定义的 `Response` ，同时仍具有自动数据转换，文档等功能。
