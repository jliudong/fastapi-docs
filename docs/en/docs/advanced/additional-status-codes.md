# 附加状态码

默认情况下，**FastAPI** 将使用 `JSONResponse` 返回响应，将从 *path operation* 返回的内容放在 `JSONResponse` 中。

它将使用默认状态代码或您在 *path operation* 中设置的代码。

## 附加状态码

如果要返回除主代码之外的其他状态代码，可以通过直接返回 `Response`（如 `JSONResponse` ）并直接设置其他状态代码来实现。

例如，假设您想要一个 *path operation*，该操作可以更新项目，并在成功时返回HTTP状态代码200 “OK”。

但是您也希望它接受新项目。 并且当这些项目以前不存在时，它将创建它们，并返回HTTP状态代码201 “已创建”。

为此，导入 `JSONResponse` ，然后直接返回您的内容，并设置所需的 `status_code`：

```Python hl_lines="2  19"
{!../../../docs_src/additional_status_codes/tutorial001.py!}
```

!!! 警告
    如上例所示，当您直接返回 `Response` 时，它将直接返回。

    不会与模型等序列化。
    
    确保它具有所需的数据，并且值是有效的JSON（如果使用的是 `JSONResponse` ）。

!!! 请注意“技术细节”
    您还可以使用 `from starlette.responses import JSONResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。 与 `status` 相同。

## OpenAPI和API文档

如果您直接返回其他状态代码和响应，它们将不会包含在OpenAPI架构（API文档）中，因为FastAPI无法事先知道要返回的内容。

但是您可以使用以下代码在代码中进行记录：[其他响应](additional-responses.md){.internal-link target=_blank}。
