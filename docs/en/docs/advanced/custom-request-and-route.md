# 自定义请求和APIRoute类

在某些情况下，您可能想覆盖 `Request` 和 `APIRoute` 类使用的逻辑。

特别是，这可能是中间件中逻辑的一个很好的选择。

例如，如果您想在应用程序处理请求主体之前读取或操纵它。

!!! 危险
    这是“高级”功能。

    如果您只是刚开始接触 **FastAPI**，则可能要跳过此部分。

## 用例

一些用例包括：

* 将非JSON请求正文转换为JSON（例如<a href="https://msgpack.org/index.html" class="external-link" target="_blank">`msgpack` </a>）。
* 解压缩gzip压缩的请求正文。
* 自动记录所有请求正文。

## 处理自定义请求体编码

让我们看看如何利用自定义的 `Request` 子类解压缩gzip请求。

还有一个 `APIRoute` 子类来使用该自定义请求类。

### 创建一个自定义的 `GzipRequest` 类

!!! 提示
    这是一个演示如何工作的玩具示例，如果需要Gzip支持，则可以使用提供的[`GzipMiddleware`](./middleware.md#gzipmiddleware){.internal-link target=_blank}。

首先，我们创建一个 `GzipRequest` 类，该类将覆盖 `Request.body()` 方法以在存在适当的标头的情况下对主体进行解压缩。

如果标题中没有 `gzip`，它将不会尝试解压缩主体。

这样，同一路由类可以处理gzip压缩或未压缩的请求。

```Python hl_lines="8 9 10 11 12 13 14 15"
{!../../../docs_src/custom_request_and_route/tutorial001.py!}
```

### 创建一个自定义的 `GzipRoute` 类

接下来，我们将创建一个自定义子类 `fastapi.routing.APIRoute`，它将使用 `GzipRequest`。

这次，它将覆盖方法 `APIRoute.get_route_handler()`。

此方法返回一个函数。 该函数将接收请求并返回响应。

在这里，我们使用它从原始请求创建一个 `GzipRequest`。

```Python hl_lines="18 19 20 21 22 23 24 25 26"
{!../../../docs_src/custom_request_and_route/tutorial001.py!}
```

!!! 请注意“技术细节”
    一个 `Request` 具有一个 `request.scope` 属性，它只是一个Python `dict`，包含与请求相关的元数据。

    一个 `Request` 也有一个 `request.receive`，这是一个“接收”请求体的功能。

    作用域 `dict` 和 `receive` 都是ASGI规范的一部分。

    创建 `Request` 实例所需要的是“ scope”和“ receive”这两件事。

    要了解有关 `Request` 的更多信息，请检查<a href="https://www.starlette.io/requests/" class="external-link" target="_blank"> Starlette关于请求的文档</a>。

由 `GzipRequest.get_route_handler` 返回的函数所做的唯一不同就是将 `Request` 转换为 `GzipRequest`。

这样做，我们的 `GzipRequest` 会在将数据传递给我们的* path操作*之前负责解压缩数据（如有必要）。

之后，所有处理逻辑都是相同的。

但是由于我们对 `GzipRequest.body` 的更改，当需要时，通过 **FastAPI** 加载请求正文时，请求正文将自动解压缩。

## 在异常处理程序中访问请求体

!!! 提示
    为了解决同样的问题，在自定义处理程序中将 `body` 用于 `RequestValidationError` 可能要容易得多([Handling Errors](../tutorial/handling-errors.md#use-the-requestvalidationerror-body){.internal-link target=_blank})。

    但是此示例仍然有效，并且显示了如何与内部组件进行交互。

我们还可以使用相同的方法在异常处理程序中访问请求正文。

我们要做的就是在 `try`/`except` 块内处理请求：

```Python hl_lines="13 15"
{!../../../docs_src/custom_request_and_route/tutorial002.py!}
```

如果发生异常，则 `Request` 实例仍将在范围内，因此我们可以在处理错误时阅读并使用请求正文：

```Python hl_lines="16 17 18"
{!../../../docs_src/custom_request_and_route/tutorial002.py!}
```

## 路由器中的自定义 `APIRoute` 类

您还可以设置 `APIRouter` 的 `route_class` 参数：

```Python hl_lines="26"
{!../../../docs_src/custom_request_and_route/tutorial003.py!}
```

在此示例中，`router` 下的 *path operations* 将使用自定义的 `TimedRoute` 类，并且在响应中将带有一个额外的 `X-Response-Time` 标头以及生成响应所花费的时间：

```Python hl_lines="13 14 15 16 17 18 19 20"
{!../../../docs_src/custom_request_and_route/tutorial003.py!}
```
