# 自定义响应 - HTML，流，文件等

默认情况下，**FastAPI** 将使用 `JSONResponse` 返回响应。

您可以通过直接返回 `Response` 来覆盖它，如[直接返回响应](response-directly.md){.internal-link target=_blank}所示。

但是，如果您直接返回 `Response`，则不会自动转换数据，也不会自动生成文档（例如，在HTTP标头 `Content-Type` 中包含特定的“媒体类型”，例如生成的OpenAPI的一部分）。

但是，您也可以在 *path operation decorator* 中声明要使用的 `Response`。

从 *path operation function* 返回的内容将放在 `Response` 中。

并且，如果 `Response` 具有JSON媒体类型（`application/json`），例如 `JSONResponse` 和 `UJSONResponse` 的情况，则您返回的数据将使用任何Pydantic `response_model` 自动进行转换（并过滤）您在 *path operation decorator* 中声明的。

!!! 注意
    如果您使用没有媒体类型的响应类，FastAPI将期望您的响应不包含任何内容，因此它将不会在其生成的OpenAPI文档中记录响应格式。

## 使用 `ORJSONResponse`

例如，如果要压缩性能，则可以安装并使用<a href="https://github.com/ijl/orjson" class="external-link" target="_blank"> `orjson` </a>并将响应设置为 `ORJSONResponse`。

导入要使用的 `Response` 类（子类），并在 *path operation decorator* 中声明它。

```Python hl_lines="2 7"
{!../../../docs_src/custom_response/tutorial001b.py!}
```

!!! 信息
    参数 `response_class` 也将用于定义响应的“媒体类型”。

    在这种情况下，HTTP header `Content-Type` 将被设置为 `application/json`。

    将在OpenAPI中进行记录。

!!! 提示
    目前，`ORJSONResponse` 仅在FastAPI中可用，而在Starlette中不可用。

## HTML Response

要直接从 **FastAPI** 返回带有HTML的响应，请使用 `HTMLResponse`。

* 导入 `HTMLResponse`。
* 传递 `HTMLResponse` 作为路径操作的参数 `content_type`。

```Python hl_lines="2 7"
{!../../../docs_src/custom_response/tutorial002.py!}
```

!!! 信息
    参数 `response_class` 也将用于定义响应的“媒体类型”。

    在这种情况下，HTTP header `Content-Type` 将被设置为 `text/html`。

    它将在OpenAPI中进行记录。

### 返回 `Response`

如[直接返回响应](response-directly.md){.internal-link target=_blank}所示，您还可以通过返回操作直接在 *path operation* 中覆盖响应。

上面的相同示例返回了 `HTMLResponse`，可能类似于：

```Python hl_lines="2 7 19"
{!../../../docs_src/custom_response/tutorial003.py!}
```

!!! 警告
    您的 *path operation function* 直接返回的 `Response` 不会在OpenAPI中记录（例如，不会记录 `Content-Type`），并且不会在自动交互式文档中显示。

!!! 信息
    当然，实际的 `Content-Type` header，状态码等将来自您返回的 `Response` 对象。

### OpenAPI中的文档并覆盖 `Response`

如果您想覆盖函数内部的响应，但同时在OpenAPI中记录“媒体类型”，则可以使用 `response_class` 参数并返回 `Response` 对象。

然后，`response_class` 将仅用于记录OpenAPI *path operation*，但是您的 `Response` 将被原样使用。

#### 直接返回 `HTMLResponse`

例如，可能是这样的：

```Python hl_lines="7 23 21"
{!../../../docs_src/custom_response/tutorial004.py!}
```

在此示例中，函数 `generate_html_response()` 已经生成并返回 `Response` ，而不是返回 `str` 中的HTML。

通过返回调用`generate_html_response()`的结果，您已经在返回一个 `Response`，它将覆盖默认的**FastAPI** 行为。

但是，当您也在 `response_class` 中通过 `HTMLResponse` 时，**FastAPI**也会知道如何在OpenAPI和交互式文档中使用 `text/html` 将其记录为HTML：

<img src="/img/tutorial/custom-response/image01.png">

## 可用响应

以下是一些可用的响应。

请记住，您可以使用 `Response` 返回其他任何内容，甚至创建自定义子类。

!!! 请注意“技术细节”
    您也可以使用 `from from starlette.responses import HTMLResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。

### `Response`

主`Response`类，所有其他响应都继承自它。

您可以直接将其退回。

它接受以下参数：

* `content` - 一个 `str` 或 `bytes`。
* `status_code` - 一个 `int` HTTP状态代码。
* `headers` - 字符串的 `dict`。
* `media_type` - 一个 `str` 给出媒体类型。 例如。 `"text/html"`。

FastAPI（实际上是Starlette）将自动包含Content-Length header。 它还将包括一个基于media_type的Content-Type header，并为文本类型附加一个字符集。

```Python hl_lines="1  18"
{!../../../docs_src/response_directly/tutorial002.py!}
```

### `HTMLResponse`

如上所读，获取一些文本或字节并返回HTML响应。

### `PlainTextResponse`

接受一些文本或字节并返回纯文本响应。

```Python hl_lines="2  7  9"
{!../../../docs_src/custom_response/tutorial005.py!}
```

### `JSONResponse`

获取一些数据并返回一个 `application/json` 编码的响应。

如上文所述，这是 **FastAPI** 中使用的默认响应。

### `ORJSONResponse`

如上文所述，使用 <a href="https://github.com/ijl/orjson" class="external-link" target="_blank">`orjson`</a> 的快速替代JSON响应。

### `UJSONResponse`

替代 JSON 响应使用 <a href="https://github.com/ultrajson/ultrajson" class="external-link" target="_blank">`ujson`</a>。

!!! 警告
    在处理某些边缘情况时，`ujson`不如Python的内置实现那么谨慎。

```Python hl_lines="2 7"
{!../../../docs_src/custom_response/tutorial001.py!}
```

!!! 提示
    `ORJSONResponse` 可能是一个更快的选择。

### `RedirectResponse`

返回HTTP重定向。 默认情况下，使用307状态代码（临时重定向）。

```Python hl_lines="2  9"
{!../../../docs_src/custom_response/tutorial006.py!}
```

### `StreamingResponse`

使用异步生成器或普通生成器/迭代器，然后流式传输响应主体。

```Python hl_lines="2  14"
{!../../../docs_src/custom_response/tutorial007.py!}
```

#### 对file-like的对象使用 `StreamingResponse`

如果您有file-like的对象（例如，由 `open()` 返回的对象），则可以在 `StreamingResponse` 中将其返回。

这包括许多与云存储，视频处理等交互的库。

```Python hl_lines="2  10 11"
{!../../../docs_src/custom_response/tutorial008.py!}
```

!!! 提示
    请注意，由于此处使用的是不支持 `async` 和 `await` 的标准 `open()`，因此我们使用普通的 `def` 声明了路径操作。

### `FileResponse`

异步传输文件作为响应。

与其他响应类型相比，采用不同的参数集进行实例化：

* `path` - 要流式传输的文件的文件路径。
* `headers` - 作为字典包含的所有自定义标题。
* `media_type` - 给出媒体类型的字符串。 如果未设置，则文件名或路径将用于推断媒体类型。
* `filename` - 如果设置，它将包含在响应 `Content-Disposition` 中。

文件响应将包括适当的`Content-Length`, `Last-Modified` 和 `ETag` header。

```Python hl_lines="2  10"
{!../../../docs_src/custom_response/tutorial009.py!}
```

## 默认响应类别

创建 **FastAPI** 类实例或 `APIRouter` 时，您可以指定默认使用哪个响应类。

定义它的参数是 `default_response_class`。

在以下示例中，默认情况下，**FastAPI** 将在所有 *path operations* 中使用 `ORJSONResponse`，而不是 `JSONResponse`。

```Python hl_lines="2 4"
{!../../../docs_src/custom_response/tutorial010.py!}
```

!!! 提示
    您仍然可以像以前一样在 *path operations* 中覆盖 `response_class`。

## 其他文档

您还可以使用 `responses` 在OpenAPI中声明媒体类型和许多其他详细信息：[OpenAPI中的其他响应](additional-responses.md){.internal-link target=_blank}。
