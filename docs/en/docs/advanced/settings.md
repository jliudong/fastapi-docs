# 设置和环境变量

在许多情况下，您的应用程序可能需要一些外部设置或配置，例如密钥，数据库凭据，电子邮件服务的凭据等。

这些设置大多数都是可变的（可以更改），例如数据库URL。 许多人可能会很敏感，例如秘密。

因此，通常在应用程序读取的环境变量中提供它们。

## 环境变量

!!! 提示
    如果您已经知道什么是“环境变量”以及如何使用它们，请随时跳到下面的下一部分。

<a href="https://en.wikipedia.org/wiki/Environment_variable" class="external-link" target="_blank">环境变量</a>（也称为"env var"）是一个 变量位于操作系统的Python代码之外，并且可以由Python代码（或其他程序）读取。

您可以在shell中创建和使用环境变量，而无需使用Python：

=== "Linux, macOS, Windows Bash"

    <div class="termy">

    ```console
    // You could create an env var MY_NAME with
    $ export MY_NAME="Wade Wilson"

    // Then you could use it with other programs, like
    $ echo "Hello $MY_NAME"

    Hello Wade Wilson
    ```

    </div>

=== "Windows PowerShell"

    <div class="termy">

    ```console
    // Create an env var MY_NAME
    $ $Env:MY_NAME = "Wade Wilson"

    // Use it with other programs, like
    $ echo "Hello $Env:MY_NAME"

    Hello Wade Wilson
    ```

    </div>

### 在Python中读取环境变量

您也可以在Python之外，在终端中（或使用任何其他方法）创建环境变量，然后在Python中读取它们。

例如，您可能具有以下文件 `main.py`：

```Python hl_lines="3"
import os

name = os.getenv("MY_NAME", "World")
print(f"Hello {name} from Python")
```

!!! 提示
    <a href="https://docs.python.org/3.8/library/os.html#os.getenv" class="external-link" target="_blank">`os.getenv()`</a>的第二个参数是要返回的默认值。

    如果未提供，则默认为 `None`，这里我们提供 `"World"` 作为默认值。

然后，您可以调用该Python程序：

<div class="termy">

```console
// Here we don't set the env var yet
$ python main.py

// As we didn't set the env var, we get the default value

Hello World from Python

// But if we create an environment variable first
$ export MY_NAME="Wade Wilson"

// And then call the program again
$ python main.py

// Now it can read the environment variable

Hello Wade Wilson from Python
```

</div>

由于环境变量可以在代码外部设置，但是可以被代码读取，并且不必与其余文件一起存储（提交到 `git`），因此通常将它们用于配置或设置 。

您也可以仅为特定的程序调用创建环境变量，该环境变量仅对该程序可用，并且仅在其持续时间内可用。

为此，请在程序本身之前的同一行上创建它：

<div class="termy">

```console
// Create an env var MY_NAME in line for this program call
$ MY_NAME="Wade Wilson" python main.py

// Now it can read the environment variable

Hello Wade Wilson from Python

// The env var no longer exists afterwards
$ python main.py

Hello World from Python
```

</div>

!!! 提示
    您可以在<a href="https://12factor.net/config" class="external-link" target="_blank">十二要素应用：配置</a>中了解更多信息。

### 类型和验证

这些环境变量只能处理文本字符串，因为它们在Python外部，并且必须与其他程序和系统的其余部分（甚至与其他操作系统，例如Linux，Windows，macOS）兼容。

这意味着在Python中从环境变量读取的任何值都将是一个 `str`，并且任何到其他类型的转换或验证都必须在代码中完成。

## Pydantic`Settings`

幸运的是，Pydantic提供了一个很棒的实用程序来处理来自环境变量的设置，这些设置使用<a href="https://pydantic-docs.helpmanual.io/usage/settings/" class="external-link" target="_blank">Pydantic: Settings management</a>。

### 创建 `Settings` 对象

从Pydantic导入 `BaseSettings` 并创建一个子类，非常类似于Pydantic模型。

与Pydantic模型相同，您可以使用类型注释和可能的默认值声明类属性。

您可以使用与Pydantic模型相同的所有验证功能和工具，例如不同的数据类型以及使用 `Field()` 进行的其他验证。

```Python hl_lines="2  5 6 7 8  11"
{!../../../docs_src/settings/tutorial001.py!}
```

!!! 提示
    如果要快速复制和粘贴某些内容，请不要使用此示例，请使用下面的最后一个示例。

然后，当您创建该 `Settings` 类的实例（在本例中为 `Settings` 对象）时，Pydantic将以不区分大小写的方式读取环境变量，因此，大写变量 `APP_NAME` 将 仍会读取属性 `app_name`。

接下来，它将转换并验证数据。 因此，当您使用该 `Settings` 对象时，您将拥有声明的类型的数据（例如，`items_per_user` 将是一个 `int`）。

### 使用 `settings`

然后，您可以在应用程序中使用新的 `settings` 对象：

```Python hl_lines="18 19 20"
{!../../../docs_src/settings/tutorial001.py!}
```

### 运行服务

接下来，您将运行服务器，将配置作为环境变量传递，例如，可以使用以下命令设置 `ADMIN_EMAIL` 和 `APP_NAME`：

<div class="termy">

```console
$ ADMIN_EMAIL="deadpool@example.com" APP_NAME="ChimichangApp" uvicorn main:app

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

!!! 提示
    要为单个命令设置多个env var，只需将它们用空格隔开，然后将其全部放在命令之前。

然后将 `admin_email` 设置设置为 `deadpool@example.com`。

`app_name` 将为 `"ChimichangApp"`。

并且 `items_per_user` 将保留其默认值 `50`。

##另一个模块中的设置

您可以将这些设置放在另一个模块文件中，就像在[更大的应用程序-多个文件](../tutorial/bigger-applications.md){.internal-link target=_blank}中看到的那样。

例如，您可能具有以下文件 `config.py`：

```Python
{!../../../docs_src/settings/app01/config.py!}
```

然后在文件 `main.py` 中使用它：

```Python hl_lines="3  11 12 13"
{!../../../docs_src/settings/app01/main.py!}
```

!!! 提示
    如[更大的应用程序-多个文件](../tutorial/bigger-applications.md){.internal-link target=_blank}上看到的，您还需要一个文件 `__init__.py`。

## 依赖项中的设置

在某些情况下，从依赖项中提供设置可能会有用，而不是使用在各处使用的带有 `settings` 的全局对象。

这在测试期间可能特别有用，因为使用您自己的自定义设置覆盖依赖项非常容易。

### 配置文件

来自前面的示例，您的 `config.py` 文件看起来像：

```Python hl_lines="10"
{!../../../docs_src/settings/app02/config.py!}
```

注意，现在我们不创建默认实例 `settings = Settings()`。

### 主应用程序文件

现在，我们创建一个依赖关系，该依赖关系返回一个新的 `config.Settings()`。

```Python hl_lines="5  11 12"
{!../../../docs_src/settings/app02/main.py!}
```

!!! 提示
    我们将讨论一下 `@lru_cache`。

     现在，您可以假设 `get_settings()` 是一个正常函数。

然后，我们可以从 *path operation function* 中将其作为依赖项，并在需要的任何地方使用它。

```Python hl_lines="16  18 19 20"
{!../../../docs_src/settings/app02/main.py!}
```

### 设置和测试

然后，通过为 `get_settings` 创建一个依赖覆盖将很容易在测试过程中提供一个不同的设置对象：

```Python hl_lines="8 9  12  21"
{!../../../docs_src/settings/app02/test_main.py!}
```

在依赖项覆盖中，我们在创建新的 `Settings` 对象时为 `admin_email` 设置一个新值，然后返回该新对象。

然后我们可以测试它是否被使用。

## 读取 `.env` 文件

如果您有许多可能在不同环境中发生很大变化的设置，则将它们放在文件中，然后像对待环境变量一样从文件中读取它们可能会很有用。

这种做法很常见，它有一个名字，这些环境变量通常放在文件 `.env` 中，该文件称为“dotenv”。

!!! 提示
    以点（`.`）开头的文件是类似Unix的系统（例如Linux和macOS）中的隐藏文件。

    但是dotenv文件实际上不必具有确切的文件名。

Pydantic支持使用外部库读取这些类型的文件。 您可以在<a href="https://pydantic-docs.helpmanual.io/usage/settings/#dotenv-env-support" class="external-link" target="_blank"> Pydantic设置中查看更多信息：Dotenv （.env）支持</a>。

!!! 提示
    为此，您需要 `pip install python-dotenv`。

### `.env` 文件

您可能有一个 `.env` 文件，其中包含：

```bash
ADMIN_EMAIL="deadpool@example.com"
APP_NAME="ChimichangApp"
```

### 从 `.env` 中读取设置

然后使用以下命令更新您的 `config.py`：

```Python hl_lines="9 10"
{!../../../docs_src/settings/app03/config.py!}
```

在这里，我们在Pydantic `Settings` 类内部创建一个 `Config` 类，并将 `env_file` 设置为带有我们要使用的dotenv文件的文件名。

!!! 提示
    `Config` 类仅用于Pydantic配置。 您可以在<a href="https://pydantic-docs.helpmanual.io/usage/model_config/" class="external-link" target="_blank"> Pydantic模型配置</a>中了解更多信息。

### 使用 `lru_cache` 仅创建一次 `Settings`

从磁盘读取文件通常是一项昂贵（缓慢）的操作，因此您可能只想执行一次，然后重新使用相同的设置对象，而不是针对每个请求都读取它。

但是每次我们这样做：

```Python
config.Settings()
```

将创建一个新的 `Settings` 对象，并在创建时再次读取 `.env` 文件。

如果依赖项函数如下所示：

```Python
def get_settings():
    return config.Settings()
```

我们将为每个请求创建该对象，然后将为每个请求读取 `.env` 文件。 ⚠️

但是，由于我们在顶部使用了 `@lru_cache()` 装饰器，因此，`Settings` 对象只会在第一次调用时创建一次。 ✔️

```Python hl_lines="1  10"
{!../../../docs_src/settings/app03/main.py!}
```

然后，对于下一个请求在依赖项中对 `get_settings()` 的任何后续调用，而不是执行 `get_settings()` 的内部代码并创建新的Settings对象，它将返回在上返回的相同对象。 第一次电话，一次又一次。

#### `lru_cache` 技术细节

`@lru_cache()` 修改它装饰的函数以返回与第一次返回的值相同的值，而不是再次对其进行计算，每次都执行该函数的代码。

因此，它下面的函数将针对每个参数组合执行一次。 然后，每当以完全相同的参数组合调用函数时，将一次又一次使用这些参数组合中的每一个返回的值。

例如，如果您有一个功能：

```Python
@lru_cache()
def say_hi(name: str, salutation: str = "Ms."):
    return f"Hello {salutation} {name}"
```

您的程序可以这样执行：

```mermaid
sequenceDiagram

participant code as Code
participant function as say_hi()
participant execute as Execute function

    rect rgba(0, 255, 0, .1)
        code ->> function: say_hi(name="Camila")
        function ->> execute: execute function code
        execute ->> code: return the result
    end

    rect rgba(0, 255, 255, .1)
        code ->> function: say_hi(name="Camila")
        function ->> code: return stored result
    end

    rect rgba(0, 255, 0, .1)
        code ->> function: say_hi(name="Rick")
        function ->> execute: execute function code
        execute ->> code: return the result
    end

    rect rgba(0, 255, 0, .1)
        code ->> function: say_hi(name="Rick", salutation="Mr.")
        function ->> execute: execute function code
        execute ->> code: return the result
    end

    rect rgba(0, 255, 255, .1)
        code ->> function: say_hi(name="Rick")
        function ->> code: return stored result
    end

    rect rgba(0, 255, 255, .1)
        code ->> function: say_hi(name="Camila")
        function ->> code: return stored result
    end
```

对于我们的依赖项 `get_settings()`，该函数甚至不接受任何参数，因此它始终返回相同的值。

这样，它的行为几乎就像只是一个全局变量。 但是由于它使用了依赖函数，因此我们可以轻松地对其进行覆盖以进行测试。

`@lru_cache()` 是 `functools` 的一部分，functools是Python的标准库的一部分，您可以在<a href="https://docs.python.org/3/library/functools.html#functools.lru_cache" class="external-link" target="_blank"> Python文档 `@lru_cache()`</a>。

## 总结

您可以使用Pydantic设置来利用Pydantic模型的所有功能来处理应用程序的设置或配置。

* 通过使用依赖项，可以简化测试。
* 您可以使用 `.env` 文件。
* 使用 `@lru_cache()` 可以避免为每个请求重复读取dotenv文件，同时允许您在测试期间覆盖它。
