# 直接使用请求

到目前为止，您已经在声明中要求了您需要的部分及其类型。

从以下位置获取数据：

* 路径参数
* Headers
* Cookies
* 等等

通过这样做，**FastAPI** 会验证该数据，对其进行转换并自动为您的API生成文档。

但是在某些情况下，您可能需要直接访问 `Request` 对象。

## 有关 `Request` 对象的详细信息

由于 **FastAPI** 实际上是 **Starlette**，其顶部是几个工具，因此您可以使用Starlette的<a href="https://www.starlette.io/requests/" class="external-link" target="_blank">`Request`</a>对象直接在需要时提供。

这也意味着，如果您直接从 `Request` 对象中获取数据（例如，读取正文），则FastAPI将不会对其进行验证，转换或记录（使用OpenAPI，用于自动API用户界面）。

尽管正常声明的任何其他参数（例如，具有Pydantic模型的物体）仍将被验证，转换，注释等。

但是在特定情况下，获取`Request`对象很有用。

## 直接使用 `Request` 对象

假设您想在 *path operation function* 中获取客户端的IP地址/主机。

为此，您需要直接访问请求。

```Python hl_lines="1  7 8"
{!../../../docs_src/using_request_directly/tutorial001.py!}
```

通过声明类型为 `Request` 的 *path operation function* 参数，**FastAPI** 将知道在该参数中传递 `Request` 。

!!! 提示
    请注意，在这种情况下，我们在request参数旁边声明了一个path参数。

    因此，路径参数将被提取，验证，转换为指定的类型并使用OpenAPI进行注释。

    同样，您可以像平常一样声明其他任何参数，此外，也可以获取 `Request`。

## `Request`文档

您可以在官方Starlette文档站点中阅读有关<a href="https://www.starlette.io/requests/" class="external-link" target="_blank">`Request`对象的更多详细信息</a>。

!!! 请注意“技术细节”
    您也可以使用 `from from starlette.requests import Request`。

    **FastAPI** 直接提供它，只是为开发人员提供方便。但它直接来自Starlette。
