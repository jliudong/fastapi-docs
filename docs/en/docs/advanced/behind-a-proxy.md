# 在代理后面

在某些情况下，您可能需要使用诸如Traefik或Nginx之类的 **proxy** ，并在配置中添加应用程序无法看到的额外路径前缀。

在这些情况下，您可以使用 `root_path` 来配置您的应用程序。

`root_path` 是ASGI规范提供的一种机制（FastAPI是通过Starlette构建的）。

`root_path` 用于处理这些特定情况。

并且在安装子应用程序时也在内部使用。

## 具有剥离路径前缀的代理

在这种情况下，使用具有剥离路径前缀的代理意味着您可以在代码中的 `/app` 处声明路径，但是随后，您需要在顶部添加一个层（代理），以放置您的 **FastAPI** 在 `/api/v1` 之类的路径下的应用程序。

在这种情况下，原始路径 `/app` 实际上将在 `/api/v1/app` 中提供。

即使您所有的代码都是在假设只有 `/app` 的情况下编写的。

而且，在将请求传输给Uvicorn之前，代理将在运行中 **"stripping"** **path prefix**，使您的应用程序确信它在 `/app` 服务，这样您就不必更新所有代码，使其包含前缀 `/api/v1`。

到这里，一切都会正常进行。

但是，当您打开集成的文档UI（前端）时，它将期望在 `/openapi.json` 而不是 `/api/v1/openapi.json`获得OpenAPI模式。

因此，前端（在浏览器中运行）将尝试访问 `/openapi.json` 并且无法获取OpenAPI架构。

因为我们的应用程序有一个路径前缀为 `/api/v1` 的代理，所以前端需要在 `/api/v1/openapi.json` 处获取OpenAPI模式。

```mermaid
graph LR

browser("Browser")
proxy["Proxy on http://0.0.0.0:9999/api/v1/app"]
server["Server on http://127.0.0.1:8000/app"]

browser --> proxy
proxy --> server
```

!!! 提示
    IP `0.0.0.0` 通常用于表示程序侦听该计算机/服务器中所有可用的IP。

docs UI还需要具有OpenAPI架构的JSON负载的路径定义为 `/api/v1/app`（在代理后面），而不是 `/app`。 例如，类似：

```JSON hl_lines="5"
{
    "openapi": "3.0.2",
    // More stuff here
    "paths": {
        "/api/v1/app": {
            // More stuff here
        }
    }
}
```

在此示例中，“代理”可能类似于 **Traefik**。 服务器将像 **Uvicorn** 一样运行您的FastAPI应用程序。

### 提供 `root_path`

为此，可以使用命令行选项 `--root-path` 像这样：

<div class="termy">

```console
$ uvicorn main:app --root-path /api/v1

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

如果您使用Hypercorn，它也有选项 `--root-path`。

!!! 请注意“技术细节”
    ASGI规范为此用例定义了一个 `root_path`。

    而 `--root-path` 命令行选项提供了该 `root_path`。

### 检查当前的 `root_path`

您可以获取应用程序为每个请求使用的当前 `root_path` ，它是 `scope` 字典的一部分（属于ASGI规范）。

在这里，我们将其包含在消息中只是出于演示目的。

```Python hl_lines="8"
{!../../../docs_src/behind_a_proxy/tutorial001.py!}
```

然后，如果您使用以下命令启动Uvicorn：

<div class="termy">

```console
$ uvicorn main:app --root-path /api/v1

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

响应将类似于：

```JSON
{
    "message": "Hello World",
    "root_path": "/api/v1"
}
```

### 在FastAPI应用中设置 `root_path`

另外，如果您无法提供命令行选项（如 `--root-path` 或等效选项），则可以在创建 `FastAPI` 应用时设置 `root_path` 参数：

```Python hl_lines="3"
{!../../../docs_src/behind_a_proxy/tutorial002.py!}
```

将 `root_path` 传递给 `FastAPI` 相当于将命令行选项 `--root-path` 传递给Uvicorn或Hypercorn。

### 关于 `root_path`

请记住，服务器（Uvicorn）不会将 `root_path` 用作传递给应用程序的任何其他方法。

但是，如果您使用浏览器访问<a href="http://127.0.0.1:8000" class="external-link" target="_blank"> http://127.0.0.1:8000/app</a>您将看到正常的响应：

```JSON
{
    "message": "Hello World",
    "root_path": "/api/v1"
}
```

因此，不会期望通过 `http://127.0.0.1:8000/api/v1/app` 访问该文件。

Uvicorn希望代理访问位于 `http://127.0.0.1:8000/app` 的Uvicorn，然后由代理负责在顶部添加额外的 `/api/v1` 前缀。

## 关于带有剥离路径前缀的代理

请记住，带剥离路径前缀的代理只是配置它的方法之一。

可能在许多情况下，默认设置是代理没有剥离路径前缀。

在这种情况下（没有剥离的路径前缀），代理将侦听类似 `https://myawesomeapp.com` 的内容，然后如果浏览器转到 `https://myawesomeapp.com/api/v1/app` 和您的服务器（例如Uvicorn）在 `http://127.0.0.1:8000` 上侦听，代理（不带剥离路径前缀）将在同一路径下访问Uvicorn：`http://127.0.0.1:8000/api/v1/app`。

## 使用Traefik在本地测试

您可以使用<a href="https://docs.traefik.io/" class="external-link" target="_blank"> Traefik </a>在本地使用剥离的路径前缀轻松地进行实验。

<a href="https://github.com/containous/traefik/releases" class="external-link" target="_blank">下载Traefik </a>，它是单个二进制文件，您可以提取压缩文件，并直接从终端运行它。

然后使用以下命令创建文件 `traefik.toml`：

```TOML hl_lines="3"
[entryPoints]
  [entryPoints.http]
    address = ":9999"

[providers]
  [providers.file]
    filename = "routes.toml"
```

这告诉Traefik监听端口9999并使用另一个文件 `routes.toml`。

!!! 提示
    我们使用的是端口9999而不是标准的HTTP端口80，因此您不必使用admin（`sudo`）特权即可运行它。

现在创建其他文件 `routes.toml` ：

```TOML hl_lines="5  12  20"
[http]
  [http.middlewares]

    [http.middlewares.api-stripprefix.stripPrefix]
      prefixes = ["/api/v1"]

  [http.routers]

    [http.routers.app-http]
      entryPoints = ["http"]
      service = "app"
      rule = "PathPrefix(`/api/v1`)"
      middlewares = ["api-stripprefix"]

  [http.services]

    [http.services.app]
      [http.services.app.loadBalancer]
        [[http.services.app.loadBalancer.servers]]
          url = "http://127.0.0.1:8000"
```

该文件将Traefik配置为使用路径前缀 `/api/v1`。

然后它将请求重定向到运行在 `http://127.0.0.1:8000` 上的Uvicorn。

现在启动Traefik：

<div class="termy">

```console
$ ./traefik --configFile=traefik.toml

INFO[0000] Configuration loaded from file: /home/user/awesomeapi/traefik.toml
```

</div>

现在，使用 `--root-path` 选项从Uvicorn启动您的应用程序：

<div class="termy">

```console
$ uvicorn main:app --root-path /api/v1

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

### 检查响应

现在，如果您转到带有Uvicorn端口的URL：<a href="http://127.0.0.1:8000/app" class="external-link" target="_blank">http://127.0.0.1:8000/app</a>，您将看到正常的响应：

```JSON
{
    "message": "Hello World",
    "root_path": "/api/v1"
}
```

!!! 提示
    请注意，即使您在 `http://127.0.0.1:8000/app` 上访问它，它也会显示 `/api/v1` 的 `root_path`，取自选项 `--root-path`。

现在，打开带有Traefik端口的URL，包括路径前缀：<a href="http://127.0.0.1:9999/api/v1/app" class="external-link" target="_blank">http://127.0.0.1:9999/api/vi/app</a>。

我们得到相同的响应：

```JSON
{
    "message": "Hello World",
    "root_path": "/api/v1"
}
```

但这一次是在具有代理提供的前缀路径的URL上：`/api/v1`。

当然，这里的想法是每个人都可以通过代理访问应用程序，因此路径前缀为 `/api/v1` 的版本是“正确的”版本。

Uvicorn直接提供的不带路径前缀的版本（`http://127.0.0.1:8000/app`）将专门供 _proxy_（Traefik）访问。

这说明了代理服务器（Traefik）如何使用路径前缀以及服务器（Uvicorn）如何使用选项 `--root-path` 中的 `root_path`。

### 检查docs UI

但这是有趣的部分。 ✨

访问应用程序的“官方”方式是通过具有我们定义的路径前缀的代理。 因此，正如我们期望的那样，如果直接尝试由Uvicorn提供的docs UI，并且URL中没有路径前缀，它将无法正常工作，因为它希望通过代理进行访问。

您可以在<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>上进行检查 ：

<img src="/img/tutorial/behind-a-proxy/image01.png">

但是，如果我们使用代理在 `/api/v1/docs` 处通过“官方” URL访问docs UI，它将正常工作！ 🎉

恰如我们所愿。 ✔️

这是因为FastAPI在内部使用此 `root_path` 来告诉docs UI使用由 `root_path` 提供的路径前缀的OpenAPI URL。

您可以在<a href="http://127.0.0.1:9999/api/v1/docs" class="external-link" target="_blank">http://127.0.0.1:9999/api/v1/docs</a>：

<img src="/img/tutorial/behind-a-proxy/image02.png">

## 挂载子应用程序

如果您需要安装子应用程序（如[子应用程序 - 安装](./sub-applications.md){.internal-link target=_blank})中所述，同时还使用带有 `root_path` 的代理，则可以 正如您所期望的那样正常进行。

FastAPI将在内部智能地使用 `root_path`，因此它将正常工作。 ✨
