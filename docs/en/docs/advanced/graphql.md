# GraphQL

**FastAPI** 使用 `graphene` 库对GraphQL（由Starlette直接提供）提供可选支持。

您可以在同一应用程序上将常规FastAPI *path operations* 与GraphQL结合使用。

## 导入和使用 `graphene`

GraphQL是通过Graphene实现的，您可以检查<a href="https://docs.graphene-python.org/en/latest/quickstart/" class="external-link" target="_blank"> Graphene的文档</a>有关更多详细信息。

导入 `graphene` 并定义您的GraphQL数据：

```Python hl_lines="1 6 7 8 9 10"
{!../../../docs_src/graphql/tutorial001.py!}
```

## 添加Starlette的 `GraphQLApp`

然后导入并添加Starlette的 `GraphQLApp`：

```Python hl_lines="3 14"
{!../../../docs_src/graphql/tutorial001.py!}
```

!!! 信息
    这里我们使用 `.add_route`，这是在Starlette中添加路由的方法（由FastAPI继承），而无需声明特定的操作（如 `.get()`, `.post()` 等） 。

## 查看结果

使用Uvicorn运行它，并在<a href="http://127.0.0.1:8000" class="external-link" target="_blank"> http://127.0.0.1:8000 </a>上打开浏览器 。

您将看到GraphiQL Web用户界面：

<img src="/img/tutorial/graphql/image01.png">

## 更多细节

有关更多详细信息，包括：

* 访问请求信息
* 添加后台任务
* 使用普通或异步功能

查看官方<a href="https://www.starlette.io/graphql/" class="external-link" target="_blank"> Starlette GraphQL文档</a>。
