# SQL (关系型) 数据库 使用Peewee

!!! 警告
    如果您只是开始，那么使用SQLAlchemy的教程[SQL（关系）数据库](../tutorial/sql-databases.md){.internal-link target=_blank}就足够了。

    随时跳过。

如果您是从头开始的项目，则最好使用SQLAlchemy ORM（[SQL（关系）数据库](../tutorial/sql-databases.md){.internal-link target=_blank}或任何其他工具。 其他异步ORM。

如果您已经拥有使用<a href="http://docs.peewee-orm.com/zh-CN/latest/" class="external-link" target="_blank"> Peewee ORM </a>的代码库 ，您可以在此处查看如何与 **FastAPI** 一起使用。

!!! 警告“需要Python 3.7+”
    您将需要Python 3.7或更高版本才能将Peewee与FastAPI一起安全使用。

## Peewee异步

Peewee不是为异步框架设计的，也不是考虑到它们的。

Peewee对它的默认值和应如何使用有一些“重”的假设。

如果您正在使用较旧的非异步框架开发应用程序，并且可以使用其所有默认值，那么 **它可能是一个很好的工具**。

但是，如果您需要更改一些默认值，支持多个预定义数据库，使用异步框架（如FastAPI）等，则需要添加一些复杂的额外代码来覆盖这些默认值。

尽管如此，还是有可能做到的，在这里您将确切地看到要使用Peewee与FastAPI一起添加的代码。

!!! 请注意“技术细节”
    您可以阅读有关Peewee关于Python中异步的立场的更多信息<a href="http://docs.peewee-orm.com/en/latest/peewee/database.html#async-with-gevent" class="external-link" target="_blank">in the docs</a>, <a href="https://github.com/coleifer/peewee/issues/263#issuecomment-517347032" class="external-link" target="_blank">an issue</a>, <a href="https://github.com/coleifer/peewee/pull/2072#issuecomment-563215132" class="external-link" target="_blank">a PR</a>。

## 同一应用

我们将创建与SQLAlchemy教程（[SQL（关系）数据库](../tutorial/sql-databases.md){.internal-link target=_blank}）相同的应用程序。

大多数代码实际上是相同的。

因此，我们将只关注差异。

## 文件结构

假设您有一个名为 `my_super_project` 的目录，其中包含一个名为 `sql_app` 的子目录，其结构如下：

```
.
└── sql_app
    ├── __init__.py
    ├── crud.py
    ├── database.py
    ├── main.py
    └── schemas.py
```

这与SQLAlchemy教程的结构几乎相同。

现在，让我们看看每个文件/模块的功能。

## 创建Peewee零件

让我们参考文件 `sql_app/database.py`。

### 标准Peewee代码

让我们首先检查所有正常的Peewee代码，创建一个Peewee数据库：

```Python hl_lines="3  5  22"
{!../../../docs_src/sql_databases_peewee/sql_app/database.py!}
```

!!! 提示
    请记住，如果要使用其他数据库（例如PostgreSQL），则不能只更改字符串。 您将需要使用其他Peewee数据库类。

#### 注意

参数：

```Python
check_same_thread=False
```

等效于SQLAlchemy教程中的一个：

```Python
connect_args={"check_same_thread": False}
```

...仅对于 `SQLite` 是必需的。

!!! 信息“技术细节”

    与[SQL（关系）数据库](../tutorial/sql-databases.md#note){.internal-link target=_blank}中的技术细节完全相同。

### 使Peewee异步兼容 `PeeweeConnectionState`

Peewee和FastAPI的主要问题在于Peewee高度依赖<a href="https://docs.python.org/3/library/threading.html#thread-local-data" class="external-link" target="_blank">Python的 `threading.local` </a>，它没有直接覆盖它的方法，也没有让您直接处理连接/会话的方法（如SQLAlchemy教程中所述）。

而且 `threading.local` 与现代Python的新异步功能不兼容。

!!! 请注意“技术细节”
    `threading.local` 用于具有“魔术”变量，每个线程的值都不同。

    这在较旧的框架中很有用，这些框架设计为每个请求只有一个线程，不能多也不少。

    使用此功能，每个请求将具有其自己的数据库连接/会话，这是实际的最终目标。

    但是，使用新的异步功能的FastAPI可以在同一线程上处理多个请求。 同时，对于单个请求，它可以在不同的线程（线程池）中运行多种操作，具体取决于您使用的是 `async def` 还是普通的 `def`。 这就是为FastAPI提供所有性能改进的原因。

但是Python 3.7及更高版本提供了一个更高级的替代 `threading.local` 的替代方法，它也可以在将要使用 `threading.local` 的地方使用，但是与新的异步功能兼容。

我们将使用它。 它称为<a href="https://docs.python.org/3/library/contextvars.html" class="external-link" target="_blank"> `contextvars` </a>。

我们将覆盖使用 `threading.local` 的Peewee内部部分，并以 `contextvars` 替换它们，并进行相应的更新。

这似乎有点复杂（实际上确实如此），您实际上并不需要完全了解它如何使用。

我们将创建一个 `PeeweeConnectionState` ：

```Python hl_lines="10 11 12 13 14 15 16 17 18 19"
{!../../../docs_src/sql_databases_peewee/sql_app/database.py!}
```

该类继承自Peewee使用的特殊内部类。

它具有使Peewee使用 `contextvars` 而不是 `threading.local` 的所有逻辑。

`contextvars` 的工作原理与 `threading.local` 略有不同。 但是Peewee内部代码的其余部分假定该类与 `threading.local` 一起使用。

因此，我们需要做一些额外的技巧，以使其像使用 `threading.local` 一样工作。 `__init__`，`__setattr__`和`__getattr__`实现了Peewee使用的所有必需的技巧，而又不知道它现在与FastAPI兼容。

!!! 提示
    当与FastAPI一起使用时，这只会使Peewee正常运行。 不能随机打开或关闭正在使用的连接，创建错误等。

    但这并没有赋予Peewee异步超级能力。 您仍然应该使用普通的 `def` 函数，而不是 `async def`。

### 使用自定义的 `PeeweeConnectionState` 类

现在，使用新的 `PeeweeConnectionState` 覆盖Peewee数据库 `db` 对象中的 `._state` 内部属性：

```Python hl_lines="24"
{!../../../docs_src/sql_databases_peewee/sql_app/database.py!}
```

!!! 提示
    确保在创建 `db` 后覆盖 `db._state`。

!!! 提示
    您将对任何其他Peewee数据库执行相同的操作，包括 `PostgresqlDatabase`，`MySQLDatabase` 等。

## 创建数据库模型

现在让我们看一下文件 `sql_app/models.py`。

###为我们的数据创建Peewee模型

现在为 `User` 和 `Item` 创建Peewee模型（类）。

如果遵循Peewee教程并将模型更新为具有与SQLAlchemy教程相同的数据，则将执行相同的操作。

!!! 提示
    Peewee还使用术语“**模型**”来指代与数据库交互的这些类和实例。

    但是Pydantic还使用术语“**模型**”来指代不同的东西，即数据验证，转换以及文档类和实例。

从 `database` 中导入 `db`（上面的文件 `database.py`），并在此处使用。

```Python hl_lines="3  6 7 8 9 10 11 12  15 16 17 18 19 20 21"
{!../../../docs_src/sql_databases_peewee/sql_app/models.py!}
```

!!! 提示
    Peewee创建了几个魔术属性。

     它将自动添加一个 `id` 属性作为整数作为主键。

     它将基于类名称选择表的名称。

     对于 `Item` ，它将创建一个属性 `owner_id` ，具有 `User` 的整数ID。 但是我们没有在任何地方声明它。

## 创建Pydantic模型

现在让我们检查文件 `sql_app/schemas.py`。

!!! 提示
    为了避免Peewee *models* 和 Pydantic *models* 之间的混淆，我们将在Peewee模型中使用 `models.py` 文件，在Pydantic模型中使用 `schemas.py` 文件。

    这些Pydantic模型或多或少定义了“模式”（有效数据形状）。
    
    因此，这将有助于我们避免在同时使用两者时产生混淆。

### 创建Pydantic *模型* /模式

创建与SQLAlchemy教程中相同的所有Pydantic模型：

```Python hl_lines="16 17 18  21 22  25 26 27 28 29 30  34 35  38 39  42 43 44 45 46 47 48"
{!../../../docs_src/sql_databases_peewee/sql_app/schemas.py!}
```

!!! 提示
    在这里，我们使用 `id` 创建模型。

    我们没有在Peewee模型中明确指定 `id` 属性，但是Peewee自动添加了一个。

    我们还将神奇的 `owner_id` 属性添加到 `Item` 中。

### 为Pydantic *模型* /模式创建 `PeeweeGetterDict`

当您访问Peewee对象中的关系时，例如在 `some_user.items` 中，Peewee不提供 `Item` 的 `list`。

它提供了 `ModelSelect` 类的特殊自定义对象。

可以使用 `list(some_user.items)` 创建其项目的 `list`。

但是对象本身不是 `list`。 而且它也不是真正的Python <a href="https://docs.python.org/3/glossary.html#term-generator" class="external-link" target="_blank"> generator </a> 。 因此，Pydantic默认情况下不知道如何将其转换为Pydantic *模型* /模式的 `list`。

但是最新版的Pydantic允许提供一个自 `pydantic.utils.GetterDict` 继承的自定义类，以提供使用 `orm_mode = True` 来检索ORM模型属性值时使用的功能。

我们将创建一个自定义的 `PeeweeGetterDict` 类，并在所有使用 `orm_mode` 的Pydantic * models /模式中使用它：

```Python hl_lines="3 8 9 10 11 12 13  31  49"
{!../../../docs_src/sql_databases_peewee/sql_app/schemas.py!}
```

在这里，我们正在检查正在访问的属性（例如 `some_user.items` 中的 `.items` ）是否是 `peewee.ModelSelect` 的实例。

如果是这种情况，只需返回一个 `list` 即可。

然后，在配置为 `getter_dict = PeeweeGetterDict` 的Pydantic *models* /模式中使用 `orm_mode = True`。

!!! 提示
     我们只需要创建一个 `PeeweeGetterDict` 类，就可以在所有Pydantic *models* /模式中使用它。

## CRUD实用程序

现在让我们看一下文件 `sql_app/crud.py`。

### 创建所有CRUD实用程序

创建与SQLAlchemy教程中相同的所有CRUD utils，所有代码都非常相似：

```Python hl_lines="1  4 5  8 9  12 13  16 17 18 19 20  23 24  27 28 29 30"
{!../../../docs_src/sql_databases_peewee/sql_app/crud.py!}
```

SQLAlchemy教程的代码有些不同。

我们没有传递 `db` 属性。 相反，我们直接使用模型。 这是因为 `db` 对象是一个全局对象，其中包括所有连接逻辑。 这就是为什么我们必须进行上面的所有 `contextvars` 更新。

麻生太郎，当返回几个对象时，例如在 `get_users` 中，我们直接调用 `list`，例如在：

```Python
list(models.User.select())
```

出于同样的原因，我们不得不创建自定义的 `PeeweeGetterDict`。 但是通过返回一个已经是 `list` 而不是 `peewee.ModelSelect` 的东西，带有 `List[models.User]`的 *path operation* 中的 `response_model`（我们将在后面看到）将正常工作。

## **FastAPI** 应用主入口

现在，在文件 `sql_app/main.py` 中，我们可以集成和使用之前创建的所有其他部分。

### 创建数据库表

以非常简单的方式创建数据库表：

```Python hl_lines="9 10 11"
{!../../../docs_src/sql_databases_peewee/sql_app/main.py!}
```

### 创建依赖

创建一个依赖关系，该依赖关系将在请求开始时连接数据库，并在请求结束时断开连接：

```Python hl_lines="23 24 25 26 27 28 29"
{!../../../docs_src/sql_databases_peewee/sql_app/main.py!}
```

这里我们有一个空的 `yield`，因为我们实际上并没有直接使用数据库对象。

它正在连接到数据库，并将连接数据存储在一个内部变量中，该变量对于每个请求都是独立的（使用上面的 `contextvars` 技巧）。

因为数据库连接可能是I/O阻塞，所以此依赖关系是通过常规的 `def` 函数创建的。

然后，在每个需要访问数据库的 *path operation function* 中，我们将其添加为依赖项。

但是我们没有使用该依赖关系给定的值（它实际上没有给出任何值，因为它的收益率是空的）。 因此，我们不会将其添加到 *path operation function* 中，而是添加到 `dependencies` 参数中的 *path operation decorator* 中：

```Python hl_lines="32  40  47  59  65  72"
{!../../../docs_src/sql_databases_peewee/sql_app/main.py!}
```

### 上下文变量子依赖性

为了使所有 `contextvars` 部分正常工作，我们需要确保对于每个使用数据库的请求，在 `ContextVar` 中都有一个独立的值，并且该值将用作数据库状态（连接，事务等） 整个请求。

为此，我们需要创建另一个 `async` 依赖关系 `reset_db_state()`，用作 `get_db()` 中的子依赖关系。 它将设置上下文变量的值（仅带有默认的“ dict”），该变量将用作整个请求的数据库状态。 然后，依赖项 `get_db()` 将在其中存储数据库状态（连接，事务等）。

```Python hl_lines="18 19 20"
{!../../../docs_src/sql_databases_peewee/sql_app/main.py!}
```

对于下一个请求**，因为我们将在 `async` 依赖关系 `reset_db_state()` 中再次重置该上下文变量，然后在 `get_db()` 依赖关系中创建一个新连接，所以该新请求将具有其自己的 数据库状态（连接，事务等）。

!!! 提示
    由于FastAPI是一个异步框架，因此可以开始处理一个请求，在完成之前，可以接收另一个请求并开始处理，并且所有请求都可以在同一线程中处理。

    但是上下文变量知道这些异步功能，因此，在async依赖项 `reset_db_state()` 中设置的Peewee数据库状态将在整个请求中保留其自己的数据。

    同时，另一个并发请求将具有自己的数据库状态，该状态对于整个请求是独立的。

#### Peewee代理

如果您使用的是<a href="http://docs.peewee-orm.com/en/latest/peewee/database.html#dynamically-defining-a-database" class="external-link" target="_blank">Peewee Proxy</a>，实际的数据库位于 `db.obj`。

因此，您可以使用以下方法重置它：

```Python hl_lines="3 4"
async def reset_db_state():
    database.db.obj._state._state.set(db_state_default.copy())
    database.db.obj._state.reset()
```

### 创建 **FastAPI** *path operations*

现在，最后是标准的 **FastAPI** *path operations* 代码。

```Python hl_lines="32 33 34 35 36 37  40 41 42 43  46 47 48 49 50 51 52 53  56 57 58 59 60 61 62  65 66 67 68  71 72 73 74 75 76 77 78 79"
{!../../../docs_src/sql_databases_peewee/sql_app/main.py!}
```

###关于 `def` 与 `sync def`

与SQLAlchemy相同，我们没有做类似的事情：

```Python
user = await models.User.select().first()
```

...但是我们使用的是：

```Python
user = models.User.select().first()
```

因此，再次，我们应该声明 *path operation functions* 和不带有 `async def` 的依赖项，而仅使用普通的 `def`，如下所示：

```Python hl_lines="2"
# Something goes here
def read_users(skip: int = 0, limit: int = 100):
    # Something goes here
```

## 使用异步测试Peewee

这个例子包括一个额外的 *path operation*，它通过 `time.sleep(sleep_time)` 来模拟一个较长的处理请求。

它将在开始时打开数据库连接，并且会等待几秒钟后再回复。 而且每个新请求将减少一秒钟的等待时间。

这很容易让您测试使用Peewee和FastAPI的应用程序是否正确处理了有关线程的所有内容。

如果您想检查Peewee如果不做任何修改就如何破坏您的应用程序，请转到 `sql_app/database.py` 文件并注释以下行：

```Python
# db._state = PeeweeConnectionState()
```

然后在文件 `sql_app/main.py` 中，注释 `async` 依赖项主体 `reset_db_state()` 并将其替换为pass：

```Python
async def reset_db_state():
#     database.db._state._state.set(db_state_default.copy())
#     database.db._state.reset()
    pass
```

然后通过Uvicorn来运行程序：

<div class="termy">

```console
$ uvicorn sql_app.main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

在<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank"> http://127.0.0.1:8000/docs </a>上打开浏览器，然后创建几个用户。

然后在<a href="http://127.0.0.1:8000/docs#/default/read_slow_users_slowusers__get" class="external-link" target="_blank">http://127.0.0.1:8000/docs#/default/read_slow_users_slowusers__get</a>同时。

在所有标签中转到 *path operation* “获取 `/slowusers/`”。 使用“Try it out”按钮，在每个选项卡中一个接一个地执行请求。

这些选项卡将等待一段时间，然后其中一些将显示`Internal Server Error`。

### 什么结果

第一个选项卡将使您的应用创建与数据库的连接，并等待几秒钟，然后回复并关闭数据库连接。

然后，对于下一个选项卡中的请求，您的应用程序将等待一秒钟，依此类推。

这意味着最终将比之前的某些选项更早地完成某些最后一个选项卡的请求。

然后，等待较少时间的最后一个请求将尝试打开数据库连接，但是由于先前对其他选项卡的请求之一可能与第一个请求在同一线程中处理，因此它将具有与第一个请求相同的数据库连接。已经打开，Peewee将抛出一个错误，您将在终端中看到它，并且响应将具有 `Internal Server Error`。

这些选项卡中的一个以上可能会发生这种情况。

如果您有多个客户完全同时在与您的应用对话，那么可能会发生这种情况。

而且，随着您的应用程序开始同时处理越来越多的客户端，单个请求中的等待时间就越来越短，以触发错误。

### 使用FastAPI修复Peewee

现在回到文件 `sql_app/database.py`，并取消注释该行：

```Python
db._state = PeeweeConnectionState()
```

然后在文件 `sql_app/main.py` 中，取消注释 `async` 依赖项 `reset_db_state()` 的主体：

```Python
async def reset_db_state():
    database.db._state._state.set(db_state_default.copy())
    database.db._state.reset()
```

终止正在运行的应用程序，然后重新启动。

使用10个标签重复相同的过程。 这次所有这些都将等待，您将获得所有结果而不会出错。

...您已修复！

## 查看所有文件

请记住，您应该有一个名为 `my_super_project` 的目录（或根据需要），该目录包含一个名为 `sql_app` 的子目录。

`sql_app` 应该有下列文件:

* `sql_app/__init__.py`: 是一个空文件.

* `sql_app/database.py`:

```Python
{!../../../docs_src/sql_databases_peewee/sql_app/database.py!}
```

* `sql_app/models.py`:

```Python
{!../../../docs_src/sql_databases_peewee/sql_app/models.py!}
```

* `sql_app/schemas.py`:

```Python
{!../../../docs_src/sql_databases_peewee/sql_app/schemas.py!}
```

* `sql_app/crud.py`:

```Python
{!../../../docs_src/sql_databases_peewee/sql_app/crud.py!}
```

* `sql_app/main.py`:

```Python
{!../../../docs_src/sql_databases_peewee/sql_app/main.py!}
```

## 技术细节

!!! 警告
    这些是您可能不需要的非常技术性的细节。

### 问题

Peewee使用<a href="https://docs.python.org/3/library/threading.html#thread-local-data" class="external-link" target="_blank"> `threading.local` </a>默认情况下存储它的数据库“状态”数据（连接，事务等）。

`threading.local` 创建一个当前线程专有的值，但是异步框架将在同一线程中运行所有代码（例如针对每个请求），并且可能不按顺序运行。

最重要的是，异步框架可以在线程池中运行一些同步代码（使用 `asyncio.run_in_executor`），但属于同一请求。

这意味着，在Peewee的当前实现中，多个任务可能会使用相同的 `threading.local` 变量，并最终共享相同的连接和数据（不应该），并且如果它们执行同步I/O阻塞线程池中的代码（与FastAPI中的常规 `def` 函数一样，在 *path operations* 和依赖项中使用），即使该代码是同一请求的一部分，也无法访问数据库状态变量。它应该能够访问相同的数据库状态。

### 上下文变量

Python 3.7具有<a href="https://docs.python.org/3/library/contextvars.html" class="external-link" target="_blank"> `contextvars` </a>可以创建一个 局部变量非常类似于 `threading.local` ，但也支持这些异步功能。

有几件事要牢记。

必须在模块顶部创建 `ContextVar`，例如：

```Python
some_var = ContextVar("some_var", default="default value")
```

要设置当前“上下文”中使用的值（例如用于当前请求），请使用：

```Python
some_var.set("new value")
```

要在上下文内的任何地方（例如，处理当前请求的任何部分）获取值，请使用：

```Python
some_var.get()
```

### 在 `async` 依赖项 `reset_db_state()` 中设置上下文变量

如果异步代码的某些部分使用 `some_var.set("updated in function")` 设置了值（例如，像 `async` 依赖项一样），则其中的其余代码和后续代码（包括内部代码） 使用 `await` 调用的 `async` 函数）将看到该新值。

因此，在本例中，如果我们在 `async` 依赖项中设置Peewee状态变量（默认为 `dict` ），则应用程序中所有其他内部代码都将看到该值并将其重用于 整个请求。

即使下一个请求是并发的，上下文变量也会再次为下一个请求设置。

### 在依赖项 `get_db()` 中设置数据库状态

由于 `get_db()` 是常规的 `def` 函数，因此 **FastAPI** 将使其在线程池中运行，并带有 `context` 的 *copy*，并保持上下文变量的相同值（即 `dict`（具有重置数据库状态）。 然后它可以将数据库状态添加到该 `dict` 中，例如连接等。

但是，如果在该普通的 `def` 函数中设置了上下文变量的值（默认值 `dict` ），它将创建一个新值，该值仅保留在线程池的该线程中，其余代码保留在该代码中（例如 *path operation functions*）将无法访问它。 在 `get_db()` 中，我们只能在 `dict` 中设置值，而不能在整个 `dict` 中设置值。

因此，我们需要具有 `async` 依赖项 `reset_db_state()`，以便在上下文变量中设置 `dict`。 这样，所有代码都可以为单个请求访问数据库状态的相同 `dict`。

### 在依赖性 `get_db()` 中连接和断开连接

那么下一个问题是，为什么不仅仅在async依赖关系本身而不是在 `get_db()` 中连接和断开数据库？

对于要在其余请求中保留的上下文变量， `async` 依赖性必须为 `async`，但是创建和关闭数据库连接可能会阻塞，因此如果存在，则可能会降低性能。

因此，我们还需要常规的 `def` 依赖关系 `get_db()` 。
