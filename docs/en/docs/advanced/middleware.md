# 高级中间件

在主教程中，您将了解如何将[自定义中间件](../tutorial/middleware.md){.internal-link target=_blank}添加到您的应用程序中。

然后，您还阅读了如何使用[CORS with the `CORSMiddleware`](../tutorial/cors.md){.internal-link target=_blank}。

在本节中，我们将看到如何使用其他中间件。

## 添加ASGI中间件

由于 **FastAPI** 基于Starlette并实现了<abbr title="异步服务器网关接口"> ASGI </abbr>规范，因此您可以使用任何ASGI中间件。

只要遵循ASGI规范，就不必为FastAPI或Starlette制作中间件。

通常，ASGI中间件是希望将ASGI应用程序作为第一个参数的类。

因此，在第三方ASGI中间件的文档中，它们可能会告诉您执行以下操作：

```Python
from unicorn import UnicornMiddleware

app = SomeASGIApp()

new_app = UnicornMiddleware(app, some_config="rainbow")
```

但是FastAPI（实际上是Starlette）提供了一种更简单的方法来确保内部中间件处理服务器错误和自定义异常处理程序正常工作。

为此，您可以使用 `app.add_middleware()`（如在CORS的示例中）。

```Python
from fastapi import FastAPI
from unicorn import UnicornMiddleware

app = FastAPI()

app.add_middleware(UnicornMiddleware, some_config="rainbow")
```

`app.add_middleware()` 接收中间件类作为第一个参数，并接收任何其他要传递给中间件的参数。

## 集成中间件

**FastAPI** 包括几种常见用例的中间件，接下来我们将介绍如何使用它们。

!!! 请注意“技术细节”
    对于下一个示例，您还可以使用 `from from starlette.middleware.something import SomethingMiddleware`。

    **FastAPI** 在 `fastapi.middleware` 中提供了几种中间件，以方便开发人员。 但是大多数可用的中间件直接来自Starlette。

## `HTTPSRedirectMiddleware`

强制所有传入请求必须为 `https` 或 `wss`。

相反，任何对 `https` 或 `wss` 的传入请求都将重定向到安全方案。

```Python hl_lines="2  6"
{!../../../docs_src/advanced_middleware/tutorial001.py!}
```

## `TrustedHostMiddleware`

强制所有传入请求都具有正确设置的 `Host` header，以防范HTTP主机标头攻击。

```Python hl_lines="2  6 7 8"
{!../../../docs_src/advanced_middleware/tutorial002.py!}
```

支持以下参数：

* `allowed_hosts` - 应该被允许作为主机名的域名列表。 支持使用通配符域（例如 `*.example.com`）来匹配子域，以允许任何主机名使用 `allowed_hosts=["*"]` 或省略中间件。

如果传入请求未正确验证，则将发送 `400` 响应。

## `GZipMiddleware`

处理任何在 `Accept-Encoding` header中包含 `gzip` 的请求的GZip响应。

中间件将处理标准响应和流响应。

```Python hl_lines="2  6"
{!../../../docs_src/advanced_middleware/tutorial003.py!}
```

支持以下参数：

* `minimum_size` - 请勿对小于此最小大小的字节进行GZip响应。 默认为 `500`。

## 其他中间件

还有许多其他的ASGI中间件。

例如：

* <a href="https://docs.sentry.io/platforms/python/asgi/" class="external-link" target="_blank">Sentry</a>
* <a href="https://github.com/encode/uvicorn/blob/master/uvicorn/middleware/proxy_headers.py" class="external-link" target="_blank">Uvicorn's `ProxyHeadersMiddleware`</a>
* <a href="https://github.com/florimondmanca/msgpack-asgi" class="external-link" target="_blank">MessagePack</a>

To see other available middlewares check <a href="https://www.starlette.io/middleware/" class="external-link" target="_blank">Starlette's Middleware docs</a> and the <a href="https://github.com/florimondmanca/awesome-asgi" class="external-link" target="_blank">ASGI Awesome List</a>.
