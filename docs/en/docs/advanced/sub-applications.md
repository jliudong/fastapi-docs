# 子应用程序 - 支架

如果您需要两个独立的FastAPI应用程序，以及各自的独立OpenAPI和文档ui，则可以拥有一个主应用程序并“装载”一个（或多个）子应用程序。

## 挂载 **FastAPI** 应用程序

“挂载”意味着在特定路径中添加一个完全“独立”的应用程序，然后使用该子应用程序中声明的 _path operation_ 来处理该路径下的所有内容。

### 顶级应用程序

首先，创建主顶级 **FastAPI** 应用程序及其 *path operations*：

```Python hl_lines="3 6 7 8"
{!../../../docs_src/sub_applications/tutorial001.py!}
```

### 子应用程序

然后，创建您的子应用程序及其 *path operations* 。

该子应用程序只是另一个标准的FastAPI应用程序，但这是将“挂载”的应用程序：

```Python hl_lines="11 14 15 16"
{!../../../docs_src/sub_applications/tutorial001.py!}
```

### 挂载子应用程序

在您的顶级应用程序 `app` 中，安装子应用程序 `subapi`。

在这种情况下，它将安装在路径 `/subapi` 处：

```Python hl_lines="11 19"
{!../../../docs_src/sub_applications/tutorial001.py!}
```

### 检查自动API文档

现在，使用主应用程序运行 `uvicorn`，如果您的文件是 `main.py`，则为：

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

并在<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank"> http://127.0.0.1:8000/docs </a>上打开文档。

您将看到主应用程序的自动API文档，仅包括其自己的 _path operation_ ：

<img src="/img/tutorial/sub-applications/image01.png">

然后，打开子应用程序的文档 <a href="http://127.0.0.1:8000/subapi/docs" class="external-link" target="_blank">http://127.0.0.1:8000/subapi/docs</a>。

您将看到该子应用程序的自动API文档，仅包括其自己的 _path operation_ ，都位于正确的子路径前缀 `/subapi` 下：

<img src="/img/tutorial/sub-applications/image02.png">

如果您尝试与两个用户界面中的任何一个进行交互，则它们将正常工作，因为浏览器将能够与每个特定的应用程序或子应用程序进行对话。

### 技术细节：`root_path`

如上所述安装子应用程序时，FastAPI将使用ASGI规范中称为 `root_path` 的机制来为子应用程序传递安装路径。

这样，子应用程序将知道在文档UI中使用该路径前缀。

子应用程序也可以有自己安装的子应用程序，并且一切都会正常运行，因为FastAPI自动处理所有这些`root_path`。

您将在关于[位于代理之后](./behind-a-proxy.md){.internal-link target=_blank} 的部分中了解有关 `root_path` 以及如何显式使用它的更多信息。
