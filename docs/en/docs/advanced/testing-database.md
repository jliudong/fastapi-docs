# 测试数据库

您可以使用[带有覆盖的测试依赖关系](testing-dependencies.md){.internal-link target=_blank}中相同的依赖关系覆盖来更改要测试的数据库。

您可能想要建立一个用于测试的其他数据库，在测试后回滚数据，用一些测试数据预填充等等。

主要思想与您在前一章中看到的完全相同。

## 为SQL应用程序添加测试

让我们从[SQL（关系）数据库](../tutorial/sql-databases.md){.internal-link target=_blank}更新示例以使用测试数据库。

所有应用程序代码都是相同的，您可以返回该章检查它的状态。

唯一的更改是在新的测试文件中。

您的常规依赖项 `get_db()` 将返回数据库会话。

在测试中，您可以使用依赖项替代来返回 *custom* 数据库会话，而不是通常使用的会话。

在此示例中，我们将仅为测试创建一个临时数据库。

## 文件结构

我们在 `sql_app/tests/test_sql_app.py` 中创建一个新文件。

因此，新的文件结构如下所示：

``` hl_lines="9 10 11"
.
└── sql_app
    ├── __init__.py
    ├── crud.py
    ├── database.py
    ├── main.py
    ├── models.py
    ├── schemas.py
    └── tests
        ├── __init__.py
        └── test_sql_app.py
```

## 创建新的数据库会话

首先，我们使用新数据库创建一个新的数据库会话。

对于测试，我们将使用文件 `test.db` 而不是 `sql_app.db`。

但是其余的会话代码大致相同，我们只复制它即可。

```Python hl_lines="8  9  10 11 12 13"
{!../../../docs_src/sql_databases/sql_app/tests/test_sql_app.py!}
```

!!! 提示
    您可以通过将其放入函数中并从 `database.py` 和 `tests/test_sql_app.py` 中使用该代码来减少重复代码。

    为了简单起见并专注于特定的测试代码，我们只复制它。

## 创建数据库

因为现在我们要在新文件中使用新数据库，所以我们需要确保使用以下方式创建数据库：

```Python
Base.metadata.create_all(bind=engine)
```

这通常在 `main.py` 中调用，但是 `main.py` 中的行使用数据库文件 `sql_app.db`，我们需要确保为测试创建 `test.db`。

因此，我们在此处添加该行以及新文件。

```Python hl_lines="16"
{!../../../docs_src/sql_databases/sql_app/tests/test_sql_app.py!}
```

## 依赖覆盖

现在，我们创建依赖项替代，并将其添加到应用程序的替代中。

```Python hl_lines="19 20 21 22 23 24  27"
{!../../../docs_src/sql_databases/sql_app/tests/test_sql_app.py!}
```

!!! 提示
    `override_get_db()` 的代码与 `get_db()` 的代码几乎完全相同，但是在 `override_get_db()` 中，我们将 `TestingSessionLocal` 用作测试数据库。

## 测试应用

然后，我们可以正常测试该应用程序。

```Python hl_lines="32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47"
{!../../../docs_src/sql_databases/sql_app/tests/test_sql_app.py!}
```

在测试过程中，我们对数据库所做的所有修改都将存储在 `test.db` 数据库中，而不是主 `sql_app.db` 中。
