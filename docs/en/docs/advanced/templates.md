# 模板

您可以将任意模板引擎与 **FastAPI** 配合使用。

常见的选择是Jinja2，与Flask和其他工具使用的选举相同。

有一些实用程序可以轻松配置它，您可以直接在 **FastAPI** 应用程序（由Starlette提供）中使用它。

##安装依赖项

安装 `jinja2`：

<div class="termy">

```console
$ pip install jinja2

---> 100%
```

</div>

如果您还需要提供静态文件（如本例所示），请安装 `aiofiles`：

<div class="termy">

```console
$ pip install aiofiles

---> 100%
```

</div>

## 使用 `Jinja2Templates`

* 导入 `Jinja2Templates`。
* 创建一个 `templates` 对象，以后可以重用。
* 在 *path operation* 中声明一个 `Request` 参数，该参数将返回一个模板。
* 使用您创建的 `templates` 来渲染并返回一个 `TemplateResponse`，并将 `request` 作为Jinja2“上下文”中的键值对之一传递。

```Python hl_lines="3  10  14 15"
{!../../../docs_src/templates/tutorial001.py!}
```

!!! 注意
    注意，您必须在Jinja2的上下文中将 `request` 作为键值对的一部分传递。 因此，您还必须在*path operation*中声明它。

!!! 请注意“技术细节”
    你也可以使用 `from from starlette.templating import Jinja2Templates`。

    **FastAPI** 与 `fastapi.templating` 提供相同的 `starlette.templating`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。 与 `Request` 和 `StaticFiles` 相同。

## 编写模板

然后，您可以在 `templates/item.html` 上使用以下内容编写模板：

```jinja hl_lines="7"
{!../../../docs_src/templates/templates/item.html!}
```

它将显示从您传递的“上下文” `dict` 中获取的 `id`：

```Python
{"request": request, "id": id}
```

## 模板和静态文件

并且您还可以在模板内部使用 `url_for()`，并将其与例如挂载的 `StaticFiles` 一起使用。

```jinja hl_lines="4"
{!../../../docs_src/templates/templates/item.html!}
```

在此示例中，它将通过以下方式链接到位于 `static/styles.css` 处的CSS文件：

```CSS hl_lines="4"
{!../../../docs_src/templates/static/styles.css!}
```

并且因为您使用的是 `StaticFiles`，所以CSS文件将由您的 **FastAPI** 应用程序自动提供给URL `/static/styles.css`。

## 更多详细内容

遥想获得更多详细内容，包括如何测试模板，请查看 <a href="https://www.starlette.io/templates/" class="external-link" target="_blank">Starlette's docs on templates</a>.
