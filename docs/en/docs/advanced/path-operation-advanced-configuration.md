# 路径操作高级配置

## OpenAPI operationId

!!! 警告
    如果您不是OpenAPI中的“专家”，则可能不需要它。

您可以通过参数 `operation_id` 设置要在 *path operation* 中使用的OpenAPI `operationId`。

您必须确保每个操作的唯一性。

```Python hl_lines="6"
{!../../../docs_src/path_operation_advanced_configuration/tutorial001.py!}
```

### 使用 *path operation function* 名字 operationId

如果您想将API的函数名用作 `operationId`，则可以对其全部进行迭代，并使用其 `APIRoute.name` 覆盖每个 *path operations* 的 `operation_id`。

您应该在添加所有 *path operations* 之后执行此操作。

```Python hl_lines="2 12 13 14 15 16 17 18 19 20 21 24"
{!../../../docs_src/path_operation_advanced_configuration/tutorial002.py!}
```

!!! 提示
    如果您手动调用 `app.openapi()`，则应在此之前更新 `operationId`。

!!! 警告
    如果这样做，则必须确保每个 *path operation functions* 都具有唯一的名称。

    即使它们位于不同的模块（Python文件）中。

## 从OpenAPI排除

要从生成的OpenAPI模式（因此，从自动文档系统）中排除 *path operation* ，请使用参数 `include_in_schema` 并将其设置为 `False`；

```Python hl_lines="6"
{!../../../docs_src/path_operation_advanced_configuration/tutorial003.py!}
```

## 文档字符串的高级描述

您可以限制OpenAPI的 *path operation function* 的文档字符串中使用的行。

添加`\f`（转义的“换页”字符）会导致 **FastAPI** 截断此时用于OpenAPI的输出。

它不会显示在文档中，但是其他工具（例如Sphinx）将可以使用其余的工具。

```Python hl_lines="19 20 21 22 23 24 25 26 27 28 29"
{!../../../docs_src/path_operation_advanced_configuration/tutorial004.py!}
```
