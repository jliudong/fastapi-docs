# 高级依赖

## 参数化依赖

我们看到的所有依赖项都是固定的函数或类。

但是在某些情况下，您可能希望能够在依赖项上设置参数，而不必声明许多不同的函数或类。

假设我们想要一个依赖项来检查查询参数 `q` 是否包含某些固定内容。

但是我们希望能够参数化该固定内容。

## “可调用”实例

在Python中，有一种方法可以将类的实例设为“可调用的”。

不是类本身（已经是可调用的），而是该类的实例。

为此，我们声明一个方法 `__call__`：

```Python hl_lines="10"
{!../../../docs_src/dependencies/tutorial011.py!}
```

在这种情况下，此 `__call__` 是 **FastAPI** 用来检查其他参数和子依赖项的方法，以后将在将其传递给 *path operation function* 中的参数时调用该方法。

## 参数化实例

现在，我们可以使用 `__init__` 声明实例的参数，以用于“参数化”依赖项：

```Python hl_lines="7"
{!../../../docs_src/dependencies/tutorial011.py!}
```

在这种情况下，**FastAPI** 永远不会涉及或关心 `__init__`，我们将在我们的代码中直接使用它。

## 创建一个实例

我们可以使用以下方法创建此类的实例：

```Python hl_lines="16"
{!../../../docs_src/dependencies/tutorial011.py!}
```

这样，我们就可以“参数化”我们的依赖项，该依赖项中现在有 `"bar"` 作为属性 `checker.fixed_content`。

## 使用实例作为依赖项

然后，我们可以在 `Depends(checker)` 中使用此 `checker`，而不是 `Depends(FixedContentQueryChecker)`，因为依赖关系是实例 `checker`，而不是类本身。

在解决依赖关系时，**FastAPI** 将调用此 `checker`，例如：

```Python
checker(q="somequery")
```

...并在我们的 *path operation function* 中将返回的所有依赖项值作为参数 `fixed_content_included` 传递：

```Python hl_lines="20"
{!../../../docs_src/dependencies/tutorial011.py!}
```

!!! 提示
    所有这些似乎都是人为的。 可能还不清楚如何有用。

    这些示例特意简单，但说明了它们的工作原理。

    在有关安全性的章节中，有以相同方式实现的实用程序功能。

    如果您理解了所有这些内容，那么您已经知道这些用于安全性的实用工具如何在下面工作。
