# 扩展OpenAPI

!!! 警告
    这是一个相当高级的功能。您可以跳过它。

    如果您只是在遵循教程-用户指南，则可以跳过本节。

    如果您已经知道需要修改生成的OpenAPI模式，请继续阅读。

在某些情况下，您可能需要修改生成的OpenAPI模式。

在本节中，您将看到如何。

## 正常程序

正常（最小值）过程如下。

一个 `FastAPI` 应用程序（实例）具有一个 `.openapi()` 方法，该方法应返回OpenAPI模式。

作为应用程序对象创建的一部分，将为 `/openapi.json`（或您为自己设置的openapi_url）设置 *path operation*。

它只是返回一个JSON响应，并带有应用程序的 `.openapi()` 方法的结果。

默认情况下，方法 `.openapi()` 执行的操作是检查属性 `.openapi_schema` 以查看其是否包含内容并返回它们。

如果没有，则使用实用程序程序 `fastapi.openapi.utils.get_openapi` 生成它们。

函数 `get_openapi()` 接收作为参数：

* `title`：文档中显示的OpenAPI标题。
* `version`：您的API版本，例如 `2.5.0`。
* `openapi_version`：使用的OpenAPI规范的版本。 默认情况下，最新的是 `3.0.2`。
* `description`：您的API的描述。
* `routes`：路由列表，这些是每个已注册的 *path operations*。 它们取自 `app.routes`。
* `openapi_prefix`：在您的OpenAPI中使用的URL前缀。

## 覆盖默认值

使用以上信息，您可以使用相同的实用程序函数来生成OpenAPI架构并覆盖所需的每个部分。

例如，我们添加<a href="https://github.com/Rebilly/ReDoc/blob/master/docs/redoc-vendor-extensions.md#x-logo" class="external-link" target="_blank"> ReDoc的OpenAPI扩展包括自定义徽标</a>。

### 正常 **FastAPI**

首先，照常编写所有 **FastAPI** 

```Python hl_lines="1 4 7 8 9"
{!../../../docs_src/extending_openapi/tutorial001.py!}
```

### 生成OpenAPI模式

然后，在 `custom_openapi()` 函数内部，使用相同的实用程序函数生成OpenAPI模式：

```Python hl_lines="2  15 16 17 18 19 20 21"
{!../../../docs_src/extending_openapi/tutorial001.py!}
```

!!! 提示
    `openapi_prefix` 将包含生成的OpenAPI *path operations* 所需的任何前缀。

    FastAPI将自动使用 `root_path` 传递给 `openapi_prefix`。

    但是重要的是，您的函数应该接收该参数 `openapi_prefix` 并将其传递。

### 修改OpenAPI模式

现在，您可以添加ReDoc扩展，在OpenAPI模式中的 `info` “对象”中添加自定义的 `x-logo`：

```Python hl_lines="22 23 24"
{!../../../docs_src/extending_openapi/tutorial001.py!}
```

### 缓存OpenAPI模式

您可以使用属性 `.openapi_schema` 作为“缓存”来存储生成的模式。

这样，您的应用程序不必在用户每次打开API文档时都生成架构。

它只会生成一次，然后相同的缓存模式将用于下一个请求。

```Python hl_lines="13 14  25 26"
{!../../../docs_src/extending_openapi/tutorial001.py!}
```

### 覆盖方法

现在您可以用新函数替换 `.openapi()` 方法。

```Python hl_lines="29"
{!../../../docs_src/extending_openapi/tutorial001.py!}
```

### 检查结果

转到<a href="http://127.0.0.1:8000/redoc" class="external-link" target="_blank"> http://127.0.0.1:8000/redoc </a>后，您 会看到您正在使用您的自定义徽标（在此示例中为 **FastAPI** 的徽标）：

<img src="/img/tutorial/extending-openapi/image01.png">

## 用于文档的自托管JavaScript和CSS

API文档使用 **Swagger UI** 和 **ReDoc**，并且每个文档都需要一些JavaScript和CSS文件。

默认情况下，这些文件是通过 <abbr title="Content Delivery Network: A service, normally composed of several servers, that provides static files, like JavaScript and CSS. It's commonly used to serve those files from the server closer to the client, improving performance.">CDN</abbr>。

但是可以对其进行自定义，可以设置特定的CDN，也可以自己提供文件。

例如，如果您需要应用程序即使在脱机，无法打开Internet访问或在本地网络中的情况下仍能正常工作，则这很有用。

在这里，您将看到如何在同一FastAPI应用程序中自行提供这些文件，以及如何配置文档以使用它们。

### 项目文件结构

假设您的项目文件结构如下所示：

```
.
├── app
│   ├── __init__.py
│   ├── main.py
```

现在创建一个目录来存储那些静态文件。

您的新文件结构可能如下所示：

```
.
├── app
│   ├── __init__.py
│   ├── main.py
└── static/
```

### 下载文件

下载文档所需的静态文件，并将其放在该 `static/` 目录中。

您可能可以右键单击每个链接，然后选择类似于 `Save link as...` 的选项。

**Swagger UI** 使用以下文件：

* <a href="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3/swagger-ui-bundle.js" class="external-link" target="_blank">`swagger-ui-bundle.js`</a>
* <a href="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3/swagger-ui.css" class="external-link" target="_blank">`swagger-ui.css`</a>

并且 **ReDoc** 使用文件:

* <a href="https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js" class="external-link" target="_blank">`redoc.standalone.js`</a>

文件结构应该类似如下：

```
.
├── app
│   ├── __init__.py
│   ├── main.py
└── static
    ├── redoc.standalone.js
    ├── swagger-ui-bundle.js
    └── swagger-ui.css
```

### 安装 `aiofiles`

现在您需要安装 `aiofiles`：

<div class="termy">

```console
$ pip install aiofiles

---> 100%
```

</div>

### 发布静态文件

* 导入 `StaticFiles`。
* 在特定路径中“挂载”  `StaticFiles()` 实例。

```Python hl_lines="7 11"
{!../../../docs_src/extending_openapi/tutorial002.py!}
```

### 测试静态文件

启动您的应用程序，然后转到 <a href="http://127.0.0.1:8000/static/redoc.standalone.js" class="external-link" target="_blank">http://127.0.0.1:8000/static/redoc.standalone.js</a>。

您应该会看到 **ReDoc** 的非常长的JavaScript文件。

它可以开始于：

```JavaScript
/*!
 * ReDoc - OpenAPI/Swagger-generated API Reference Documentation
 * -------------------------------------------------------------
 *   Version: "2.0.0-rc.18"
 *   Repo: https://github.com/Redocly/redoc
 */
!function(e,t){"object"==typeof exports&&"object"==typeof m

...
```

这确认您能够从您的应用程序提供静态文件，并且已将文档的静态文件放置在正确的位置。

现在，我们可以配置应用程序以将这些静态文件用于文档。

### 禁用自动文档

第一步是禁用自动文档，因为默认情况下那些文档使用CDN。

要禁用它们，请在创建 `FastAPI` 应用时将其URL设置为 `None`：

```Python hl_lines="9"
{!../../../docs_src/extending_openapi/tutorial002.py!}
```

### 包括自定义文档

现在，您可以为自定义文档创建 *path operations*。

您可以重复使用FastAPI的内部函数为文档创建HTML页面，并将所需的参数传递给它们：

* `openapi_url`：文档的HTML页面可在其中获取API的OpenAPI架构的URL。 您可以在此处使用属性`app.openapi_url`。
* `title`：您的API的标题。
* `oauth2_redirect_url`：您可以在此处使用 `app.swagger_ui_oauth2_redirect_url` 来使用默认值。
* `swagger_js_url`：Swagger UI文档的HTML可以获取** JavaScript **文件的URL。 这是您自己的应用现在正在提供的应用。
* `swagger_css_url`：Swagger UI文档的HTML可以获取** CSS **文件的URL。 这是您自己的应用现在正在提供的应用。

同样对于ReDoc ...

```Python hl_lines="2 3 4 5 6   14 15 16 17 18 19 20 21 22    25 26 27   30 31 32 33 34 35 36"
{!../../../docs_src/extending_openapi/tutorial002.py!}
```

!!! 提示
    `swagger_ui_redirect` 的 *path operation* 是使用OAuth2时的帮助器。

     如果您将API与OAuth2提供程序集成，则可以进行身份验证，并使用获取的凭据返回API文档。 并使用真正的OAuth2身份验证与其进行交互。

     Swagger UI将在后台为您处理它，但是它需要此“重定向”帮助程序。

### 创建 *path operation* 进行测试

现在，为了能够测试一切正常，创建一个 *path operation*：

```Python hl_lines="39 40 41"
{!../../../docs_src/extending_openapi/tutorial002.py!}
```

### 测试

现在，您应该可以断开WiFi了，转到<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank"> http：// 127.0.0.1:8000/docs </a>，然后重新加载页面。

即使没有Internet，您也可以查看API的文档并与之交互。
