# 响应 - 更改状态代码

您可能在阅读之前可以设置默认的[响应状态代码](../tutorial/response-status-code.md){.internal-link target=_blank}。

但是在某些情况下，您需要返回不同于默认状态代码的状态代码。

## 用例

例如，假设您想默认返回HTTP状态代码“OK” `200`。

但是，如果数据不存在，则要创建它，并返回HTTP状态代码“ CREATED” `201`。

但是您仍然希望能够使用 `response_model` 过滤和转换返回的数据。

在这种情况下，您可以使用 `Response` 参数。

## 使用 `Response` 参数

您可以在 *path operation function* 中声明类型为 `Response` 的参数（就像对cookie和header一样）。

然后，您可以在该 *temporal* 响应对象中设置 `status_code`。

```Python hl_lines="1  9  12"
{!../../../docs_src/response_change_status_code/tutorial001.py!}
```

然后，您可以像往常一样返回所需的任何对象（`dict`，数据库模型等）。

而且，如果您声明了 `response_model` ，它仍将用于过滤和转换您返回的对象。

**FastAPI** 将使用该 *temporal* 响应来提取状态代码（还包括cookie和header），并将它们放入包含您返回的值的最终响应中，并通过任何 `response_model` 进行过滤。

您还可以在依赖项中声明 `Response` 参数，并在依赖项中设置状态代码。 但是请记住，最后一个才有效。
