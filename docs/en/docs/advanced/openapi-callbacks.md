# OpenAPI回调

您可以使用 *path operation* 创建一个API，该API可以触发对其他人（可能是*使用*您的API的相同开发人员）创建的 *external API* 的请求。

当您的API应用程序调用“外部API”时发生的过程称为“回调”。 因为外部开发人员编写的软件会将请求发送到您的API，然后您的API“回呼” *，因此会将请求发送到“外部API”（可能是同一开发人员创建的）。

在这种情况下，您可能希望记录外部API的外观。 它应该具有什么 *path operation*，它应该期待什么主体，它应该返回什么响应等。

## 带有回调的应用

我们来看一个例子。

想象一下，您开发了一个可以创建发票的应用程序。

这些发票将包含一个 `id`，`title`（可选），`customer` 和 `total`。

API的用户（外部开发人员）将使用POST请求在您的API中创建发票。

然后，您的API将（让我们想象）：

* 将发票发送给外部开发人员的某些客户。
* 收钱。
* 将通知发送回API用户（外部开发人员）。
    * 这将通过向该外部开发人员提供的某些 *外部API* 发送POST请求（从 *您的API*）来完成（这是“回调”）。

## 常规 **FastAPI** 应用

在添加回调之前，让我们首先了解普通API应用的外观。

它将具有一个 *path operation*，它将接收一个 `Invoice` 主体，以及一个查询参数 `callback_url`，其中将包含回调的URL。

这部分很正常，您可能已经熟悉了大多数代码：

```Python hl_lines="8 9 10 11 12  34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53"
{!../../../docs_src/openapi_callbacks/tutorial001.py!}
```

!!! 提示
    `callback_url` 查询参数使用Pydantic <a href="https://pydantic-docs.helpmanual.io/usage/types/#urls" class="external-link" target="_blank"> URL </a>类型。

唯一的新事物是 `callbacks=messages_callback_router.routes` 作为 *path operation decorator* 的参数。 我们将看到接下来的内容。

## 记录回调

实际的回调代码将在很大程度上取决于您自己的API应用。

从一个应用程序到下一个应用程序，它可能会有很大的不同。

它可能只是一两行代码，例如：

```Python
callback_url = "https://example.com/api/v1/invoices/events/"
requests.post(callback_url, json={"description": "Invoice paid", "paid": True})
```

但是回调中最重要的部分可能是根据您的API将在回调的请求主体中发送的数据，确保您的API用户（外部开发人员）正确实现 *external API*， 等等

因此，我们接下来要做的是添加代码以记录 *external API* 应该如何从 *your API* 接收回调。

该文档将显示在API中 `/docs` 的Swagger UI中，并使外部开发人员知道如何构建 *外部API*。

此示例未实现回调本身（可能只是一行代码），仅实现了文档部分。

!!! 提示
    实际的回调只是一个HTTP请求。

    自己实现回调时，可以使用<a href="https://www.encode.io/httpx/" class="external-link" target="_blank"> HTTPX </a>或<a href =“ https://requests.readthedocs.io/” class =“ external-link” target =“ _ blank”>请求</a>。

## 编写回调文档代码

这段代码不会在您的应用中执行，我们只需要用它来 *记录* *external API* 的外观即可。

但是，您已经知道如何使用 **FastAPI** 轻松地为API创建自动文档。

因此，我们将使用相同的知识来通过创建外部API应该实现的路径操作（您的API将调用的操作）来记录外部API的外观。

!!! 提示
    在编写用于记录回调的代码时，想象一下您是 *external developer* 可能会很有用。 并且您当前正在实现 *外部API*，而不是 *您的API*。

    暂时采用（*external developer*）的这种观点可以帮助您感觉更明显的是，在何处放置参数，主体的Pydantic模型，响应的信息等等。

### 创建一个回调 `APIRouter`

首先创建一个新的 `APIRouter`，它将包含一个或多个回调。

该路由器永远不会添加到实际的 `FastAPI` 应用中（即，永远不会传递到 `app.include_router(...)`）。

因此，您需要声明 `default_response_class`，并将其设置为 `JSONResponse`。

!!! 注意“技术细节”
    通常在调用app.include_router（some_router）时，由FastAPI应用设置“ response_class”。

     但是由于我们从不调用 `app.include_router(some_router)`，因此我们需要在创建 `APIRouter` 的过程中设置 `default_response_class`。

```Python hl_lines="3 24"
{!../../../docs_src/openapi_callbacks/tutorial001.py!}
```

### 创建回调 *path operation*

要创建回调 *path operation*，请使用上面创建的同一 `APIRouter`。

它看起来应该像普通的FastAPI *path operation*：

* 它可能应该声明应该接收的主体，例如 `body：InvoiceEvent`。
* 并且还可以声明应返回的响应，例如 `response_model = InvoiceEventReceived`。

```Python hl_lines="15 16 17  20 21  27 28 29 30 31"
{!../../../docs_src/openapi_callbacks/tutorial001.py!}
```

与常规 *path operation* 有2个主要区别：

* 不需要任何实际代码，因为您的应用程序永远不会调用此代码。 它仅用于记录 *external API*。 因此，该功能可能只有 `pass`。
* *path* 可以包含<a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#key-expression" class="external-link" target="_blank">OpenAPI 3 expression</a>（请参阅下面的更多内容），在这里它可以使用带有参数的变量和发送到 *your API* 的原始请求的一部分。

### 回调路径表达式

回调 *path* 可以具有<a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#key-expression" class="external-link" target="_blank">OpenAPI 3 expression</a>，其中可以包含发送到 *your API* 的原始请求的一部分。

在这种情况下，它是 `str`：

```Python
"{$callback_url}/invoices/{$request.body.id}"
```

因此，如果您的API用户（外部开发人员）向 *your API* 发送请求至：

```
https://yourapi.com/invoices/?callback_url=https://www.external.org/events
```

JSON主体为：

```JSON
{
    "id": "2expen51ve",
    "customer": "Mr. Richie Rich",
    "total": "9999"
}
```

然后，*your API* 将处理发票，并在稍后的某个时间点将回调请求发送至 `callback_url`（*external API*）：

```
https://www.external.org/events/invoices/2expen51ve
```

包含以下内容的JSON主体：

```JSON
{
    "description": "Payment celebration",
    "paid": true
}
```

并期望来自该 *external API* 的响应，并带有JSON主体，例如：

```JSON
{
    "ok": true
}
```

!!! 提示
    注意使用的回调URL如何包含在 `callback_url` (`https://www.external.org/events`) 中作为查询参数接收的URL，以及从JSON主体内部的发票 `id`（`2expen51ve`）。 

### 添加回调路由器

此时，您在上面创建的回调路由器中需要执行 *callback path operation(s)* （ *external developer* 应在 *external API* 中实现的操作）。

现在，在 *your API's path operation decorator* 中使用参数 `callbacks`，从该回调路由器传递属性 `.routes`（实际上只是路由/*path operations* 的 `list`）：

```Python hl_lines="34"
{!../../../docs_src/openapi_callbacks/tutorial001.py!}
```

!!! 提示
    请注意，您并未将路由器本身（`invoices_callback_router`）传递给`callback=`，而是将属性 `.routes` 传递给了`invoices_callback_router.routes` 中。

### 检查文档

现在，您可以使用Uvicorn启动应用程序，然后转到<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>。

您将看到文档，其中包含 *path operation* 的“回调”部分，该部分显示了 *external API* 的外观：

<img src="/img/tutorial/openapi-callbacks/image01.png">
