# OAuth2范围

您可以将OAuth2作用域直接与 **FastAPI** 一起使用，它们已集成为无缝工作。

这样一来，您可以遵循OAuth2标准，将更细粒度的权限系统集成到您的OpenAPI应用程序（和API文档）中。

具有范围的OAuth2是许多大型身份验证提供程序使用的机制，例如Facebook，Google，GitHub，Microsoft，Twitter等。他们使用它来向用户和应用程序提供特定权限。

每次您“登录” Facebook，Google，GitHub，Microsoft，Twitter时，该应用程序都会使用具有范围的OAuth2。

在本部分中，您将看到如何在您的 **FastAPI** 应用程序中使用具有范围的相同OAuth2管理身份验证和授权。

!!! 警告
    这是一个或多或少的高级部分。如果您只是开始，则可以跳过它。

    您不一定需要OAuth2范围，并且可以根据需要处理身份验证和授权。

    但是带有范围的OAuth2可以很好地集成到您的API（使用OpenAPI）和您的API文档中。
    
    但是，您仍然可以在代码中强制执行这些范围或任何其他安全/授权要求。

    在许多情况下，带有范围的OAuth2可能会过大。
    
    但是，如果您知道自己需要它，或者您很好奇，请继续阅读。

## OAuth2范围和OpenAPI

OAuth2规范将“作用域”定义为用空格分隔的字符串列表。

每个字符串的内容可以具有任何格式，但不能包含空格。

这些范围代表“权限”。

在OpenAPI（例如API文档）中，您可以定义“安全方案”。

当这些安全方案之一使用OAuth2时，您还可以声明和使用范围。

每个“作用域”只是一个字符串（没有空格）。

它们通常用于声明特定的安全权限，例如：

* `users:read` 或 `users:write` 是通用的范例。
* `instagram_basic` 是 Facebook / Instagram 使用的。
* `https://www.googleapis.com/auth/drive` 是Google使用的。

!!! 信息
    在OAuth2中，“作用域”只是声明所需的特定权限的字符串。

    它是否有其他字符，如 `:` 还是URL。
    
    这些细节是特定于实现的。

    对于OAuth2，它们只是字符串。

## 全局视图

首先，让我们快速查看与主要 **教程-用户指南** 中的示例不同的部分，这些示例涉及[带密码的OAuth2（和哈希），带JWT令牌的Bearer](../../tutorial/security/oauth2-jwt.md){.internal-link target=_blank}。 现在使用OAuth2范围：

```Python hl_lines="2  5  9  13  47  65  106  108 109 110 111 112 113 114 115 116  122 123 124 125  129 130 131 132 133 134 135  140  154"
{!../../../docs_src/security/tutorial005.py!}
```

现在，让我们逐步查看这些更改。

## OAuth2安全方案

第一个变化是，现在我们声明具有两个可用范围（即 `me` 和 `items` ）的OAuth2安全方案。

`scopes` 参数接收一个 `dict`，每个作用域作为键，描述作为值：

```Python hl_lines="63 64 65 66"
{!../../../docs_src/security/tutorial005.py!}
```

因为我们现在正在声明这些范围，所以它们将在您登录/授权时显示在API文档中。

您将能够选择要授予访问权限的范围：`me` 和 `items` 。

这是在使用Facebook，Google，GitHub等登录时授予权限时使用的相同机制：

<img src="/img/tutorial/security/image11.png">

## 具有范围的JWT令牌

现在，修改令牌 *path operation* 以返回请求的范围。

我们仍在使用相同的 `OAuth2PasswordRequestForm`。 它包括属性 `scopes` 和 `str` 的 `list`，以及在请求中接收到的每个范围。

我们将范围作为JWT令牌的一部分返回。

!!! 危险
    为简单起见，这里我们只是将接收到的范围直接添加到令牌中。

    但是在您的应用程序中，为了安全起见，应确保仅添加用户实际具有的范围或预定义的范围。

```Python hl_lines="155"
{!../../../docs_src/security/tutorial005.py!}
```

## 声明 *path operations* 和依赖项中的作用域

现在我们声明 `/users/me/items/` 的 *path operation* 需要范围 `items`。

为此，我们从 `fastapi` 导入并使用 `Security`。

您可以使用 `Security` 来声明依赖项（就像 `Depends` 一样），但是 `Security` 也会收到带有范围（字符串）列表的参数 `scopes`。

在这种情况下，我们将依赖函数 `get_current_active_user` 传递给 `Security` （与使用 `Depends` 相同）。

但是我们还传递了一个范围的 `list`，在这种情况下，只有一个范围：`items`（它可以有更多）。

依赖函数 `get_current_active_user` 还可以声明子依赖关系，不仅可以使用 `Depends` ，还可以使用 `Security`。 声明自己的子依赖函数（`get_current_user`），以及更多的范围要求。

在这种情况下，它需要范围 `me`（可能需要多个范围）。

!!! 注意
    您不一定需要在不同的地方添加不同的范围。
    
    我们在这里这样做是为了演示 **FastAPI** 如何处理在不同级别声明的范围。

```Python hl_lines="5  140  167"
{!../../../docs_src/security/tutorial005.py!}
```

!!! 信息“技术细节”
    `Security` 实际上是 `Depends` 的子类，并且只有一个额外的参数，我们将在后面看到。

    但是通过使用 `Security` 而不是 `Depends`，**FastAPI** 将知道它可以声明安全范围，在内部使用它们，并使用OpenAPI记录API。

    但是，当您从 `fastapi` 导入 `Query`, `Path`, `Depends`, `Security` 和其他时，这些实际上是返回特殊类的函数。

## 使用 `SecurityScopes`

现在更新依赖项 `get_current_user`。

这是上面的依赖项使用的那个。

我们使用的是之前创建的OAuth2方案，将其声明为依赖项：`oauth2_scheme`。

因为此依赖项函数本身没有任何范围要求，所以我们可以将 `Depends` 与 `oauth2_scheme` 一起使用，当我们不需要指定安全范围时，就不必使用 `Security`。

我们还声明了一个类型为 `SecurityScopes` 的特殊参数，该参数是从 `fastapi.security` 导入的。

这个 `SecurityScopes` 类类似于 `Request`（请求用于直接获取 `Request`对象）。

```Python hl_lines="9  106"
{!../../../docs_src/security/tutorial005.py!}
```

## 使用 `scopes`

参数 `security_scopes` 将是 `SecurityScopes` 类型。

它将具有一个 `scopes` 属性，该属性包含一个列表，该列表包含自身所需的所有作用域以及将其用作子依赖项的所有依赖项。 这意味着，所有"dependants"...这听起来可能令人困惑，下面将在后面再次解释。

`security_scopes` 对象（ `SecurityScopes` 类的对象）还提供了一个 `scope_str` 属性，该属性带有单个字符串，其中包含用空格分隔的那些范围（我们将使用它）。

我们创建一个 `HTTPException`，稍后可以在多个地方重复使用（`raise`）。

在这种情况下，我们将所需的范围（如果有的话）包含在一个用空格分隔的字符串中（使用 `scope_str`）。 我们将包含范围的字符串放在 `WWW-Authenticate` header中（这是规范的一部分）。

```Python hl_lines="106  108 109 110 111 112 113 114 115 116"
{!../../../docs_src/security/tutorial005.py!}
```

## 验证 `username` 和数据形状

我们验证是否获得了 `username`，并提取范围。

然后，我们使用Pydantic模型验证该数据（捕获 `ValidationError` 异常），如果在读取JWT令牌或使用Pydantic验证数据时遇到错误，则会引发我们之前创建的 `HTTPException` 。

为此，我们使用新属性 `scopes` 更新了Pydantic模型 `TokenData`。

通过使用Pydantic验证数据，我们可以确保例如具有带范围的 `str` 的 `list` 和带有 `username` 的 `str` 的正好。

而不是例如 `dict` 或其他东西，因为它可能在以后某个时候中断应用程序，从而带来安全风险。

我们还验证了我们是否有一个使用该用户名的用户，否则，我们将引发之前创建的相同异常。

```Python hl_lines="47  117 118 119 120 121 122 123 124 125 126 127 128"
{!../../../docs_src/security/tutorial005.py!}
```

## 验证 `scopes`

现在，我们验证此依赖项和所有依赖项（包括 *path operations*）所需的所有作用域是否都包含在收到的令牌中提供的作用域中，否则引发 `HTTPException`。

为此，我们使用 `security_scopes.scopes`，其中包含一个 `list`，所有范围都作为 `str`。

```Python hl_lines="129 130 131 132 133 134 135"
{!../../../docs_src/security/tutorial005.py!}
```

## 依赖树和范围

让我们再次查看该依赖关系树和范围。

由于 `get_current_active_user` 依赖关系是对 `get_current_user` 的子依赖关系，因此在 `get_current_active_user` 声明的作用域 `me` 将包含在传递给 `get_current_user` 的 `security_scopes.scopes` 的必需作用域列表中。

*path operation* 本身也声明了一个范围 `items`，因此它也将在传递给 `get_current_user` 的 `security_scopes.scopes` 列表中。

依赖关系和范围的层次结构如下所示：

* *path operation* `read_own_items` 有：
    * 带有依赖项的必须范围 `["items"]`：
    * `get_current_active_user`:
        *  依赖项函数 `get_current_active_user` 有：
            * 带有依赖项的必须范围 `["me"]`：
            * `get_current_user`:
                * 依赖项函数 `get_current_user` 有：
                    * 本身不需要范围。
                    * 依赖项使用 `oauth2_scheme`.
                    * 参数 `security_scopes` 的类型是 `SecurityScopes`:
                        * `security_scopes` 参数有属性 带有 `list` 的 `scopes`  包含以上声明的所有这些范围，因此：
                            * `security_scopes.scopes` 将要包含 `["me", "items"]` for the *path operation* `read_own_items`.
                            * `security_scopes.scopes` 将要包含 `["me"]` for the *path operation* `read_users_me`, because it is declared in the dependency `get_current_active_user`.
                            * `security_scopes.scopes` 将要包含 `[]` (nothing) for the *path operation* `read_system_status`, because it didn't declare any `Security` with `scopes`, and its dependency, `get_current_user`, doesn't declare any `scope` either.

!!! 提示
    这里重要且“神奇”的事情是，`get_current_user` 将具有不同的 `scopes` 列表，以检查每个 *path operation*。

    全部取决于在每个 *path operation* 中声明的 `scopes` 以及该特定 *path operation* 在依赖关系树中的每个依赖项。

## 有关 `SecurityScopes` 的更多详细信息

您可以随时在多个位置使用 `SecurityScopes`，而不必依赖于“root”依赖项。

它将始终具有在当前 `Security` 依赖项中声明的安全范围，以及 **特定于** 路径操作和 **特定于** 依赖项树的所有依赖项。

因为 `SecurityScopes` 将具有依赖者声明的所有范围，所以您可以使用它来验证令牌在中央依赖函数中是否具有必需的范围，然后在不同的 *path operations* 中声明不同的范围要求。

对于每个 *path operations*，将对其进行独立检查。

## 核实

如果打开API文档，则可以进行身份验证并指定要授权的范围。

<img src="/img/tutorial/security/image11.png">

如果您没有选择任何范围，那么您将获得“身份验证”，但是当您尝试访问 `/users/me/` 或 `/users/me/items/` 时，您会收到一条错误消息，指出您没有足够的权限。 您仍然可以访问`/status/`。

而且，如果您选择范围 `me` 而不是范围 `items` ，则可以访问 `/users/me/`，但不能访问`/users/me/items/`。

这就是试图使用用户提供的令牌访问这些 *path operations* 中的一个的第三方应用程序会发生的情况，具体取决于用户为该应用程序赋予了多少权限。

## 关于第三方集成

在此示例中，我们使用OAuth2“密码”流程。

当我们使用自己的前端登录到自己的应用程序时，这是适当的。

因为我们可以信任它来接收我们控制的 `username` 和 `password`。

但是，如果您正在构建其他人可以连接的OAuth2应用程序（即，如果您正在构建与Facebook，Google，GitHub等等效的身份验证提供程序），则应使用其他流程之一。

最常见的是隐式流。

最安全的是代码流，但是由于需要更多步骤，因此实现起来更加复杂。 由于更为复杂，因此许多提供程序最终都会建议隐式流程。

!!! 注意
    通常，每个身份验证提供程序都以不同的方式命名其流，以使其成为其品牌的一部分。

    但最后，他们正在实现相同的OAuth2标准。

**FastAPI** 在 `fastapi.security.oauth2` 中包含用于所有这些OAuth2身份验证流的实用程序。

## 装饰器 `dependencies` 中的 `Security`

您可以在修饰器的 `dependencies` 参数中定义 `Depends` 的 `list` 的相同方法（如[路径操作装饰器中的依赖项](../../tutorial/dependencies/dependencies-in-path-operation-decorators.md){.internal-link target=_blank})，您还可以在其中将 `Security` 与 `scopes` 一起使用。
