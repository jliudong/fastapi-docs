# HTTP基本身份验证

对于最简单的情况，可以使用HTTP Basic Auth。

在HTTP Basic Auth中，应用程序需要包含用户名和密码的header。

如果未收到，则会返回HTTP 401 “未经授权”错误。

并返回带有 `Basic` 值和可选参数 `realm` 的header `WWW-Authenticate`。

告诉浏览器显示用户名和密码的集成提示。

然后，当您键入该用户名和密码时，浏览器会自动将它们发送到标题中。

## 简单的HTTP基本身份验证

* 导入 `HTTPBasic` 和 `HTTPBasicCredentials`。
* 使用 `HTTPBasic` 创建一个“ `security` 方案”。
* 在您的 *path operation* 中使用带有依赖项的 `security`。
* 返回类型为 `HTTPBasicCredentials` 的对象：
    * 它包含发送的 `username` 和 `password`。

```Python hl_lines="2 6 10"
{!../../../docs_src/security/tutorial006.py!}
```

当您尝试首次打开URL时（或单击文档中的“执行”按钮），浏览器将询问您您的用户名和密码：

<img src="/img/tutorial/security/image12.png">

## 检查用户名

这是一个更完整的示例。

使用依赖项检查用户名和密码是否正确。

为此，请使用Python标准模块<a href="https://docs.python.org/3/library/secrets.html" class="external-link" target="_blank"> `secrets` </a>检查用户名和密码：

```Python hl_lines="1  11 12 13"
{!../../../docs_src/security/tutorial007.py!}
```

这将确保 `credentials.username` 是 `“ stanleyjobson”`，并且 `credentials.password` 是 `“ swordfish”`。 这类似于：

```Python
if not (credentials.username == "stanleyjobson") or not (credentials.password == "swordfish"):
    # Return some error
    ...
```

但是通过使用 `secrets.compare_digest()`，它可以抵御称为“定时攻击”的攻击。

### 时间攻击

但是什么是“时间攻击”？

假设攻击者试图猜测用户名和密码。

然后，该攻击者发送了一个带有用户名 `johndoe` 和密码 `love123` 的请求。

然后，您的应用程序中的Python代码将等效于以下内容：

```Python
if "johndoe" == "stanleyjobson" and "love123" == "swordfish":
    ...
```

但是目前，Python会将 `johndoe` 中的第一个 `j` 与 `stanleyjobson` 中的第一个 `s` 相比较，它将返回 `False` ，因为它已经知道那两个字符串是不相同的，并认为“比较其余字母，无需浪费更多的计算时间。” 并且您的应用程序将显示“错误的用户名或密码”。

但是攻击者随后尝试使用用户名 `stanleyjobsox` 和密码 `love123`。

您的应用程序代码执行以下操作：

```Python
if "stanleyjobsox" == "stanleyjobson" and "love123" == "swordfish":
    ...
```

在意识到两个字符串不相同之前，Python必须在 `stanleyjobsox` 和 `stanleyjobson` 中比较整个 `stanleyjobso`。 因此，将花费一些额外的微秒来回复“不正确的用户或密码”。

#### 应答时间可以帮助攻击者

到那时，攻击者注意到服务器发送“错误的用户或密码”响应花费了几微秒的时间，攻击者将知道她（他）正确无误，其中一些首字母正确。

然后她/他可以再次尝试，知道它可能更像是 `stanleyjobsox`，而不是 `johndoe`。

#### “专业”攻击

当然，攻击者不会手动尝试所有这些操作，而是会编写一个程序来执行此操作，可能每秒进行数千或数百万次测试。 并且一次只会得到一个额外的正确字母。

但是这样做，在几分钟或几小时内，攻击者只要使用回答所花费的时间，便可以在应用程序的“帮助”下猜出正确的用户名和密码。

#### 使用 `secrets.compare_digest()` 来修复

但是在我们的代码中，我们实际上使用的是 `secrets.compare_digest()`。

简而言之，将 `stanleyjobsox` 与 `stanleyjobson` 进行比较所需的时间要比将 `johndoe` 与 `stanleyjobson` 进行比较所需的时间相同。 与密码相同。

这样，在您的应用程序代码中使用 `secrets.compare_digest()`，可以抵御所有范围的安全攻击。

### 返回错误

在检测到凭据不正确之后，返回状态代码为401的 `HTTPException`（没有提供凭据时返回的状态），并添加header `WWW-Authenticate`，以使浏览器再次显示登录提示：

```Python hl_lines="15 16 17 18 19"
{!../../../docs_src/security/tutorial007.py!}
```
