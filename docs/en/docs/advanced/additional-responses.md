# OpenAPI中的其他响应

!!! 警告
    这是一个相当高级的主题。

    如果您刚刚开始用**FastAPI**，可能不需要读这个章节。

您可以声明其他响应，以及其他状态代码，媒体类型，描述等。

这些其他响应将包含在OpenAPI架构中，因此它们也将出现在API文档中。

但是对于那些额外的响应，您必须确保直接返回一个诸如 `JSONResponse` 之类的 `Response` ，以及您的状态代码和内容。

## 带有 `model` 的附加响应

您可以将参数 `respons` 传递给 *path operation decorators*。

它接收一个 `dict`，密钥是每个响应的状态码，例如 `200`，而值是其他 `dict`，其中包含每个信息。

每个响应 `dict` 都可以有一个关键 `model` ，其中包含一个Pydantic模型，就像 `response_model` 一样。

**FastAPI** 将采用该模型，生成其JSON模式，并将其包含在OpenAPI中的正确位置。

例如，要声明状态代码为 `404` 和Pydantic模型为 `Message` 的另一个响应，您可以编写：

```Python hl_lines="18 23"
{!../../../docs_src/additional_responses/tutorial001.py!}
```

!!! 注意
    请记住，您必须直接返回 `JSONResponse`。

!!! 信息
    模型密钥不是OpenAPI的一部分。

    **FastAPI** 将从那里获取Pydantic模型，生成 `JSON Schema`，并将其放在正确的位置。

    正确的位置是：

    * 在 `content` 键中，该键具有另一个JSON对象（ `dict` ）作为值，该对象包含：
        * 具有媒体类型的键，例如 `application/json`，包含另一个JSON对象作为值，该对象包含：
            * 键 `schema`，具有来自模型的JSON Schema的值，这是正确的位置。
                * **FastAPI** 在此处添加对OpenAPI中另一个位置的全局JSON模式的引用，而不是直接包含它。 这样，其他应用程序和客户端可以直接使用这些JSON模式，提供更好的代码生成工具等。

在OpenAPI中为此 *path operation* 生成的响应将是：

```JSON hl_lines="3 4 5 6 7 8 9 10 11 12"
{
    "responses": {
        "404": {
            "description": "Additional Response",
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/Message"
                    }
                }
            }
        },
        "200": {
            "description": "Successful Response",
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/Item"
                    }
                }
            }
        },
        "422": {
            "description": "Validation Error",
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/HTTPValidationError"
                    }
                }
            }
        }
    }
}
```

模式被引用到OpenAPI模式内的另一个位置：

```JSON hl_lines="4 5 6 7 8 9 10 11 12 13 14 15 16"
{
    "components": {
        "schemas": {
            "Message": {
                "title": "Message",
                "required": [
                    "message"
                ],
                "type": "object",
                "properties": {
                    "message": {
                        "title": "Message",
                        "type": "string"
                    }
                }
            },
            "Item": {
                "title": "Item",
                "required": [
                    "id",
                    "value"
                ],
                "type": "object",
                "properties": {
                    "id": {
                        "title": "Id",
                        "type": "string"
                    },
                    "value": {
                        "title": "Value",
                        "type": "string"
                    }
                }
            },
            "ValidationError": {
                "title": "ValidationError",
                "required": [
                    "loc",
                    "msg",
                    "type"
                ],
                "type": "object",
                "properties": {
                    "loc": {
                        "title": "Location",
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "msg": {
                        "title": "Message",
                        "type": "string"
                    },
                    "type": {
                        "title": "Error Type",
                        "type": "string"
                    }
                }
            },
            "HTTPValidationError": {
                "title": "HTTPValidationError",
                "type": "object",
                "properties": {
                    "detail": {
                        "title": "Detail",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/ValidationError"
                        }
                    }
                }
            }
        }
    }
}
```

## 主要回应的其他媒体类型

您可以使用相同的 `responses` 参数为同一主要响应添加不同的媒体类型。

例如，您可以添加 `image/png` 的其他媒体类型，声明您的 *path operation* 可以返回JSON对象（媒体类型为 `application/json` ）或PNG图片：

```Python hl_lines="17 18 19 20 21 22 23 24 28"
{!../../../docs_src/additional_responses/tutorial002.py!}
```

!!! 注意
    注意，您必须直接使用 `FileResponse` 返回图像。

!!! 信息
     除非您在 `responses` 参数中明确指定其他媒体类型，否则FastAPI将假定该响应具有与主要响应类相同的媒体类型（默认为 `application/json`）。

     但是，如果您指定的自定义响应类的媒体类型为 `None`，则FastAPI将对任何具有关联模型的其他响应使用 `application/json`。

## 合并信息

您还可以组合来自多个位置的响应信息，包括`response_model`, `status_code`, 和 `responses`参数。

您可以使用默认状态代码 `200`（或自定义状态代码）来声明 `response_model`，然后直接在OpenAPI模式中在 `responses` 中声明同一响应的其他信息。

**FastAPI** 将保留来自 `responses` 的其他信息，并将其与模型中的JSON模式组合。

例如，您可以使用状态代码 `404` 声明响应，该代码使用Pydantic模型并具有自定义的 `description` 。

状态码为200的响应使用您的 `response_model` ，但包含自定义的 `example` ：

```Python hl_lines="20 21 22 23 24 25 26 27 28 29 30 31"
{!../../../docs_src/additional_responses/tutorial003.py!}
```

它将全部组合并包含在您的OpenAPI中，并显示在API文档中：

<img src="/img/tutorial/additional-responses/image01.png">

## 结合预定义的响应和自定义的响应

您可能希望有一些适用于许多 *path operation* 的预定义响应，但是您想将它们与每个 *path operation* 所需的自定义响应结合起来。

对于这些情况，您可以使用Python技术 `**dict_to_unpack` 来“解包” `dict`：

```Python
old_dict = {
    "old key": "old value",
    "second old key": "second old value",
}
new_dict = {**old_dict, "new key": "new value"}
```

在这里，`new_dict` 将包含 `old_dict` 中的所有键值对以及新的键值对：

```Python
{
    "old key": "old value",
    "second old key": "second old value",
    "new key": "new value",
}
```

您可以使用该技术在 *path operations* 中重用一些预定义的响应，并将其与其他自定义响应组合。

例如：

```Python hl_lines="11 12 13 14 15 24"
{!../../../docs_src/additional_responses/tutorial004.py!}
```

## 有关OpenAPI响应的更多信息

要查看您可以在响应中确切包含哪些内容，可以查看OpenAPI规范中的以下部分：

* <a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#responsesObject" class="external-link" target="_blank"> OpenAPI响应对象 </a>，其中包含“响应对象”。
* <a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#responseObject" class="external-link" target="_blank"> OpenAPI响应对象 </a>，您可以在 `responses` 参数内的每个响应中直接包含任何内容。 包括 `description`, `headers`, `content`（其中是您声明不同的媒体类型和JSON模式）和 `links`。
