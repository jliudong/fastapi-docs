# WebSockets

您可以将<a href="https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API" class="external-link" target="_blank"> WebSockets </a>与 **FastAPI** 一起使用。

## WebSockets客户端

### 生产环境

在您的生产系统中，您可能已经使用现代框架（如React，Vue.js或Angular）创建了前端。

为了使用WebSockets与您的后端进行通信，您可能会使用前端的实用程序。

或者，您可能有一个本机移动应用程序，以本机代码直接与WebSocket后端通信。

或者，您可能还有任何其他与WebSocket端点进行通信的方式。

---

但是对于这个示例，我们将使用一个非常简单的HTML文档以及一些JavaScript，它们都包含在一个长字符串中。

当然，这不是最佳选择，您不会将其用于生产。

在生产中，您将具有上述选择之一。

但这是专注于WebSockets服务器端并提供有效示例的最简单方法：

```Python hl_lines="2  6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38  41 42 43"
{!../../../docs_src/websockets/tutorial001.py!}
```

## 创建一个 `websocket`

在您的 **FastAPI** 应用程序中，创建一个 `websocket`：

```Python hl_lines="1 46 47"
{!../../../docs_src/websockets/tutorial001.py!}
```

!!! 请注意“技术细节”
    您也可以使用 `from from starlette.websockets import WebSocket`。

    **FastAPI** 直接提供相同的 `WebSocket`，只是为开发人员提供方便。 但它直接来自Starlette。

## 等待消息并发送消息

在您的WebSocket路由中，您可以 `await` 消息并发送消息。

```Python hl_lines="48 49 50 51 52"
{!../../../docs_src/websockets/tutorial001.py!}
```

您可以接收和发送二进制，文本和JSON数据。

## 试试吧

如果文件名为 `main.py` ，请使用以下命令运行应用程序：

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

打开浏览器 <a href="http://127.0.0.1:8000" class="external-link" target="_blank">http://127.0.0.1:8000</a>.

您将看到一个简单的页面类似如下：

<img src="/img/tutorial/websockets/image01.png">

您可以在输入框输入消息，并发送：

<img src="/img/tutorial/websockets/image02.png">

并且您的 **FastAPI** 应用将响应:

<img src="/img/tutorial/websockets/image03.png">

您可以直接发送若干消息：

<img src="/img/tutorial/websockets/image04.png">

并且所有这些都使用相同的WebSocket连接。

## 使用 `Depends` 和其他

在WebSocket端点中，您可以从 `fastapi` 导入并使用：

* `Depends`
* `Security`
* `Cookie`
* `Header`
* `Path`
* `Query`

它们的工作方式与其他FastAPI端点/*path operations* 相同：

```Python hl_lines="56 57 58 59 60 61  64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79"
{!../../../docs_src/websockets/tutorial002.py!}
```

!!! 信息
    在WebSocket中，引发 `HTTPException` 并没有任何意义。 因此最好直接关闭WebSocket连接。

    您可以使用<a href="https://tools.ietf.org/html/rfc6455#section-7.4.1" class="external-link" target="_blank">规范中定义的有效代码</a>。

    将来，将有一个 `WebSocketException`，您将可以从任何地方 `raise` 它，并为其添加异常处理程序。 这取决于Starlette中的<a href="https://github.com/encode/starlette/pull/527" class="external-link" target="_blank"> PR＃527 </a>。

### 尝试使用依赖项的WebSocket

如果文件名为 `main.py`，请使用以下命令运行应用程序：

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

打开浏览器 <a href="http://127.0.0.1:8000" class="external-link" target="_blank">http://127.0.0.1:8000</a>.

您可以在此处设置：

* 路径中使用的“项目ID”。
* “令牌”用作查询参数。

!!! 提示
    请注意，查询 `token` 将由依赖项处理。

这样，您可以连接WebSocket，然后发送和接收消息：

<img src="/img/tutorial/websockets/image05.png">

## 更多信息

要了解有关这些选项的更多信息，请查看Starlette的文档以了解：

* <a href="https://www.starlette.io/websockets/" class="external-link" target="_blank"> `WebSocket` 类</a>。
* <a href="https://www.starlette.io/endpoints/#websocketendpoint" class="external-link" target="_blank">基于WebSocket类的处理</a>。
