# SQL (关系型) 数据库

**FastAPI** 不需要您使用SQL（关系）数据库。

但是您可以使用所需的任何关系数据库。

在此我们来看一个使用 <a href="https://www.sqlalchemy.org/" class="external-link" target="_blank">SQLAlchemy</a>的例子。

您可以轻松地使其适应SQLAlchemy支持的任何数据库，例如：

* PostgreSQL
* MySQL
* SQLite
* Oracle
* Microsoft SQL Server, etc.

在此示例中，我们将使用**SQLite**，因为它使用单个文件并且Python集成了支持。 因此，您可以复制此示例并按原样运行它。

稍后，对于您的生产应用程序，您可能想要使用**PostgreSQL**之类的数据库服务器。

!!! 提示
    有一个官方的项目生成器，它们都基于**FastAPI**和**PostgreSQL**，它们全部基于**Docker**，包括前端和更多工具： <a href="https://github.com/tiangolo/full-stack-fastapi-postgresql" class="external-link" target="_blank">https://github.com/tiangolo/full-stack-fastapi-postgresql</a>

!!! 注意
    请注意，大多数代码是与任何框架一起使用的标准SQLAlchemy代码。

    **FastAPI**专用代码与以往一样小。

## ORM

**FastAPI**可与任何数据库和任何样式的库一起使用，以与数据库进行通信。

一种常见的模式是使用“ORM”：“对象关系映射”库。

ORM具有在代码和数据库表（"*map*"）中的*对象*之间进行转换（"*relations*"）的工具。

使用ORM，通常会创建一个表示SQL数据库中的表的类，该类的每个属性表示一个具有名称和类型的列。

例如，类 `Pet` 可以表示SQL表 `Pet` 。

并且该类的每个 *instance* 对象都代表数据库中的一行。

例如，对象 `orion_cat` （`Pet` 的实例）可以为 `type` 列具有属性 `orion_cat.type` 。 该属性的值可以是例如 `"cat"` 。

这些ORM还具有在表或实体之间建立连接或关系的工具。

这样，您还可以拥有一个属性 `orion_cat.owner` ，所有者将包含该宠物所有者的数据，该数据取自表*owners*。

因此，`orion_cat.owner.name` 可能是该宠物主人的名字（来自 `owners` 表中的 `name` 列）。

它的值可能类似于 `"Arquilian"` 。

当您尝试从宠物对象访问ORM时，ORM将完成所有工作以从相应的表 *owners* 中获取信息。

常见的ORM例如：Django-ORM（Django框架的一部分），SQLAlchemy ORM（SQLAlchemy的一部分，独立于框架）和Peewee（独立于框架），等等。

在这里，我们将了解如何使用 **SQLAlchemy ORM**。

以类似的方式，您可以使用任何其他ORM。

!!! 提示
    在文档中有一篇使用Peewee的等效文章。

## 文件结构

对于这些示例，假设您有一个名为 `my_super_project` 的目录，其中包含一个名为 `sql_app` 的子目录，其结构如下：

```
.
└── sql_app
    ├── __init__.py
    ├── crud.py
    ├── database.py
    ├── main.py
    ├── models.py
    └── schemas.py
```

文件 `__init__.py` 只是一个空文件，但它告诉Python带有所有模块的 `sql_app`（Python文件）是一个软件包。

现在，让我们看看每个文件/模块的功能。

## 创建SQLAlchemy部分

让我们参考文件 `sql_app/database.py`。

### 导入 SQLAlchemy 部分

```Python hl_lines="1 2 3"
{!../../../docs_src/sql_databases/sql_app/database.py!}
```

### 为SQLAlchemy创建数据库URL

```Python hl_lines="5 6"
{!../../../docs_src/sql_databases/sql_app/database.py!}
```

在此示例中，我们将“连接”到SQLite数据库（使用SQLite数据库打开文件）。

该文件将位于文件 `sql_app.db` 中的同一目录中。

这就是为什么最后一部分是 `./sql_app.db` 的原因。

如果您使用的是**PostgreSQL**数据库，则只需取消注释以下行：

```Python
SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"
```

...并使其适应您的数据库数据和凭据（等效于MySQL，MariaDB或其他任何数据库）。

!!! 提示

    如果要使用其他数据库，这是必须修改的主行。

### 创建SQLAlchemy `engine`

第一步是创建一个SQLAlchemy“引擎”。

稍后我们将在其他地方使用此 `engine` 。

```Python hl_lines="8 9 10"
{!../../../docs_src/sql_databases/sql_app/database.py!}
```

#### 注释

参数：

```Python
connect_args={"check_same_thread": False}
```

...仅对于 `SQLite` 才需要。 其他数据库不需要它。

!!! 信息“技术细节”

    默认情况下，假定每个线程将处理一个独立的请求，SQLite将仅允许一个线程与其通信。

    这是为了防止为不同的事物（针对不同的请求）意外共享同一连接。

    但是在FastAPI中，使用正常功能（`def`），一个以上的线程可以与数据库进行同一请求的交互，因此我们需要使SQLite知道它应该允许使用 `connect_args={"check_same_thread": False}` 。

    另外，我们将确保每个请求都以依赖关系获取其自己的数据库连接会话，因此不需要该默认机制。

### 创建一个 `SessionLocal` 类

`SessionLocal` 类的每个实例将是一个数据库会话。 该类本身还不是数据库会话。

但是，一旦我们创建了 `SessionLocal` 类的实例，该实例将成为实际的数据库会话。

我们将其命名为 `SessionLocal` ，以区别于我们从SQLAlchemy导入的 `Session`。

稍后我们将使用 `Session`（从SQLAlchemy导入的会话）。

要创建 `SessionLocal` 类，请使用 `sessionmaker` 函数：

```Python hl_lines="11"
{!../../../docs_src/sql_databases/sql_app/database.py!}
```

### 创建一个 `Base` 类

现在，我们将使用函数 `declarative_base()` 返回一个类。

稍后，我们将从该类继承以创建每个数据库模型或类（ORM模型）：

```Python hl_lines="13"
{!../../../docs_src/sql_databases/sql_app/database.py!}
```

## 创建数据库模型

现在让我们看一下文件 `sql_app/models.py`。

### 从Base类创建SQLAlchemy模型

我们将使用之前创建的 `Base` 类来创建SQLAlchemy模型。

!!! 提示
    SQLAlchemy使用术语“**模型**”来指代与数据库交互的这些类和实例。

    但是Pydantic还使用术语“**模型**”来指代不同的东西，即数据验证，转换以及文档类和实例。

从 `database` 中导入 `Base` （从上方导入文件 `database.py` ）。

创建从其继承的类。

这些类是SQLAlchemy模型。

```Python hl_lines="4 7 8 18 19"
{!../../../docs_src/sql_databases/sql_app/models.py!}
```

`__tablename__` 属性告诉SQLAlchemy这些模型中每个模型在数据库中使用的表的名称。

### 创建模型属性/列

现在创建所有模型（类）属性。

这些属性中的每一个都代表其相应数据库表中的一列。

我们使用SQLAlchemy的 `Column` 作为默认值。

然后，我们传递一个SQLAlchemy类“类型”，作为 `Integer`, `String`, 和 `Boolean` ，它们将数据库中的类型定义为参数。

```Python hl_lines="1 10 11 12 13 21 22 23 24"
{!../../../docs_src/sql_databases/sql_app/models.py!}
```

### 创建关系

现在创建关系。

为此，我们使用SQLAlchemy ORM提供的 `relationship`。

这或多或少将成为一个“魔术”属性，其中将包含与此表相关的其他表中的值。

```Python hl_lines="2 15 26"
{!../../../docs_src/sql_databases/sql_app/models.py!}
```

当访问 `User` 中的 `items` 属性时，如 `my_user.items` 中一样，它将有一个 `items` SQLAlchemy模型列表（来自 `items` 表），该模型具有指向此记录的外键 `users` 表。

当您访问 `my_user.items` 时，SQLAlchemy实际上会去从 `items` 表中的数据库中获取项目，并在此处填充它们。

当访问 `items` 中的属性 `owner` 时，它将包含来自 `users` 表中的 `user` SQLAlchemy模型。 它将使用 `owner_id` 属性/列及其外键来知道要从 `users` 表中获取哪条记录。

## 创建Pydantic模型

现在让我们检查文件 `sql_app/schemas.py`。

!!! 提示
    为了避免SQLAlchemy模型和Pydantic模型之间的混淆，我们将在SQLAlchemy模型中使用文件 `models.py` ，在Pydantic模型中使用文件`schemas.py`。

    这些Pydantic模型或多或少定义了“模式”（有效数据形状）。
    
    因此，这将有助于我们避免在同时使用两者时产生混淆。

### 创建初始Pydantic *模型* /模式

创建一个 `ItemBase` 和 `UserBase` Pydantic *模型*（或称“方案”）以在创建或读取数据时具有共同的属性。

并创建一个从它们继承的 `ItemCreate` 和 `UserCreate`（这样它们将具有相同的属性），以及创建所需的任何其他数据（属性）。

因此，用户在创建密码时也会有一个 `password`。

但是为了安全起见，例如，`password` 不会在其他Pydantic *模型*中使用，在读取用户时不会从API发送。

```Python hl_lines="3 6 7 8 11 12 23 24 27 28"
{!../../../docs_src/sql_databases/sql_app/schemas.py!}
```

#### SQLAlchemy风格和Pydantic风格

注意，SQLAlchemy * models *使用`=`定义属性，并将类型作为参数传递给`Column`，例如：

```Python
name = Column(String)
```

Pydantic * models *使用`:`声明类型时，新的类型注释语法/类型提示：

```Python
name: str
```

记住这一点，因此在将它们一起使用 `=` 和 `:` 时，不要感到困惑。

### 创建Pydantic *模型* /模式以读取/返回

现在创建Pydantic *models*（方案），该方案在读取数据以及从API返回数据时将使用。

例如，在创建项目之前，我们不知道分配给它的ID是什么，但是在读取它（从API返回）时，我们已经知道它的ID。

同样，当读取用户时，我们现在可以声明 `items` 将包含属于该用户的项目。

不仅是这些项目的ID，还有我们在Pydantic *model* 中定义的用于读取项目的所有数据：`Item`。

```Python hl_lines="15 16 17 31 32 33 34"
{!../../../docs_src/sql_databases/sql_app/schemas.py!}
```

!!! 提示
    请注意，在读取用户（从API返回）时将使用的 `User` ，Pydantic *model* 不包含 `password` 。

### 使用Pydantic的 `orm_mode`

现在，在Pydantic的 *models*（用于读取 `Item` 和 `User` ）中，添加一个内部的 `Config` 类。

这个 <a href="https://pydantic-docs.helpmanual.io/#config" class="external-link" target="_blank">`Config`</a> 类用于为Pydantic提供配置。

在 `Config` 类中，设置属性 `orm_mode = True` 。

```Python hl_lines="15 19 20 31 36 37"
{!../../../docs_src/sql_databases/sql_app/schemas.py!}
```

!!! 提示
    注意，它使用 `=` 来赋值，例如：

    `orm_mode = True`

    之前的类型声明不使用`:`。

    这是在设置配置值，而不是声明类型。

Pydantic的 `orm_mode` 会告诉Pydantic *model* 读取数据，即使它不是 `dict` 而是ORM模型（或任何其他具有属性的任意对象）。

这样，而不是仅尝试从 `dict` 中获取 `id` 值，如下所示：

```Python
id = data["id"]
```

它还将尝试从属性获取它，如：

```Python
id = data.id
```

有了这个，Pydantic *model*就可以与ORM兼容了，您可以在path操作中的 `response_model` 参数中声明它。

您将能够返回数据库模型，并且它将从中读取数据。

#### 有关ORM模式的技术详细信息

SQLAlchemy和许多其他默认情况下是“延迟加载”。

例如，这意味着除非您尝试访问包含该数据的属性，否则它们不会从数据库中获取关系数据。

例如，访问属性 `items`：

```Python
current_user.items
```

可以使SQLAlchemy转到 `items` 表并获取该用户的项目，但不能早于此。

没有 `orm_mode`，如果您从 *path操作* 返回了一个SQLAlchemy模型，它将不包含关系数据。

即使您在Pydantic模型中声明了这些关系。

但是在ORM模式下，由于Pydantic本身会尝试从属性访问其所需的数据（而不是假设 `dict`），因此您可以声明要返回的特定数据，甚至可以继续获取它，来自ORM。

## CRUD 工具

现在让我们看一下文件 `sql_app/crud.py`。

在此文件中，我们将具有可重用的功能来与数据库中的数据进行交互。

**CRUD**来自：**C** 创建，**R** 读取，**U** 更新和**D** 删除。

...尽管在此示例中，我们仅创建和阅读。

### 读取数据

从 `sqlalchemy.orm` 导入 `Session` ，这将允许您声明db参数的类型，并在函数中进行更好的类型检查和完成。

导入`models`（SQLAlchemy模型）和`schemas`（Pydantic *models* /模式）。

创建实用程序函数以：

* 通过ID和电子邮件读取单个用户。
* 读取多个用户。
* 阅读单个项目。

```Python hl_lines="1 3 6 7 10 11 14 15 27 28"
{!../../../docs_src/sql_databases/sql_app/crud.py!}
```

!!! 提示
    通过创建仅专用于与 *path操作函数* 无关的与数据库交互（获取用户或项目）的函数，您可以更轻松地在多个部分中重用它们，还可以添加<abbr title="自动测试，编写在代码中，检查另一段代码是否正常工作。">对其进行单元测试</abbr>。

### 创建数据

现在创建实用程序函数来创建数据。

这些步骤是：

* 使用数据创建一个SQLAlchemy模型 *instance*。
* 将该实例对象添加到数据库会话中。
* 将更改提交到数据库（以便将其保存）。
* 刷新您的实例（以便它包含数据库中的任何新数据，例如生成的ID）。

```Python hl_lines="18 19 20 21 22 23 24 31 32 33 34 35 36"
{!../../../docs_src/sql_databases/sql_app/crud.py!}
```

!!! 提示
    `User` 的SQLAlchemy模型包含一个 `hashed_password` ，该密码应包含密码的安全散列版本。

    但是由于API客户端提供的是原始密码，因此您需要提取原始密码并在应用程序中生成哈希密码。

    然后将值 `hashed_password` 传递给保存的值。

!!! 警告
    此示例不安全，密码不进行哈希处理。

    在现实生活中的应用程序中，您将需要对密码进行哈希处理，并且永远不要将其保存为纯文本格式。

    有关更多详细信息，请返回教程中的“安全性”部分。

    在这里，我们仅关注数据库的工具和机制。

!!! 提示
    我们没有将每个关键字参数传递给 `Item` 并从Pydantic *model* 中读取每个参数，而是使用Pydantic *model* 的数据生成了一个 `dict`，其包含：

    `item.dict()`

    然后我们将 `dict` 的键值对作为关键字参数传递给SQLAlchemy `Item`，并带有：

    `Item(**item.dict())`

    然后，我们传递Pydantic *model* 未提供的额外关键字参数 `owner_id` ，并带有：

    `Item(**item.dict(), owner_id=user_id)`

## **FastAPI**的主入口程序应用

现在，在文件 `sql_app/main.py` 中，我们可以集成和使用之前创建的所有其他部分。

### 创建数据库表

以非常简单的方式创建数据库表：

```Python hl_lines="9"
{!../../../docs_src/sql_databases/sql_app/main.py!}
```

#### Alembic注释

通常，您可能会使用<a href="https://alembic.sqlalchemy.org/en/latest/" class="external-link" target="_blank"> Alembic </a>来初始化数据库（创建表等）。

而且您还将Alembic用于“迁移”（这是它的主要工作）。

“迁移”是每当您更改SQLAlchemy模型的结构，添加新属性等以在数据库中复制这些更改，添加新列，新表等时所需的一组步骤。

您可以在[Project Generation - Template](../project-generation.md){.internal-link target=_blank}中的模板中找到FastAPI项目中的Alembic示例。 具体在<a href="https://github.com/tiangolo/full-stack-fastapi-postgresql/tree/master/%7B%7Bcookiecutter.project_slug%7D%7D/backend/app/alembic/" class="external-link" target="_ blank">源代码中的 `alembic` 目录</a>。

### 创建依赖

!!! 信息
    为此，您需要使用**Python 3.7**或更高版本，或者在**Python 3.6**中安装“反向端口”：

    ```console
    $ pip install async-exit-stack async-generator
    ```

    这会安装<a href="https://github.com/sorcio/async_exit_stack" class="external-link" target="_blank"> async-exit-stack </a>和<a href="https://github.com/python-trio/async_generator" class="external-link" target="_ blank"> async-generator </a>。

    您还可以将替代方法与最后说明的“中间件”一起使用。

现在使用在 `sql_app/databases.py` 文件中创建的 `SessionLocal` 类来创建依赖项。

我们需要每个请求有一个独立的数据库会话/连接（`SessionLocal`），在所有请求中使用相同的会话，然后在请求完成后关闭它。

然后将为下一个请求创建一个新会话。

为此，我们将使用 `yield` 创建一个新的依赖项，如前面在[`yield`的依赖项](dependencies/dependencies-with-yield.md){.internal-link target=_blank}所述。

我们的依赖关系将创建一个新的SQLAlchemy`SessionLocal`，它将在单个请求中使用，然后在请求完成后将其关闭。

```Python hl_lines="15 16 17 18 19 20"
{!../../../docs_src/sql_databases/sql_app/main.py!}
```

!!! 信息
    我们将创建 `SessionLocal()` 和处理请求放入 `try` 块中。

    然后，在 `finally` 块中将其关闭。
    
    这样，我们确保在请求后数据库会话始终关闭。 即使在处理请求时出现异常。

    但是您不能从退出代码中引发另一个异常（在`yield`之后）。 请参阅[带有`yield`和`HTTPException`的依赖项](./dependencies/dependencies-with-yield.md#dependencies-with-yield-and-httpexception){.internal-link target=_blank}

然后，在*path操作函数*中使用依赖项时，我们使用直接从SQLAlchemy导入的 `Session` 类型声明它。

这将为我们在*path操作函数*中提供更好的编辑器支持，因为编辑器将知道`db`参数的类型为 `Session`：

```Python hl_lines="24  32  38  47  53"
{!../../../docs_src/sql_databases/sql_app/main.py!}
```

!!! 信息“技术细节”
    参数 `db` 实际上是 `SessionLocal` 类型，但是此类（使用 `sessionmaker()` 创建）是SQLAlchemy `Session` 的“代理”，因此，编辑器实际上并不知道提供了哪些方法。 

    但是通过将类型声明为 `Session`，编辑器现在可以知道可用的方法（`.add()`, `.query()`, `.commit()`等），并且可以提供更好的支持（例如完成 ）。 类型声明不影响实际对象。

### 创建**FastAPI** *路径操作*

现在，最后是标准的**FastAPI** *路径操作*代码。

```Python hl_lines="23 24 25 26 27 28  31 32 33 34  37 38 39 40 41 42  45 46 47 48 49  52 53 54 55"
{!../../../docs_src/sql_databases/sql_app/main.py!}
```

我们在依赖项中的每个请求前使用 `yield` 创建数据库会话，然后在之后关闭它。

然后，我们可以在“路径操作函数”中创建所需的依赖关系，以直接获取该会话。

这样，我们可以直接从“路径操作函数”内部调用`crud.get_user`并使用该会话。

!!! 提示
    请注意，您返回的值是SQLAlchemy模型或SQLAlchemy模型的列表。

    但是，由于所有 *path操作* 都具有使用 `orm_mode` 的Pydantic *models* /模式的 `response_model`，因此将从它们中提取Pydantic模型中声明的数据，并返回给客户端，并进行所有常规过滤和验证。

!!! 提示
    还要注意，有些具有标准Python类型的 `response_models`，如 `List[schemas.Item]` 。

    但是，由于该 `List` 的内容/参数是带有 `orm_mode` 的Pydantic *model*，因此将正常检索数据并将其返回给客户端，而不会出现问题。

### 关于 `def` 与 `sync def`

在这里，我们在 *path操作函数* 内和依赖项中使用SQLAlchemy代码，然后它将与外部数据库进行通信。

那可能需要一些“等待”。

但是由于SQLAlchemy不兼容直接使用 `await`，因此类似于：

```Python
user = await db.query(User).first()
```

...相反，我们使用的是：

```Python
user = db.query(User).first()
```

然后，我们应该声明 *path operation functions* 和不带 `async def` 的依赖项，而仅使用普通的 `def` ，如下所示：

```Python hl_lines="2"
@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    ...
```

!!! 注意“非常技术细节”
    如果您好奇并且有很深的技术知识，可以在[Async](../async.md#very-technical-details){.internal-link target=_blank}中查看有关如何处理 `async def` 和 `def` 的非常技术细节文档。

## 迁移

因为我们直接使用SQLAlchemy，并且不需要任何插件即可与**FastAPI**配合使用，所以我们可以集成数据库<abbr title="自动更新数据库以拥有我们定义的任何新列 我们的模型。">迁移</abbr>直接与<a href="https://alembic.sqlalchemy.org" class="external-link" target="_blank"> Alembic </a>。

而且，由于与SQLAlchemy和SQLAlchemy模型相关的代码位于单独的独立文件中，因此您甚至可以使用Alembic执行迁移，而无需安装FastAPI，Pydantic或其他任何工具。

以同样的方式，您将能够在与**FastAPI**不相关的代码的其他部分中使用相同的SQLAlchemy模型和实用程序。

例如，在后台任务工作者中使用 <a href="http://www.celeryproject.org/" class="external-link" target="_blank">Celery</a>, <a href="https://python-rq.org/" class="external-link" target="_blank">RQ</a>, or <a href="https://arq-docs.helpmanual.io/" class="external-link" target="_blank">ARQ</a>.

## 查看所有文件

记住，您应该有一个名为 `my_super_project` 的目录，其中包含一个名为 `sql_app` 的子目录。

sql_app应该具有以下文件：

* `sql_app/__init__.py`：是一个空文件。

* `sql_app/database.py`：

```Python
{!../../../docs_src/sql_databases/sql_app/database.py!}
```

* `sql_app/models.py`:

```Python
{!../../../docs_src/sql_databases/sql_app/models.py!}
```

* `sql_app/schemas.py`:

```Python
{!../../../docs_src/sql_databases/sql_app/schemas.py!}
```

* `sql_app/crud.py`:

```Python
{!../../../docs_src/sql_databases/sql_app/crud.py!}
```

* `sql_app/main.py`:

```Python
{!../../../docs_src/sql_databases/sql_app/main.py!}
```

## 核实

您可以复制此代码并按原样使用。

!!! 信息

    实际上，此处显示的代码是测试的一部分。 就像这些文档中的大多数代码一样。

然后，您可以使用Uvicorn运行它：

<div class="termy">

```console
$ uvicorn sql_app.main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

然后，可以通过浏览器查看 <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>。

您将能够与**FastAPI**应用程序进行交互，并从真实数据库中读取数据：

<img src="/img/tutorial/sql-databases/image01.png">

## 直接与数据库进行交互

如果要独立于FastAPI直接浏览SQLite数据库（文件）以调试其内容，添加表，列，记录，修改数据等，则可以使用<a href="https://sqlitebrowser.org/" class="external-link" target="_blank">用于SQLite的数据库浏览器</a>。

它看起来像这样：

<img src="/img/tutorial/sql-databases/image02.png">

还可以通过在线方式使用 SQLite browser： <a href="https://inloop.github.io/sqlite-viewer/" class="external-link" target="_blank">SQLite Viewer</a> 或 <a href="https://extendsclass.com/sqlite-browser.html" class="external-link" target="_blank">ExtendsClass</a>.

## 中间件的备用数据库会话

如果您不能将依赖关系与 `yield` 配合使用-例如，如果您未使用**Python 3.7**，并且无法安装上述针对**Python 3.6**的“反向端口” -- 您可以设置以类似的方式在“中间件”中建立会话。

“中间件”基本上是始终针对每个请求执行的功能，其中某些代码在端点功能之前执行，而某些代码在端点功能之后执行。

### 创建中间件

我们将添加的中间件（只是一个函数）将为每个请求创建一个新的SQLAlchemy `SessionLocal`，将其添加到请求中，然后在请求完成后将其关闭。

```Python hl_lines="14 15 16 17 18 19 20 21 22"
{!../../../docs_src/sql_databases/sql_app/alt_main.py!}
```

!!! 信息
    我们将创建 `SessionLocal()` 和处理请求放入 `try` 块中。

    然后，在 `finally` 块中将其关闭。
    
    这样，我们确保在请求后数据库会话始终关闭。 即使在处理请求时出现异常。

### 关于 `request.state`

`request.state` 是每个 `Request` 对象的属性。 它可以存储附加到请求本身的任意对象，例如本例中的数据库会话。 您可以在<a href="https://www.starlette.io/requests/#other-state" class="external-link" target="_blank"> Starlette关于 `Request` 状态的文档中阅读更多相关内容</a>。

在这种情况下，对于我们来说，这有助于我们确保在所有请求中使用单个数据库会话，然后再关闭（在中间件中）。

### 与 `yield` 或中间件的依赖关系

在此处添加 **中间件** 类似于 `yield` 的依赖项，但有一些区别：

* 它需要更多代码，并且稍微复杂一些。
* 中间件必须是一个 `async` 功能。
    * 如果其中包含必须“等待”网络的代码，则可能会“阻止”您的应用程序，从而降低性能。
    * 尽管在这里 `SQLAlchemy` 的工作方式可能不是很成问题。
    * 但是，如果您向具有很多<abbr title="input and output"> I/O </abbr>等待的中间件添加了更多代码，则可能会出现问题。
* 中间件针对 “每个” 请求运行。
    * 因此，将为每个请求创建一个连接。
    * 即使处理该请求的*path操作*不需要DB。

!!! 提示
    当依赖关系足以满足用例时，最好将其与 `yield` 一起使用。

!!! 信息
    最近，在 **FastAPI** 中添加了带有 `yield` 的依赖项。

    本教程的先前版本仅包含带有中间件的示例，并且可能有多个应用程序使用中间件进行数据库会话管理。
