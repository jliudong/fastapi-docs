# 路径参数和数值校验

可以使用 `Query` 为查询参数声明更多的验证和元数据，也可以使用 `Path` 为路径参数声明相同的验证和元数据。

## 导入 Path

首先，import `Path` from `fastapi`:

```Python hl_lines="1"
{!../../../docs_src/path_params_numeric_validations/tutorial001.py!}
```

## 声明元数据

您可以声明与 `Query` 相同的所有参数。

例如，要声明路径参数 `item_id` 的 `title` 元数据值，您可以输入：

```Python hl_lines="8"
{!../../../docs_src/path_params_numeric_validations/tutorial001.py!}
```

!!! 注释
    始终需要一个路径参数，因为路径参数是路径的一个必要组成部分。
    
    因此，您应该使用 `...` 对其进行声明，以根据需要对其进行标记。

    但是，即使使用 `None` 声明它或设置默认值，这不会有任何变化，它仍然是一个必需的参数。

## 根据需要来排序参数

假设要声明查询参数 `q` 作为必需的 `str` 。

而且不需要为该参数声明其他任何内容，因此实际上不需要使用 `Query` 。

但是仍然需要对 `item_id` 路径参数使用 `Path` 。

Python不允许将带有“default”的值放在没有“default”的值之前。

但是可以对其重新排序，并首先获得不带默认值的值（查询参数 `q` ）。

顺序对于 **FastAPI** 是无关紧要。因为它将通过参数的名称，类型和默认声明（`Query`, `Path`等）来检测参数，而不关心顺序。

因此，可以将函数声明为：

```Python hl_lines="8"
{!../../../docs_src/path_params_numeric_validations/tutorial002.py!}
```

## 根据需要来排序参数，技巧

如果要声明不带 `Query` 或任何默认值的 `q` 查询参数和使用 `Path` 的路径参数 `item_id` 并使用不同的顺序，Python语言对此有一些特殊语法支持的。

传递 `*` 作为函数的第一个参数。

Python语言中不会对该 `*` 做任何事情，但是它将知道以下所有参数都应称为关键字参数（键/值对），也称为<abbr title="From: K-ey W-ord Arg-uments"><code>kwargs</code></abbr>。 即使它们没有默认值。

```Python hl_lines="8"
{!../../../docs_src/path_params_numeric_validations/tutorial003.py!}
```

## 数值验证：大于或等于

使用 `Query` 和 `Path` （以及其他功能，您将在后面看到）可以声明字符串约束，但也可以声明数量约束。

在这里，当 `ge = 1` 时，`item_id` 必须是一个整数，且大于等于 `1`。

```Python hl_lines="8"
{!../../../docs_src/path_params_numeric_validations/tutorial004.py!}
```

## 数值验证：大于和小于或等于

The same applies for:

* `gt`: 大于
* `le`: 小于等于

```Python hl_lines="9"
{!../../../docs_src/path_params_numeric_validations/tutorial005.py!}
```

## 数值验证：浮点数，大于和小于

数值验证同样可以应用于 `float` 类型数值。

在这里，必须声明 <abbr title="大于"><code>gt</code></abbr> 而不是 <abbr title="大于等于"><code>ge</code></abbr> 变得很重要。 与之类似，例如，可以要求一个值必须大于0，即使它小于1。

因此，`0.5` 将是有效值。 但 `0.0` 或 `0` 则不是有效值。

这与 <abbr title="less than"><code>lt</code></abbr> 是等效的。

```Python hl_lines="11"
{!../../../docs_src/path_params_numeric_validations/tutorial006.py!}
```

## 总结

使用 `Query`, `Path` （以及其他尚未看到的），可以使用与 [查询参数和字符串验证](query-params-str-validations.md){.internal-link target=_blank} 相同的方式声明元数据和字符串验证。

还可以声明数值验证：

* `gt`: 大于
* `ge`: 大于等于
* `lt`: 小于
* `le`: 小于等于

!!! 信息
    `Query`, `Path` 和其他将在稍后看到的常见 `Param` 类的子类（不需要使用）。

    它们都共享您已经看到的附加验证和元数据的所有相同参数。

!!! 注释 "技术细节"
    当从 `fastapi` 导入 `Query`, `Path` 以及其他的，这些实际上就是个函数。

    当被调用时，返回同名类的实例。

    因此，您将导入 `Query` ，这是一个函数。 当您调用它时，它将返回一个类的实例，该类也命名为 `Query` 。

    这里是函数（而不是直接使用类），以便编辑器不会标记有关其类型的错误。

    这样就可以使用常规的编辑器和编码工具，而不必添加自定义配置来忽略这些错误。
