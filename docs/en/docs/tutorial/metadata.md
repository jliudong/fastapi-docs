# 元数据和文档网址

您可以在 **FastAPI** 应用程序中自定义几种元数据配置。

## 标题，描述和版本

您可以设置：

* **标题**：在OpenAPI和自动API文档用户界面中用作API的标题/名称。
* **描述**：在OpenAPI和自动API文档用户界面中对API的描述。
* **版本**：您的API版本，例如 v2或2.5.0。
    * 有用，例如，如果您具有该应用程序的先前版本，也使用OpenAPI。

要设置它们，请使用参数 `title`，`description`和`version`：

```Python hl_lines="4 5 6"
{!../../../docs_src/metadata/tutorial001.py!}
```

使用此配置，自动API文档将如下所示：

<img src="/img/tutorial/metadata/image01.png">

## 标签的元数据

您还可以使用参数 `openapi_tags` 为用于将路径操作分组的不同标签添加其他元数据。

它需要一个列表，其中每个标签包含一个字典。

每个字典可以包含：

* `name`（**必填**）：具有与您在 *path operations* 和 `APIRouter` 中的 `tags` 参数中使用的标签名称相同的 `str`。
* `description`：带有字符串简短说明的 `str`。 它可以具有Markdown，并将显示在docs UI中。
* `externalDocs`：一个 `dict` 描述外部文档，其中：
    * `description`：带有外部文档简短说明的 `str`。
    * `url`（**必填**）：带有外部文档URL的 `str`。

### 为标签创建元数据

让我们在带有 `users` 和 `items` 标签的示例中进行尝试。

为标签创建元数据，并将其传递给 `openapi_tags` 参数：

```Python hl_lines="3 4 5 6 7 8 9 10 11 12 13 14 15 16  18"
{!../../../docs_src/metadata/tutorial004.py!}
```

请注意，您可以在说明内使用Markdown，例如，“登录”将以粗体显示（**login**），而“花式”将以斜体显示（_fancy_）。

!!! 提示
    您不必为所有使用的标签添加元数据。

### 使用您的标签

在您的 *path operations*（和 `APIRouter`）中使用 `tags` 参数将它们分配给不同的标签：

```Python hl_lines="21  26"
{!../../../docs_src/metadata/tutorial004.py!}
```

!!! 信息
     在[路径操作配置](../path-operation-configuration/#tags){.internal-link target=_blank}中阅读有关标签的更多信息。

### 检查文档

现在，如果您检查文档，它们将显示所有其他元数据：

<img src="/img/tutorial/metadata/image02.png">

### 标签顺序

每个标签元数据字典的顺序还定义了docs UI中显示的顺序。

例如，即使 `users` 按字母顺序位于 `items` 之后，它也会显示在它们前面，因为我们将其元数据添加为列表中的第一个字典。

## OpenAPI URL

默认情况下，OpenAPI架构位于`/openapi.json`。

但是您可以使用参数 `openapi_url` 对其进行配置。

例如，将其设置为在 `/api/v1/openapi.json` 中投放：

```Python hl_lines="3"
{!../../../docs_src/metadata/tutorial002.py!}
```

如果要完全禁用OpenAPI模式，可以设置 `openapi_url=None`，这还将禁用使用它的文档用户界面。

## 文件网址

您可以配置包括的两个文档用户界面：

* **Swagger UI**：在`/docs`中提供。
    * 您可以使用参数 `docs_url` 设置其URL。
    * 您可以通过设置 `docs_url=None` 禁用它。
* ReDoc：在 `/redoc` 中提供。
    * 您可以使用参数 `redoc_url` 设置其URL。
    * 您可以通过设置 `redoc_url=None` 来禁用它。

例如，将Swagger UI设置为在 `/documentation` 服务并禁用ReDoc：

```Python hl_lines="3"
{!../../../docs_src/metadata/tutorial003.py!}
```
