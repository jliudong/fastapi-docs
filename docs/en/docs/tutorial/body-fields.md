# 请求体 - 字段

可以使用 `Query`, `Path` 和 `Body` 在 *path operation function（路径操作函数）* 参数中声明其他验证和元数据的方式相同，也可以使用Pydantic的 `Field` 在Pydantic模型中声明验证和元数据。

## 导入 `Field`

首先需要导入 `Field` ：

```Python hl_lines="2"
{!../../../docs_src/body_fields/tutorial001.py!}
```

!!! 警告
    注意 `Field` 是直接从 `pydantic` 导入的，而非像`Query`, `Path`, `Body`等，是从 `fastapi` 导入的。

## 声明模型属性

可以结合模型属性来使用 `Field` ：

```Python hl_lines="9 10"
{!../../../docs_src/body_fields/tutorial001.py!}
```

`Field` 与 `Query`, `Path` 和 `Body` 具有相同的工作方式，比如他们都有相同的参数等。

!!! 注释 "技术细节"
    实际上， `Query`, `Path` 和接下来看到的一些都是通用类 `Param` 的子类。而 `Field` 则是Pydantic中 `FieldInfo` 类的子类。

    Pydantic的 `Field` 也会返回 `FieldInfo` 的实例。

    `Body` 也直接返回 `FieldInfo` 子类对象。接下来看到的一些也是 `Body` 类的子类。

    记住当从 `fastapi` 导入 `Query`, `Path` 或其他的时，实际上是返回了那些特殊类的函数。

!!! 提示
    请注意，每个具有类型、默认值和 `Field` 的模型的属性与 *path operation function(路径操作函数）* 的参数结构相同，只是使用 `Field` 而不是 `Path` ，`Query` 和 `Body`。

## 添加额外的信息

可以在 `Field`, `Query`, `Body`等中声明额外信息。这些内容将被包含在产生的JSON模型中。

在学习声明示例时，您将在文档的后面部分学到更多有关添加额外信息的信息。

## 总结

可以使用Pydantic的 `Field` 来为模型属性声明额外验证和元数据。

也可以使用额外的关键词参数来传递附加的JSON模型元数据。
