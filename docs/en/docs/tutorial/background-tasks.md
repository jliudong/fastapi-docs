# 后台任务

您可以定义要在返回响应后运行的后台任务。

这对于在请求后需要进行的操作很有用，但是客户端实际上并不需要在收到响应之前就等待操作完成。

例如，这包括：

* 执行操作后发送的电子邮件通知：
    * 由于连接到电子邮件服务器并发送电子邮件通常很“慢”（几秒钟），因此您可以立即返回响应并在后台发送电子邮件通知。
* 处理数据：
    * 例如，假设您收到的文件必须经过缓慢的处理，您可以返回“已接受”（HTTP 202）响应，并在后台对其进行处理。

## 使用 `BackgroundTasks`

首先，导入 `BackgroundTasks` 并在 *path operation function* 中定义一个带有 `BackgroundTasks` 类型声明的参数：

```Python hl_lines="1 13"
{!../../../docs_src/background_tasks/tutorial001.py!}
```

**FastAPI** 将为您创建 `BackgroundTasks` 类型的对象，并将其作为该参数传递。

## 创建任务功能

创建一个要作为后台任务运行的函数。

它只是可以接收参数的标准功能。

它可以是一个 `async def` 或普通的 `def` 功能，**FastAPI** 会知道如何正确处理它。

在这种情况下，任务功能将写入文件（模拟发送电子邮件）。

由于写操作不使用 `async` 和 `await`，我们用普通的 `def` 定义了该函数：

```Python hl_lines="6 7 8 9"
{!../../../docs_src/background_tasks/tutorial001.py!}
```

## 添加后台任务

在您的 *path operation function* 内部，使用 `.add_task()` 方法将您的任务函数传递给 *background tasks* 对象：

```Python hl_lines="14"
{!../../../docs_src/background_tasks/tutorial001.py!}
```

`.add_task()` 接收作为参数：

* 任务功能在后台运行（`write_notification`）。
*应该按顺序（`email`）传递给任务函数的任何参数序列。
*应该传递给任务功能的所有关键字参数（`message="some notification"`）。

## 依赖注入

使用 `BackgroundTasks` 还可与依赖项注入系统一起使用，您可以在多个级别上声明 `BackgroundTasks` 类型的参数：在 *path operation function* 中，在依赖项中（依赖），在子依赖项中等等。

**FastAPI** 知道每种情况下的操作以及如何重用同一对象，以便所有后台任务合并在一起，然后在后台运行：

```Python hl_lines="11 14 20 23"
{!../../../docs_src/background_tasks/tutorial002.py!}
```

在这个例子中，消息将在发送响应之后被写入 `log.txt` 文件中。

如果请求中有查询，它将在后台任务中写入日志。

然后，在 *path operation function* 处生成的另一个后台任务将使用`email` path参数写入一条消息。

## 技术细节

类 `BackgroundTasks` 直接来自<a href="https://www.starlette.io/background/" class="external-link" target="_blank"> `starlette.background` </a>。

它直接导入/包含在FastAPI中，因此您可以从 `fastapi` 导入它，并避免从 `starlette.background` 意外导入替代的 `BackgroundTask`（末尾没有s）。

通过仅使用 `BackgroundTasks`（而不是 `BackgroundTask`），便可以将其用作 *path operation function* 路径参数，并让 **FastAPI** 为您处理其余部分，就像直接使用 `Request` 对象时一样。

在FastAPI中仍然可以单独使用 `BackgroundTask`，但是您必须在代码中创建对象并返回包含它的Starlette `Response`。

您可以在<a href="https://www.starlette.io/background/" class="external-link" target="_blank"> Starlette针对后台任务的官方文档</a>中查看更多详细信息。

## 警告

如果您需要执行大量的后台计算，而不必一定要在同一进程中运行它（例如，您不需要共享内存，变量等），则可能会受益于使用其他更大的工具，例如 <a href="http://www.celeryproject.org/" class="external-link" target="_blank">Celery</a>。

它们往往需要更复杂的配置，例如RabbitMQ或Redis之类的消息/作业队列管理器，但是它们允许您在多个进程（尤其是多个服务器）中运行后台任务。

要查看示例，请检查[Project Generators](../project-generation.md){.internal-link target=_blank}，它们都包含已经配置的Celery。

但是，如果您需要从同一个 **FastAPI** 应用程序访问变量和对象，或者需要执行一些小的后台任务（例如发送电子邮件通知），则只需使用 `BackgroundTasks` 即可。

## 总结

导入并使用 `BackgroundTasks` 及其 *path operation functions* 中的参数和相关性来添加后台任务。
