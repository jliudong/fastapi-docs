# Cookie 参数

可以像定义 `Query` 和 `Path` 参数一样定义Cookie参数。

## 导入 `Cookie`

首先导入 `Cookie`:

```Python hl_lines="1"
{!../../../docs_src/cookie_params/tutorial001.py!}
```

## 声明 `Cookie` 参数

可以类似`Query` 和 `Path`的结构来声明Cookie参数。

第一个值是默认值，可以传递所有额外验证或注释参数：

```Python hl_lines="7"
{!../../../docs_src/cookie_params/tutorial001.py!}
```

!!! 注释 “技术细节”
    `Cookie` 是 `Path` 和 `Query` 的姐妹类。它同样继承自通用的 `Param` 类。

    但是请牢记，当从 `fastapi` 导入 `Query`, `Path`, `Cookie` 和其他的，实际上也是从特定类返回的函数。

!!! 信息
    要声明cookie，您需要使用 `Cookie` ，否则参数将被解释为查询参数。

## 总结

可以使用 `Cookie` 来声明，类似 `Query` 和 `Path` 的用法。
