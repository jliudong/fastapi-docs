# 调试

您可以在编辑器中连接调试器，例如使用Visual Studio Code或PyCharm。

## 调用 `uvicorn`

在您的FastAPI应用程序中，直接导入并运行 `uvicorn`：

```Python hl_lines="1 15"
{!../../../docs_src/debugging/tutorial001.py!}
```

### 关于 `__name__ == "__main__"`

`__name__ == "__main__"` 的主要目的是使一些使用以下代码调用文件时执行的代码：

<div class="termy">

```console
$ python myapp.py
```

</div>

但在另一个文件导入时不会被调用，例如：

```Python
from myapp import app
```

#### 更多细节

假设您的文件名为 `myapp.py` 。

如果使用以下命令运行它：

<div class="termy">

```console
$ python myapp.py
```

</div>

那么文件中的内部变量 `__name__`（由Python自动创建）的值将为字符串 `"__main__"`。

因此，该部分：

```Python
    uvicorn.run(app, host="0.0.0.0", port=8000)
```

将运行。

---

如果导入该模块（文件），则不会发生这种情况。

因此，如果您还有另一个文件 `importer.py` ，其中包含：

```Python
from myapp import app

# Some more code
```

在这种情况下，`myapp.py` 内部的自动变量将没有值为 `__name__` 的变量 `"__main__"` 。

因此，这一行：

```Python
    uvicorn.run(app, host="0.0.0.0", port=8000)
```

将不会执行。

!!! 信息
    更多信息请查看 <a href="https://docs.python.org/3/library/__main__.html" class="external-link" target="_blank"> Python官方文档</a>。

## 使用调试器运行代码

因为您是直接从代码运行Uvicorn服务器，所以可以直接从调试器调用Python程序（FastAPI应用程序）。

---

例如，在Visual Studio Code中，您可以：

* 转到“调试”面板。
* “添加配置...”。
* 选择“Python”
* 使用选项“Python：当前文件（集成终端）”运行调试器。

然后，它将使用您的 **FastAPI** 代码启动服务器，在断点处停止，依此类推。

这是它的样子：

<img src="/img/tutorial/debugging/image01.png">

---

如果您使用Pycharm，则可以：

* 打开“运行”菜单。
* 选择选项“调试...”。
* 然后显示上下文菜单。
* 选择要调试的文件（在这种情况下为 `main.py`）。

然后，它将使用您的 **FastAPI** 代码启动服务器，在断点处停止，依此类推。

这是它的样子：

<img src="/img/tutorial/debugging/image02.png">
