# 额外数据类型

到目前为止，您一直在使用常见的数据类型，例如：

* `int`
* `float`
* `str`
* `bool`

但是您也可以使用更复杂的数据类型。

您仍将拥有前面章节所看到的相同特性：

*强大的编辑器支持。
*来自传入请求的数据转换。
*响应数据的数据转换。
*数据验证。
*自动注释和文档。

## 其他数据类型

以下是一些您可以使用的其他数据类型：

* `UUID`:
    * 标准的“通用唯一标识符”，在许多数据库和系统中通常作为ID使用。
    * 在请求和响应中将以 `str` 表示。
* `datetime.datetime`:
    * Python语言的 `datetime.datetime`.
    * 在请求和响应中将以ISO 8601格式的 `str` 表示，如: `2008-09-15T15:53:00+05:00`.
* `datetime.date`:
    * Python语言的 `datetime.date`.
    * 在请求和响应中将以ISO 8601格式的 `str` 表示，如: `2008-09-15`.
* `datetime.time`:
    * Python语言的 `datetime.time`.
    * 在请求和响应中将以ISO 8601格式的 `str` 表示，如: `14:23:55.003`.
* `datetime.timedelta`:
    * Python语言的 `datetime.timedelta`.
    * 在请求和响应中，将以总秒数的 `float` 表示。
    * Pydantic 也允许以"ISO 8601 时钟秒差"表示, <a href="https://pydantic-docs.helpmanual.io/#json-serialisation" class="external-link" target="_blank">查看更多信息</a>.
* `frozenset`:
    * 在请求和响应中的 `set` ：
        * 在请求中，将读取一个列表，消除重复，并将其转换为 `set`。
        * 在响应中，`set` 将被转换为 `list`。
        * 生成的架构将指定 `set` 值是唯一的（使用JSON Schema的 `uniqueItems`）。
* `bytes`:
    * Python标准的 `bytes`。
    * 在请求和响应中将被视为 `str`。
    * 生成的模式将指定它是带有 `binary` “格式”的 `str` 。

## 例子

这是一个使用上面那些类型作为参数 *path operation（路径操作）* 示例。

```Python hl_lines="1 2 11 12 13 14 15"
{!../../../docs_src/extra_data_types/tutorial001.py!}
```

请注意，函数内部的参数具有自然数据类型，比如可以执行常规的日期操作，例如：

```Python hl_lines="17 18"
{!../../../docs_src/extra_data_types/tutorial001.py!}
```
