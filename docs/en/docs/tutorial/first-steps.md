# 第一步

最简单的 FastAPI 程序代码像下面这样:

```Python
{!../../../docs_src/first_steps/tutorial001.py!}
```

可以将上面的代码拷贝到 `main.py`.

运行代码启动服务：

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
<span style="color: green;">INFO</span>:     Started reloader process [28720]
<span style="color: green;">INFO</span>:     Started server process [28722]
<span style="color: green;">INFO</span>:     Waiting for application startup.
<span style="color: green;">INFO</span>:     Application startup complete.
```

</div>

!!! 注意
    `uvicorn main:app` 命令的含义是：

    * `main`: 程序文件 `main.py` (Python“模块”)。
    * `app`: `main.py` 程序文件中 `app = FastAPI()` 代码用于创建对象。
    * `--reload`: 使服务器在代码更改后重新启动。 仅用于开发模式。

在终端输出有下面这样一行：

```hl_lines="4"
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

该行显示了在本地计算机上为您的应用程序提供服务的URL。

### 核实一下

打开浏览器在地址栏输入 <a href="http://127.0.0.1:8000" class="external-link" target="_blank">http://127.0.0.1:8000</a>。

您将看到类似如下的服务器响应的JSON数据：

```JSON
{"message": "Hello World"}
```

### 交互式API文档

现在跳转到这个地址 <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>。

在此您将能看到自动交互API文档 (由 <a href="https://github.com/swagger-api/swagger-ui" class="external-link" target="_blank">Swagger UI</a> 提供)：

![Swagger UI](https://fastapi.tiangolo.com/img/index/index-01-swagger-ui-simple.png)

### 替代API文档

现在跳转到这个地址 <a href="http://127.0.0.1:8000/redoc" class="external-link" target="_blank">http://127.0.0.1:8000/redoc</a>.

您将看到替代的自动文档 (由 <a href="https://github.com/Rebilly/ReDoc" class="external-link" target="_blank">ReDoc</a> 提供)：

![ReDoc](https://fastapi.tiangolo.com/img/index/index-02-redoc-simple.png)

### OpenAPI

**FastAPI** 使用用于定义API的 **OpenAPI** 标准对所有API生成“schema（模式）”。

#### "Schema（模式）"

“schema（模式）”是事物的定义或描述。 不是实现它的代码，而是一个抽象的描述。

#### API "schema"

在这个示例中， <a href="https://github.com/OAI/OpenAPI-Specification" class="external-link" target="_blank">OpenAPI</a> 是规定如何定义API模式的规范。

此架构定义包括您的API路径，它们采用的可能参数等。

#### Data "schema"

术语“模式”也可能表示某些数据规格的描述，就好像JSON格式的内容。

在这种情况下，Data "schema"（数据模式）意味着JSON数据属性和其对应的数据类型等。

#### OpenAPI 和 JSON Schema

OpenAPI 为您的API定义了一种 API schema（API模式）。并且“模式”还包含了使用 **JSON Schema** API发送、接收数据的定义和JSON数据模式的标准。

#### 核实 `openapi.json`

如果您对原始OpenAPI模式的样子感到好奇，FastAPI会自动生成包含所有API描述的JSON（模式）。

您可以直接通过这个地址来查看： <a href="http://127.0.0.1:8000/openapi.json" class="external-link" target="_blank">http://127.0.0.1:8000/openapi.json</a>。

这里将显示以下内容开头的JSON内容：

```JSON
{
    "openapi": "3.0.2",
    "info": {
        "title": "FastAPI",
        "version": "0.1.0"
    },
    "paths": {
        "/items/": {
            "get": {
                "responses": {
                    "200": {
                        "description": "Successful Response",
                        "content": {
                            "application/json": {



...
```

#### OpenAPI是什么

OpenAPI架构包含了两个强大的交互式文档系统。

共有数十种基于OpenAPI可选方案。 您可以轻松地将这些方案中的任何一种添加到使用 **FastAPI** 构建的应用程序中。

您还可以使用它为与您的API通信的客户端自动生成代码。 例如，WEB前端应用、移动App客户端或物联网应用程序等。

## 逐步展开

### 第1步： 导入 `FastAPI`

```Python hl_lines="1"
{!../../../docs_src/first_steps/tutorial001.py!}
```

`FastAPI` 是一个Python的类，可为您的API提供所有支撑功能。

!!! 注意 "技术细节"
    `FastAPI` 是一个继承自 `Starlette` 的类。

    您能看到所有 <a href="https://www.starlette.io/" class="external-link" target="_blank">Starlette</a> 的功能在 `FastAPI` 中也是全部具备的。

### 第2步： 创建一个 `FastAPI` 实例

```Python hl_lines="3"
{!../../../docs_src/first_steps/tutorial001.py!}
```

在上面这段代码中 `app` 是 `FastAPI` 类的实例。

这也是通过 `FastAPI` 创建的所有API的主入口。

同时 `app` 变量也是被 `uvicorn` 命令所指向的：

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

如果您创建的应用类似像下面这个样子：

```Python hl_lines="3"
{!../../../docs_src/first_steps/tutorial002.py!}
```

将以上代码放入 `main.py` 文件，运行 `uvicorn` 命令，效果如下：

<div class="termy">

```console
$ uvicorn main:my_awesome_api --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

### 第3步： 创建一个 *操作路径*

#### 路径

"Path（路径）" 在此是指URL中从第一个 `/` 开始的内容。

因此，类似下面这样一个URL：

```
https://example.com/items/foo
```

...其路径将是：

```
/items/foo
```

!!! 提示
    “路径”通常也称为“端点”或“路由”。

在构建API时，“路径”是暴露“资源”的主要方法。

#### 操作

"Operation（操作）" 在此是指一个HTTP方法.

类似下面这些：

* `POST`
* `GET`
* `PUT`
* `DELETE`

...还有这几个不太常用的：

* `OPTIONS`
* `HEAD`
* `PATCH`
* `TRACE`

在HTTP协议中，每一个“路径”都可以通过一个（或多个）“方法”进行通信。

---

构建API时，通常使用这些特定的HTTP方法来执行特定的操作。

通常使用：

* `POST`: 提交数据。
* `GET`: 读取数据。
* `PUT`: 更新数据。
* `DELETE`: 删除数据。

因此在OpenAPI中，每个HTTP方法也被称为"operation（操作）".

本教程中也将它们称为 "**operations（操作）**"。

#### Define a *path operation decorator*

```Python hl_lines="6"
{!../../../docs_src/first_steps/tutorial001.py!}
```

代码`@app.get("/")` 告诉 **FastAPI** 获取到的HTTP请求将如何被处理：

* 路径 `/`
* 使用 <abbr title="an HTTP GET method"><code>get</code> 操作</abbr>

!!! 提示 "`@decorator`"
    在Python开发语言中， `@something` 语法被称为“装饰器”。

    可以将其放在函数之上。 就像一个漂亮的装饰性帽子（我想这是这个术语的来源）。

    “装饰器”采用下面的功能并对其进行处理。

    在这个示例中装饰器告诉 **FastAPI** 下面的函数与具有 `get操作` 的路径 `/` 相对应。

    这就是 "**路径操作装饰器**".

你也可以使用其他的装饰器，如下：

* `@app.post()`
* `@app.put()`
* `@app.delete()`

当然还有一些不常用的：

* `@app.options()`
* `@app.head()`
* `@app.patch()`
* `@app.trace()`

!!! 提示
    您可以随意使用每个操作（HTTP方法）。

    **FastAPI** 对此不做任何强制执行。

    此处提供的信息仅供参考，并非必需。

    例如，当使用GraphQL时，通常只使用“ POST”方法来执行所有操作。

### 第4步：定义 **路径操作函数**

这是我们的 "**路径操作函数**":

* **path**: 是 `/`.
* **operation**: 是 `get`.
* **function**: 是装饰器(`@app.get("/")`)下面的函数。

```Python hl_lines="7"
{!../../../docs_src/first_steps/tutorial001.py!}
```

这是一个Python的函数。

当从URL "`/`" 接收 `GET` 操作请求时这个函数会被**FastAPI**调用。

在这个示例中，它是一个 `async（异步）` 函数.

---

您也可以将其定义为普通函数，而不是 `async def`：

```Python hl_lines="7"
{!../../../docs_src/first_steps/tutorial003.py!}
```

!!! 注意
    如果您不太清楚它们之间的不同，可以到下面地址详细查看 [Async: *"In a hurry?"*](../async.md#in-a-hurry){.internal-link target=_blank}。

### 第5步：返回的内容

```Python hl_lines="8"
{!../../../docs_src/first_steps/tutorial001.py!}
```

可以返回一个集合数据结构，如：`dict`, `list`, 也可以是一个单值数据结构，如： `str`, `int`, 等。

也可以返回 Pydantic models (在后面的章节将详细介绍)。


还有许多其他对象和模型将自动转换为JSON（包括ORM等）。 尝试使用您喜欢的服务器，很有可能已经支持了它们。

## 总结

* 导入 `FastAPI`。
* 创建一个 `app` 实例。
* 写一个 **path operation decorator（路径操作装饰器）** (如 `@app.get("/")`)。
* 写一个 **path operation function（路径操作函数）** (如 `def root(): ...` )。
* 运行开发服务 (如 `uvicorn main:app --reload`)。
