# 路径参数

您可以使用Python格式字符串使用的相同语法声明路径“参数”或“变量”：

```Python hl_lines="6 7"
{!../../../docs_src/path_params/tutorial001.py!}
```

路径参数值 `item_id` 传递给函数的参数 `item_id`。

可以输入如下地址来运行这个示例程序 <a href="http://127.0.0.1:8000/items/foo" class="external-link" target="_blank">http://127.0.0.1:8000/items/foo</a>，能看到输入如下：

```JSON
{"item_id":"foo"}
```

## 带有类型的路径参数

可以使用标准的Python类型注释在函数中声明路径参数的类型：

```Python hl_lines="7"
{!../../../docs_src/path_params/tutorial002.py!}
```

在上面这个例子中， `item_id` 被声明为一个 `int` 类型。

!!! 检查
    这将在编辑器中为函数编写提供支持，以及错误检查，代码补全等。

## 数据 <abbr title="也称为：序列化，解析，编组">转换</abbr>

在浏览器中运行这个例子 <a href="http://127.0.0.1:8000/items/3" class="external-link" target="_blank">http://127.0.0.1:8000/items/3</a>，看到的响应内容如下：

```JSON
{"item_id":3}
```

!!! 检查
    注意函数接收到的（并且返回的）是数字 `3`，作为Python的 `int` 类型，而不是字符串 `"3"` 。

    因此，带有类型解析的 **FastAPI** 提供了一种自动化请求 <abbr title="将来自HTTP请求的字符串转换为Python数据">"解析"</abbr>。

## 数据验证

但是如果你在浏览器中输入 <a href="http://127.0.0.1:8000/items/foo" class="external-link" target="_blank">http://127.0.0.1:8000/items/foo</a>，将能看到如下错误输出：

```JSON
{
    "detail": [
        {
            "loc": [
                "path",
                "item_id"
            ],
            "msg": "value is not a valid integer",
            "type": "type_error.integer"
        }
    ]
}
```

因为路径参数 `item_id` 给的值是字符串 `"foo"`，这不是一个Python的 `int` 类型。

如果提供的是float而不是int，则会出现相同的错误，如下所示： <a href="http://127.0.0.1:8000/items/4.2" class="external-link" target="_blank">http://127.0.0.1:8000/items/4.2</a>

!!! 检查
    因此，使用相同的Python类型声明， **FastAPI** 可为您提供数据验证。

    请注意，该错误提示清楚地指出了验证未通过的错误点。

    在开发和调试与您的API交互的代码时，这非常有用。

## 文档

在浏览器中输入 <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>，可以看到自动化、交互式的API文档，类似如下：

<img src="/img/tutorial/path-params/image01.png">

!!! 检查
    同样，仅使用相同的Python类型声明，**FastAPI** 即可为您提供自动的交互式文档（集成Swagger UI）。

    请注意，路径参数声明为整数。

## 基于标准的好处，替代性文件

并且由于生成的架构来自 <a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md" class="external-link" target="_blank">OpenAPI</a> 标准， 因此有许多兼容的工具。

因此， **FastAPI** 本身提供了备用API文档（使用ReDoc），可以通过如下地址访问 <a href="http://127.0.0.1:8000/redoc" class="external-link" target="_blank">http://127.0.0.1:8000/redoc</a>：

<img src="/img/tutorial/path-params/image02.png">

同样，有许多兼容的工具。 包括多种语言的代码生成工具。

## Pydantic

所有数据验证都是由 <a href="https://pydantic-docs.helpmanual.io/" class="external-link" target="_blank">Pydantic</a>在后台进行的，因此好处是全部都由Pydantic自动化完成了。 而开发人员则无须做过多的工作就可以达到一个良好的状态。

可以使用与 `str`, `float`, `bool` 相同的类型声明，也可以和许多其他复杂数据类型一起使用。

本教程的下一章将探讨这些内容。

## 顺序很重要

在创建 *path operations（路径操作）* 时，你会发现有些情况下路径是固定的。

比如 `/users/me`, 我们假设它用来获取关于当前用户的数据。

然后，你还可以使用路径 `/users/{user_id}` 来通过用户 ID 获取关于特定用户的数据。

由于 *path operations（路径参数）* 是按顺序依次运行的，因此需要确保路径 `/users/me` 声明在路径 `/users/{user_id}` 之前：

```Python hl_lines="6 11"
{!../../../docs_src/path_params/tutorial003.py!}
```

否则， `/users/{user_id}` 的路径还将与 `/users/me` 相匹配， "认为"自己正在接收一个值为 `"me"` 的 `user_id` 参数。

## 预设值

如果你有一个接收路径参数的路径操作，但你希望预先设定可能的有效参数值，则可以使用标准的 Python <abbr title="Enumeration">`Enum`</abbr> 类型。

### 创建一个 `Enum` 类

导入 `Enum` 并创建一个继承自 `str` 和 `Enum` 的子类。

通过从 `str` 继承，API 文档将能够知道这些值必须为 `string` 类型并且能够正确地展示出来。

然后创建具有固定值的类属性，这些固定值将是可用的有效值：

```Python hl_lines="1 6 7 8 9"
{!../../../docs_src/path_params/tutorial005.py!}
```

!!! 信息
    <a href="https://docs.python.org/3/library/enum.html" class="external-link" target="_blank">枚举（或enums）在Python中可用</a> 从 3.4 版本起在 Python 中可用。

!!! 提示
    如果你知道， "AlexNet", "ResNet", 和 "LeNet" 只是机器学习中的 <abbr title="从技术上讲，深度学习模型架构">模型</abbr> 名称。

### 声明 *path parameter（路径参数）*

然后使用你定义的枚举类（`ModelName`）创建一个带有类型标注的 *path parameter（路径参数）*：

```Python hl_lines="16"
{!../../../docs_src/path_params/tutorial005.py!}
```

### 查看文档

因为已经指定了 *path parameter（路径参数）* 的可用值，所以交互式文档可以恰当地展示它们：

<img src="/img/tutorial/path-params/image03.png">

### 使用Python *enumerations（枚举类型）*

 *path parameter（路径参数）* 的值将是一个 *enumeration member（枚举成员）* 。

#### 比较 *enumeration members（枚举成员）*

你可以将它与你创建的枚举类 `ModelName` 中的枚举成员进行比较：

```Python hl_lines="17"
{!../../../docs_src/path_params/tutorial005.py!}
```

#### 获取 *enumeration value（枚举值）*

你可以使用 `model_name.value` 或通常来说 `your_enum_member.value` 来获取实际的值（在这个例子中为 `str`）：

```Python hl_lines="20"
{!../../../docs_src/path_params/tutorial005.py!}
```

!!! 提示
    你也可以通过 `ModelName.lenet.value` 来获取 `"lenet"` 值。

#### 返回 *enumeration members（枚举成员）*

你可以从路径操作中返回 *enumeration members（枚举成员）* ，即使嵌套在 JSON 结构中（例如一个 `dict` 中）。

在将它们返回给客户端之前，它们将被转换为其相应的值（在这个示例中为字符串）：

```Python hl_lines="18  21  23"
{!../../../docs_src/path_params/tutorial005.py!}
```

在客户端中，将收到类似JSON的响应：

```JSON
{
  "model_name": "alexnet",
  "message": "Deep Learning FTW!"
}
```

## 包含路径的路径参数

假设你有一个 *path operation（路径操作）* ，它的路径为 `/files/{file_path}` 。 

但是你需要 `file_path` 自身也包含 *路径* ，比如 `home/johndoe/myfile.txt` 。

因此，该文件的URL将类似于这样：`/files/home/johndoe/myfile.txt` 。

### OpenAPI 支持

OpenAPI 不支持任何方式去声明 *path parameter（路径参数）* 以在其内部包含 *path（路径）* ，因为这可能会导致难以测试和定义的情况出现。

不过，仍然可以通过 Starlette 的一个内部工具在 **FastAPI** 中实现它。

尽管仍未添加任何说明该参数应包含路径的文档，但这些文档仍然可以使用。

### 路径转换器

使用直接来自Starlette的选项，您可以使用以下网址声明包含 *path* 的 *path parameter(路径参数）* ：

```
/files/{file_path:path}
```

在这种情况下，参数的名称为 `file_path` ，最后一部分： `:path` 告诉它该参数应与任何 *path* 匹配。

因此，你可以这样使用它：

```Python hl_lines="6"
{!../../../docs_src/path_params/tutorial004.py!}
```

!!! 提示
    可能需要参数包含 `/home/johndoe/myfile.txt` ，并使用反斜杠（`/`）。

    在上面这个例子中，URL为： `/files//home/johndoe/myfile.txt` ，在 `files` 和 `home` 之间使用双斜杠（`//`）。

## 总结

使用 **FastAPI** ，通过简短、直观和标准的 Python 类型声明，你将获得：

* 编辑器支持：错误检查，代码补全等。
* 数据 "<abbr title="将来自HTTP请求的字符串转换为Python数据">解析</abbr>"
* 数据验证
* API 标注和自动生成的文档

而且你只需要声明一次即可。

这可能是 **FastAPI** 与其他框架相比主要的明显优势（除了原始性能以外）。
