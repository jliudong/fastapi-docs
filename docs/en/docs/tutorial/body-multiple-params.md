# 请求体 - 多参数

在前面的章节已经了解了如何使用 `Path` 和 `Query` ，那么让我们来看一下请求主体声明的更高级用法。

## 混合 `Path`, `Query` 和请求体参数

首先，当然，可以自由地混合 `Path` ， `Query` 和请求主体参数声明，并且 **FastAPI** 将知道该怎么做。

您还可以通过将默认值设置为 `None` 来将主体参数声明为可选参数：

```Python hl_lines="17 18 19"
{!../../../docs_src/body_multiple_params/tutorial001.py!}
```

!!! 注释
    注意，在上面这个例子中，因为item具有 `None` 默认值。因此从请求体中获取 `item` 是可选的，而不是必需的。

## 多请求体

在上一个示例中，*path operations（路径操作）* 将从请求体中寻找JSON主体来填充 `Item`，像下面这样：

```JSON
{
    "name": "Foo",
    "description": "The pretender",
    "price": 42.0,
    "tax": 3.2
}
```

但您也可以声明多个请求体参数，例如 `item` 和 `user` ： 

```Python hl_lines="20"
{!../../../docs_src/body_multiple_params/tutorial002.py!}
```

在这个例子中， **FastAPI** 将注意到函数中有多个请求体参数（两个参数是Pydantic模型）。

因此，它将使用参数名称作为正文中的键（字段名称），并期望一个类似于以下内容的正文：

```JSON
{
    "item": {
        "name": "Foo",
        "description": "The pretender",
        "price": 42.0,
        "tax": 3.2
    },
    "user": {
        "username": "dave",
        "full_name": "Dave Grohl"
    }
}
```

!!! 注释
    请注意，即使以与以前相同的方式声明了 `item` ，但现在仍可以使用键 `item` 将其放置在请求体内部。


**FastAPI** 将根据请求进行自动转换，以便参数 `item` 接收其特定内容，并且与 `user` 相同。

它将执行复合数据的验证，并将像OpenAPI模式和自动文档一样对其进行记录。

## 请求体中的奇异值

同样，使用 `Query` 和 `Path` 为查询和路径参数定义额外的数据，**FastAPI** 提供等效的 `Body`。

例如，扩展先前的模型，您可以决定要在除 `item` 和 `user` 之外的同一请求体中具有另一个键 `importance` 。

如果按原样声明它，因为它是一个奇异值，**FastAPI** 将假定它是一个查询参数。

但是您可以使用 `Body` 指示 **FastAPI** 将其视为另一个请求体的键：


```Python hl_lines="21"
{!../../../docs_src/body_multiple_params/tutorial003.py!}
```

在这个例子中 **FastAPI** 期望的请求体应该类似下面这样:

```JSON
{
    "item": {
        "name": "Foo",
        "description": "The pretender",
        "price": 42.0,
        "tax": 3.2
    },
    "user": {
        "username": "dave",
        "full_name": "Dave Grohl"
    },
    "importance": 5
}
```

另外框架还将完成数据类型转换、数据验证、生成文档等。

## 多请求体参数和查询

当然，除了任何请求体参数之外，您还可以在需要时声明其他查询参数。

由于默认情况下，奇异值被解释为查询参数，因此不必显式添加 `Query`，您可以执行以下操作：

```Python
q: str = None
```

如下：

```Python hl_lines="25"
{!../../../docs_src/body_multiple_params/tutorial004.py!}
```

!!! 信息
    `Body` 还具有与 `Query`，`Path` 以及稍后将看到的其他参数相同的所有额外验证和元数据参数。

## 嵌入单一参数

假设您只有Pydantic模型 `item` 中的一个 `item` 请求体参数。

默认情况下，**FastAPI** 将直接期望其请求体。

但是，如果您希望它期望一个带有键 `item` 的JSON并在其中包含模型内容，就像在声明额外的请求体参数时那样，则可以使用特殊的 `Body` 参数  `embed`：

```Python
item: Item = Body(..., embed=True)
```

如下：

```Python hl_lines="15"
{!../../../docs_src/body_multiple_params/tutorial005.py!}
```

在这个例子中 **FastAPI** 将期望请求体类型如下：

```JSON hl_lines="2"
{
    "item": {
        "name": "Foo",
        "description": "The pretender",
        "price": 42.0,
        "tax": 3.2
    }
}
```

代替：

```JSON
{
    "name": "Foo",
    "description": "The pretender",
    "price": 42.0,
    "tax": 3.2
}
```

## 总结

您可以将多个请求体参数添加到 *path operation function（路径操作函数）* 中，即使一个请求只能有一个请求体。

但是 **FastAPI** 会处理它，在函数中为您提供正确的数据，并在 *path operation（路径操作）* 中验证并记录正确的模式。

您还可以声明将奇异值作为请求体的一部分接收。

而且，即使仅声明了一个参数，您也可以指示 **FastAPI** 将请求体嵌入键中。
