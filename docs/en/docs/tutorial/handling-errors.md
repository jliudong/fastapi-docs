# 处理错误

在许多情况下，您需要将错误通知给正在使用API的客户端。

该客户端可以是带有前端的浏览器，其他人的代码，IoT设备等。

您可能需要告诉客户端：

* 客户端没有足够的权限进行该操作。
* 客户端无权访问该资源。
* 客户端尝试访问的项目不存在。
* 等等

在这些情况下，您通常会返回 **HTTP状态代码**，范围为 **400**（从400到499）。

这类似于HTTP状态代码 200（从200到299）。 这些 “200”状态代码意味着请求中某种程度上存在“成功”。

400范围内的状态代码表示客户端出现错误。

还记得所有那些 **“404未找到”** 错误（和笑话）吗？

## 使用`HTTPException`

要将错误的HTTP响应返回给客户端，请使用 `HTTPException`。

###导入 `HTTPException`

```Python hl_lines="1"
{!../../../docs_src/handling_errors/tutorial001.py!}
```

### 在代码中抛出 `HTTPException`

`HTTPException` 是一个普通的Python异常，带有与API相关的其他数据。

因为它是Python异常，所以您不必 `return` 它，而是 `raise` 它。

这也意味着，如果您在某个实用程序函数内部，并且正在您的 *path operation function* 内部进行调用，并且从该实用程序函数内部抛出了 `HTTPException`，它将不会运行该脚本中的其余代码。 *path operation function*，它将立即终止该请求，并将HTTP错误从 `HTTPException` 发送到客户端。

在“依赖关系和安全性”部分中，抛出异常而不是 `return` 的好处将更加明显。

在此示例中，当客户通过不存在的ID请求商品时，引发状态代码为 `404` 的异常：

```Python hl_lines="11"
{!../../../docs_src/handling_errors/tutorial001.py!}
```

### 结果响应

如何客户端请求 `http://example.com/items/foo` (一个 `item_id` `"foo"`), 则他将收到HTTP状态码200和JSON响应：

```JSON
{
  "item": "The Foo Wrestlers"
}
```

但是如何客户端请求 `http://example.com/items/bar` (一个不存在的 `item_id` `"bar"`), 他将收到HTTP状态代码404（“未找到”错误）和JSON响应：

```JSON
{
  "detail": "Item not found"
}
```

!!! 提示
    引发 `HTTPException` 时，您可以传递任何可以转换为JSON的值作为参数detail，而不仅限于str。

    您可以传递`dict`，`list`等等。

    它们由 **FastAPI** 自动处理，并转换为JSON。

## 添加自定义标题

在某些情况下，能够将自定义header添加到HTTP错误很有用。 例如，对于某些类型的安全性。

您可能不需要直接在代码中使用它。

但是，如果需要高级方案，则可以添加自定义header：

```Python hl_lines="14"
{!../../../docs_src/handling_errors/tutorial002.py!}
```

## 安装自定义异常处理程序

您可以使用<a href="https://www.starlette.io/exceptions/" class="external-link" target="_blank">来自Starlette的相同异常实用程序</a>添加自定义异常处理程序。

假设您有一个自定义异常 `UnicornException` ，您（或您使用的库）可能会 `raise`。

您想使用FastAPI全局处理此异常。

您可以使用`@app.exception_handler()`添加自定义异常处理程序：

```Python hl_lines="5 6 7  13 14 15 16 17 18  24"
{!../../../docs_src/handling_errors/tutorial003.py!}
```

在这里，如果您请求 `/unicorns/yolo`，那么 *path operation* 将 `raise` 一个 `UnicornException`。

但是它将由`unicorn_exception_handler`处理。

因此，您将收到一个干净的错误，其HTTP状态代码为 `418` 和JSON内容为：

```JSON
{"message": "Oops! yolo did something. There goes a rainbow..."}
```

!!! 请注意“技术细节”
    您也可以使用 `from starlette.requests import Request` 和 `from starlette.responses import JSONResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。但是大多数可用的响应直接来自Starlette。与`Request`相同。

## 覆盖默认的异常处理程序

**FastAPI** 具有一些默认的异常处理程序。

这些处理程序负责在您引发HTTPException时以及请求中包含无效数据时返回默认的JSON响应。

您可以使用自己的方法覆盖这些异常处理程序。

### 覆盖请求验证异常

当请求包含无效数据时，**FastAPI** 在内部引发 `RequestValidationError`。

并且它还包括一个默认的异常处理程序。

要覆盖它，请导入`RequestValidationError`并将其与`@app.exception_handler(RequestValidationError)`一起使用以装饰异常处理程序。

异常处理程序将收到一个`Request`和异常。

```Python hl_lines="2 14 15 16"
{!../../../docs_src/handling_errors/tutorial004.py!}
```

现在，如果您转到`/items/foo`，而不是使用以下命令获取默认的JSON错误：

```JSON
{
    "detail": [
        {
            "loc": [
                "path",
                "item_id"
            ],
            "msg": "value is not a valid integer",
            "type": "type_error.integer"
        }
    ]
}
```

您将获得一个文本版本，其中包含：

```
1 validation error
path -> item_id
  value is not a valid integer (type=type_error.integer)
```

#### `RequestValidationError` vs `ValidationError`

!!! 警告
    如果现在对您不重要，则可以跳过这些技术细节。

`RequestValidationError` 是一个Pydantic的 <a href="https://pydantic-docs.helpmanual.io/#error-handling" class="external-link" target="_blank">`ValidationError`</a>的子类。

**FastAPI** 使用它，以便如果您在`response_model`中使用Pydantic模型，并且您的数据有错误，您将在日志中看到该错误。

但是客户端/用户将看不到它。 相反，客户端将收到HTTP状态代码为 `500` 的“内部服务器错误”。

之所以应该这样，是因为如果您在 *response* 或代码中的任何地方（而不是客户端的 *request*）中有Pydantic`ValidationError`，则实际上是代码中的错误。

并且，在修复该错误时，您的客户/用户不应访问有关该错误的内部信息，因为这可能会暴露一个安全漏洞。

### 覆盖 `HTTPException` 错误处理程序

同样，您可以覆盖HTTPException处理程序。

例如，对于这些错误，您可能希望返回纯文本响应而不是JSON：

```Python hl_lines="3 4  9 10 11 22"
{!../../../docs_src/handling_errors/tutorial004.py!}
```

!!! 请注意“技术细节”
    您也可以使用`from starlette.responses import PlainTextResponse`。

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses`，只是为开发人员提供了方便。 但是大多数可用的响应直接来自Starlette。

### 使用 `RequestValidationError` body

`RequestValidationError` 包含接收到的 `body` 以及无效数据。

您可以在开发应用程序时使用它来记录请求体并对其进行调试，然后将其返回给用户，等等。

```Python hl_lines="14"
{!../../../docs_src/handling_errors/tutorial005.py!}
```

现在尝试发送无效的项目，例如：

```JSON
{
  "title": "towel",
  "size": "XL"
}
```

您将收到一条响应，告知您包含接收到的正文的数据无效：

```JSON hl_lines="13 14 15 16"
{
  "detail": [
    {
      "loc": [
        "body",
        "size"
      ],
      "msg": "value is not a valid integer",
      "type": "type_error.integer"
    }
  ],
  "body": {
    "title": "towel",
    "size": "XL"
  }
}
```

#### FastAPI的 `HTTPException` 与 Starlette的 `HTTPException`

**FastAPI** 拥有自己的 `HTTPException` 。

FastAPI的 `HTTPException` 错误类继承自Starlette的 `HTTPException` 错误类。

唯一的区别是，**FastAPI** 的 `HTTPException` 允许您添加要包含在响应中的header。

OAuth 2.0和某些安全实用程序在内部需要/使用此功能。

因此，您可以像往常一样在代码中继续抛出 **FastAPI** 的 `HTTPException`。

但是，当您注册异常处理程序时，应为Starlette的 `HTTPException` 注册它。

这样，如果Starlette内部代码的任何部分或Starlette扩展或插件引发了Starlette `HTTPException`，则您的处理程序将能够捕获并处理它。

在此示例中，为了能够将两个 `HTTPException` 都包含在同一代码中，Starlette的异常被重命名为`StarletteHTTPException`：

```Python
from starlette.exceptions import HTTPException as StarletteHTTPException
```

### 重用 **FastAPI** 的异常处理程序

您也可能只想以某种方式使用该异常，然后使用来自 **FastAPI** 的相同默认异常处理程序。

您可以从 `fastapi.exception_handlers` 中导入并重新使用默认的异常处理程序：

```Python hl_lines =“ 2 3 4 5 15 21”
{！../../../ docs_src / handling_errors / tutorial006.py！}
```

在此示例中，您只是用非常富有表现力的消息来 `print` 错误。

但是您知道了，可以使用异常，然后重新使用默认的异常处理程序。
