# 路径操作配置

您可以将几个参数传递给 *path operation decorator（路径操作装饰器）* 进行配置。

!!! 警告
    请注意，这些参数直接传递给 *path operation decorator（路径操作装饰器）* ，而不传递给 *path operation function（路径操作函数）*。

## 相应状态码

您可以定义（HTTP）`status_code`以在 *path operation（路径操作）* 的响应中使用。

可以直接传递 `int` 类型的代码，比如 `404`。

但是，如果您不记得每个数字代码的用途，则可以在 `status` 中使用快捷方式常量：

```Python hl_lines="3  17"
{!../../../docs_src/path_operation_configuration/tutorial001.py!}
```

该状态代码将在响应中使用，并将添加到OpenAPI模型中。

!!! note "技术细节"
    还可以使用 `from starlette import status`.

    ** FastAPI ** 与 `fastapi.status` 提供相同的 `starlette.status` ，只是为开发人员提供了方便。 但它直接来自Starlette。

## Tags

您可以将标签添加到 *path operation（路径操作）* 中，并通过带有 `str` 的 `list` 的参数 `tags`（通常只有一个 `str` ）传递：

```Python hl_lines="17 22 27"
{!../../../docs_src/path_operation_configuration/tutorial002.py!}
```

它们将被添加到OpenAPI模型中，并在自动化文档中提供给读者：

<img src="/img/tutorial/path-operation-configuration/image01.png">

## 摘要和描述

可以添加 `summary` 和 `description`：

```Python hl_lines="20 21"
{!../../../docs_src/path_operation_configuration/tutorial003.py!}
```

## 来自文档字符串的描述

由于描述往往会很长并且涵盖多行，因此您可以在函数<abbr title="多行字符串，作为用于文档编制的函数（未分配给任何变量）中的第一个表达式">docstring</abbr>和 **FastAPI** 将从那里读取它。

可以写 <a href="https://en.wikipedia.org/wiki/Markdown" class="external-link" target="_blank">Markdown</a> 在文档字符串中， 它将被正确解释和显示（考虑到文档字符串缩进）。

```Python hl_lines="19 20 21 22 23 24 25 26 27"
{!../../../docs_src/path_operation_configuration/tutorial004.py!}
```

它将在交互式文档中使用：

<img src="/img/tutorial/path-operation-configuration/image02.png">

## 响应说明

您可以使用参数指定响应描述 `response_description`:

```Python hl_lines="21"
{!../../../docs_src/path_operation_configuration/tutorial005.py!}
```

!!! 信息
    注意，`response_description` 特别是指响应，`description` 通常是指 *path operation（路径操作）*。

!!! 检查
    OpenAPI指定每个 *path operation（路径操作）* 都需要响应描述。

    因此，如果您不提供任何一项，**FastAPI** 将自动生成“成功响应”。

<img src="/img/tutorial/path-operation-configuration/image03.png">

## 弃用 *path operation*

如果您需要将 *path operation（路径操作）* 标记为<abbr title =“已过时，建议不要使用它>”>已弃用</abbr>，但是在不删除它的情况下，传递参数 `deprecated` ：

```Python hl_lines="16"
{!../../../docs_src/path_operation_configuration/tutorial006.py!}
```

在交互式文档中，它将明确标记为不推荐使用：

<img src="/img/tutorial/path-operation-configuration/image04.png">

检查已弃用和未弃用的 *path operations（路径操作）* 的样子：

<img src="/img/tutorial/path-operation-configuration/image05.png">

## 总结

通过将参数传递给*path operation decorators（路径操作装饰器）*，您可以轻松地为 *path operations（路径操作）* 配置和添加元数据。
