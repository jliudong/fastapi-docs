# 请求体 - 嵌套模型

使用 **FastAPI** 可以定义，验证，记录和使用任意深度嵌套的模型（这要归功于Pydantic）。

## 列表字段

可以定义属性为一种子类型，比如一个Python的 `list`类型：

```Python hl_lines="12"
{!../../../docs_src/body_nested_models/tutorial001.py!}
```

在上面这个例子中 `tags` 是一个list项。虽然不能声明list项中的各个类型。

## 带有子类型的列表字段

但是Python语言中，有一种特定的方法来声明带有子类型的列表：

### 导入 typing's `List`

首先，从标准的Python语言库的 `typing` 模块中导入 `List`：

```Python hl_lines="1"
{!../../../docs_src/body_nested_models/tutorial002.py!}
```

### 声明带有子类型的 `List`

声明Python语言基本数据结构`list`, `dict`, `tuple`的子类型：

* 从 `typing` 模块中导入它们
* 使用中括号： `[` 和 `]` 将类型参数传递给子类型

```Python
from typing import List

my_list: List[str]
```

这是标准的Python语言的类型声明语法。

对于模型属性的子类型也是使用了相同的Python标准语法。

因此，在上面的例子中，可以将 `tags` 指定为“字符串列表”：

```Python hl_lines="14"
{!../../../docs_src/body_nested_models/tutorial002.py!}
```

## 设置类型

但是要思考一下，要认识到标签不应该重复，它们可能是唯一的字符串。

而且Python具有一组特殊的数据类型来存储唯一项，即 `set` 。

可以导入 `Set` 并将声明 `tag` 为 `str` 的 `set`：

```Python hl_lines="1 14"
{!../../../docs_src/body_nested_models/tutorial003.py!}
```

这样，即使您收到重复数据的请求，该请求也将转换为一组唯一的项。

并且每当输出该数据时，即使源重复，它们也将作为一组唯一项输出。

并且也会相应地进行注释/记录。

## 嵌套模型

每个Pydantic模型的属性都有类型。

但是这种类型本身可以是另一种Pydantic模型。

因此，可以使用特定的属性名称，类型和验证来声明深度嵌套的JSON `object` 。

所有这些，任意嵌套。

### 定义一个子模型

例如，我们可以定义一个 `Image` 模型：

```Python hl_lines="9 10 11"
{!../../../docs_src/body_nested_models/tutorial004.py!}
```

### 使用子模型作为一个类型

然后我们可以将其用作属性的类型：

```Python hl_lines="20"
{!../../../docs_src/body_nested_models/tutorial004.py!}
```

这意味着 **FastAPI** 将期望一个类型如下的请求体：

```JSON
{
    "name": "Foo",
    "description": "The pretender",
    "price": 42.0,
    "tax": 3.2,
    "tags": ["rock", "metal", "bar"],
    "image": {
        "url": "http://example.com/baz.jpg",
        "name": "The Foo live"
    }
}
```

同样，仅使用 **FastAPI** 进行声明，您将获得：

*编辑器支持（补全等），甚至对于嵌套模型
*数据转换
*数据验证
*自动文档

## 特殊类型和验证

除了正常的单值类型（如 `str`, `int`, `float` 等）外，您还可以使用从 `str` 继承的更复杂的单值类型。

要查看所有选项，请查看 <a href="https://pydantic-docs.helpmanual.io/usage/types/" class="external-link" target="_blank">Pydantic's exotic types</a>. 可以在下一章中看到这样的例子。

例如，在 `Image` 模型中，可以有一个 `url` 字段，可以声明一个替代 `str` 的类型，Pydantic的 `HttpUrl` 类型：

```Python hl_lines="4 10"
{!../../../docs_src/body_nested_models/tutorial005.py!}
```

这样一个特殊的字符串类型将被进行合法URL的检查，并且整合到JSON Schema / OpenAPI中。

## 带有子模型列表的属性

还可以在`list`, `set`的子类型中使用Pydantic models，如下所示：

```Python hl_lines="20"
{!../../../docs_src/body_nested_models/tutorial006.py!}
```

此时期望的JSON请求体类型如下 (转换, 验证, 文档化等)：

```JSON hl_lines="11"
{
    "name": "Foo",
    "description": "The pretender",
    "price": 42.0,
    "tax": 3.2,
    "tags": [
        "rock",
        "metal",
        "bar"
    ],
    "images": [
        {
            "url": "http://example.com/baz.jpg",
            "name": "The Foo live"
        },
        {
            "url": "http://example.com/dave.jpg",
            "name": "The Baz"
        }
    ]
}
```

!!! 信息
    注意此时的 `images` 键的值将是image对象的列表。

## 深度嵌套模型

可以定义任意深度的嵌套模型：

```Python hl_lines="9 14 20 23 27"
{!../../../docs_src/body_nested_models/tutorial007.py!}
```

!!! 信息
    注意 `Offer` 中有一个 `Item` 的列表, 这个列表里面又有一个可选的 `Image` 列表。

## 纯列表的请求体

如果期望的JSON主体的顶级值为JSON `array`（Python `list`），则可以在函数的参数中声明与Pydantic模型相同的类型：

```Python
images: List[Image]
```

如下：

```Python hl_lines="15"
{!../../../docs_src/body_nested_models/tutorial008.py!}
```

## 编辑器支持无处不在

而且您到处都有编辑器支持。

即使是列表中的项：

<img src="/img/tutorial/body-nested-models/image01.png">

如果您直接使用 `dict` 而不是Pydantic模型，就无法获得这种编辑器支持了。

但是您也不必担心它们，传入的 `dict` 会自动转换，您的输出也会自动转换为JSON。

## `dict`的任意请求体

也可以使用某些类型的键和其他类型的值将请求体声明为 `dict`。

无需事先知道有效的字段/属性名称是什么（就像Pydantic模型一样）。

如果您想接收未知的键，这将很有用。

---

其他有用的情况是当您想要其他类型的键时，例如 `int`。

这就是我们在此看到的。

在这种情况下，只要它具有带有 `float` 值的 `int` 键，就可以接受任何 `dict` ：

```Python hl_lines="15"
{!../../../docs_src/body_nested_models/tutorial009.py!}
```

!!! 提示
    
    请记住，JSON仅支持将 `str` 用作键。

     但是Pydantic具有自动数据转换功能。

     这意味着，即使您的API客户端只能将字符串作为键发送，只要这些字符串包含纯整数，Pydantic就会对其进行转换并验证它们。
    
     您作为 `weights` 收到的 `dict` 实际上将具有 `int` 键和 `float` 值。

## 总结

使用 **FastAPI**，您可以拥有Pydantic模型提供的最大灵活性，同时保持代码简单，简短和美观。

但是有所有好处：

*编辑器支持（无处不在！）
*数据转换（也称为解析/序列化）
*数据验证
*模式文档
*自动文档
