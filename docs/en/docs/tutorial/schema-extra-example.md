# 额外模式信息示例

您可以定义其他信息以放入JSON模式。

一个常见的用例是添加一个将在文档中显示的 `example` 。

有几种方法可以声明额外的JSON模式信息。

## Pydantic `schema_extra`

您可以使用 `Config` 和 `schema_extra` 声明
You can declare an example for a Pydantic model using `Config` and `schema_extra`, as described in <a href="https://pydantic-docs.helpmanual.io/usage/schema/#schema-customization" class="external-link" target="_blank">Pydantic's docs: Schema customization</a>Pydantic模型的示例，如:

```Python hl_lines="13 14 15 16 17 18 19 20 21"
{!../../../docs_src/schema_extra_example/tutorial001.py!}
```

该额外信息将原样添加到输出JSON模式中。

## `Field` 附加参数

在稍后将看到的`Field`, `Path`, `Query`, `Body`等中，您还可以通过向函数传递任何其他任意参数来声明JSON模式的额外信息，例如，添加一个示例：

```Python hl_lines="2 8 9 10 11"
{!../../../docs_src/schema_extra_example/tutorial002.py!}
```

!!! 警告
    请记住，出于文档目的，传递的那些额外参数不会添加任何验证，而只会添加注释。

## `Body` 附加参数

可以使用相同的方法，传递额外信息给 `Field`，与 `Path`, `Query`, `Body`等是一致的。

例如，可以传递 `example` 给请求体 `Body`：

```Python hl_lines="19 20 21 22 23 24"
{!../../../docs_src/schema_extra_example/tutorial003.py!}
```

## 文档界面中的例子

使用以上任何一种方法，它在 `/docs` 中都是这样的：

<img src="/img/tutorial/body-fields/image01.png">

## 技术细节

关于 `example` vs `examples`...

JSON 定义一个字段 <a href="https://json-schema.org/draft/2019-09/json-schema-validation.html#rfc.section.9.5" class="external-link" target="_blank">`examples`</a> 在最新版本中，但OpenAPI是基于没有示例的JSON Schema的旧版本。

因此，OpenAPI 使用相同目标来定义 <a href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md#fixed-fields-20" class="external-link" target="_blank">`example`</a> (是 `example`, 而非 `examples`)，这就是docs UI（使用Swagger UI）所使用的。

因此，尽管 `example` 不是JSON Schema的一部分，但它是OpenAPI的一部分，而这正是文档界面中所呈现的。

## 其他信息

同样，您可以添加自己的自定义额外信息，这些信息将添加到每个模型的JSON模式中，例如，以自定义前端用户界面等。
