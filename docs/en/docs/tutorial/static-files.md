# 静态文件

您可以使用 `StaticFiles` 从目录自动提供静态文件。

## 安装 `aiofiles`

首先，您需要安装 `aiofiles`：

<div class="termy">

```console
$ pip install aiofiles

---> 100%
```

</div>

## 使用 `StaticFiles`

* 导入 `StaticFiles`.
* "挂载"一个 `StaticFiles()` 实例在指定的路径中。

```Python hl_lines="2 6"
{!../../../docs_src/static_files/tutorial001.py!}
```

!!! 请注意"技术细节"
    你也可以使用 `from starlette.staticfiles import StaticFiles`。

    **FastAPI** 提供与 `fastapi.staticfiles` 相同的 `starlette.staticfiles`，以方便开发人员。 但实际上它直接来自Starlette。

### 什么是“挂载”

“挂载”是指在特定路径中添加完整的“独立”应用程序，然后负责处理所有子路径。

这与使用 `APIRouter` 不同，因为已安装的应用程序是完全独立的。主应用程序中的OpenAPI和文档不会包含已安装应用程序等中的任何内容。

您可以在《**Advanced User Guide**》中阅读更多相关信息。

## 细节

第一个 `"/static"` 是指将“安装”该“子应用程序”的子路径。 因此，任何以 `"/static"` 开头的路径都将被它处理。

`directory="static"` 是指包含您的静态文件的目录的名称。

`name="static"` 为它提供了一个名称，供 **FastAPI** 在内部使用。

所有这些参数都可以不同于“`static`”参数，根据您自己的应用程序的需求和特定细节进行调整。

## 更多信息

有关更多详细信息和选项，请查看<a href="https://www.starlette.io/staticfiles/" class="external-link" target="_blank"> Starlette关于静态文件的文档</a>。
