# CORS (跨域资源共享)

<a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS" class="external-link" target="_blank">CORS or "跨域资源共享"</a> 指的是浏览器中运行的前端具有与后端进行通信的JavaScript代码，并且后端与前端具有不同的“来源”的情况。

## 起源

来源是通讯协定（`http`, `https`），网域（`myapp.com`, `localhost`, `localhost.tiangolo.com`）和通讯埠（`80`, `443`, `8080`）。

因此，所有这些都是不同的来源：

* `http://localhost`
* `https://localhost`
* `http://localhost:8080`

即使它们都在 `localhost` 中，它们也使用不同的协议或端口，因此它们是不同的“来源”。

## 步骤

因此，假设您在浏览器中的 `http://localhost:8080` 上运行了一个前端，并且其JavaScript试图与在 `http://localhost` 上运行的后端进行通信（因为我们未指定端口，浏览器将采用默认端口 `80`）。

然后，浏览器将向后端发送HTTP `OPTIONS` 请求，如果后端发送了适当的标头以授权来自此不同来源（ `http://localhost:8080` ）的通信，则浏览器将允许JavaScript进入 前端将其请求发送到后端。

为此，后端必须具有“允许的来源”列表。

在这种情况下，前端必须包含 `http://localhost:8080` 才能正常工作。

## 通配符

也可以将列表声明为 `"*"`（通配符），以表示所有内容都允许。

但这仅允许某些类型的通信，不包括涉及凭据的所有内容：Cookie，授权标头（如与Bearer令牌一起使用的那些标头等）。

因此，为了使一切正常工作，最好明确指定允许的来源。

## 使用 `CORSMiddleware`

您可以使用 `CORSMiddleware` 在您的 **FastAPI** 应用程序中对其进行配置。

* 导入`CORSMiddleware`。
* 创建允许的来源列表（作为字符串）。
* 将其作为“中间件”添加到您的 **FastAPI** 应用程序中。

您还可以指定后端是否允许：

* 凭证（授权标头，Cookie等）。
* 特定的HTTP方法（`POST`, `PUT`）或所有通配符 `"*"`。
* 特定的HTTP标头或所有通配符 `"*"`。

```Python hl_lines="2  6 7 8 9 10 11  13 14 15 16 17 18 19"
{!../../../docs_src/cors/tutorial001.py!}
```

默认情况下，`CORSMiddleware` 实现使用的默认参数是限制性的，因此您需要显式启用特定的来源，方法或标头，以便允许浏览器在跨域上下文中使用它们。

支持以下参数：

* `allow_origins` - 应该被允许发出跨域请求的起源列表。例如。 `['https://example.org','https://www.example.org']`。您可以使用`['*']`允许任何来源。
* `allow_origin_regex` - 一个正则表达式字符串，与应允许进行跨域请求的来源相匹配。例如。 `'https://.*\.example\.org'`。
* `allow_methods` - 跨域请求应允许的HTTP方法列表。默认为 `['GET']`。您可以使用 `['*']` 来允许所有标准方法。
* `allow_headers` - 跨域请求应支持的HTTP请求标头列表。默认为 `[]`。您可以使用 `['*']` 来允许所有标题。对于CORS请求，始终允许使用 `Accept`, `Accept-Language`, `Content-Language` 和 `Content-Type` 标头。
* `allow_credentials` - 表示跨域请求应支持cookie。默认为 `False`。
* `expose_headers` - 表示应该使浏览器可以访问的任何响应头。默认为 `[]`。
* `max_age` - 设置浏览器缓存CORS响应的最长时间（以秒为单位）。默认为 `600` 。

中间件响应两种特定类型的HTTP请求...

### CORS处理前要求

这些是带有 `Origin` 和 `Access-Control-Request-Method` 标头的任何 `OPTIONS` 请求。

在这种情况下，中间件将拦截传入的请求并使用适当的CORS标头进行响应，并出于提供信息的目的而响应 `200` 或 `400` 。

### 简单请求

任何带有 `Origin` 标头的请求。 在这种情况下，中间件将照常传递请求，但在响应中将包含适当的CORS标头。

## 更多信息

关于 <abbr title="Cross-Origin Resource Sharing">CORS</abbr>的更多信息，请查看 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS" class="external-link" target="_blank">Mozilla CORS documentation</a>.

!!! 请注意“技术细节”
    您还可以使用 `from starlette.middleware.cors import CORSMiddleware`。

    **FastAPI** 在 `fastapi.middleware` 中提供了几种中间件，以方便开发人员。 但是大多数可用的中间件直接来自Starlette。
