# 请求体 - 更新

## 更新为 `PUT`

可以使用 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT" class="external-link" target="_blank">HTTP `PUT`</a> 操作更新内容。

可以使用 `jsonable_encoder` 转换输入数据成为存储于JSON中的数据。例如：转换 `datetime` 为 `str`。

```Python hl_lines="30 31 32 33 34 35"
{!../../../docs_src/body_updates/tutorial001.py!}
```

`PUT` 用于接收应替换现有数据的数据。

### 关于替换的警告

这意味着如果要使用包含以下内容的主体，使用 `PUT` 更新项目 `bar` ：

```Python
{
    "name": "Barz",
    "price": 3,
    "description": None,
}
```

因为它不包含已经存储的属性 `"tax": 20.2`，所以输入模型将采用默认值 `"tax": 10.5`。

并且数据中已经存储了“新” `tax` of `10.5`。

## 使用 `PATCH` 进行部分更新

还可以使用 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH" class="external-link" target="_blank">HTTP `PATCH`</a> 操作符来“部分”地更新数据。

这意味着可以仅发送想要更新的数据，其余部分保持不变。

!!! 注释
    与 `PUT` 相比， `PATCH` 的使用和使用不太普遍。

    许多团队甚至只对部分更新使用 `PUT` 。

    您可以随意使用它们，** FastAPI **没有强加任何限制。

    但是本指南或多或少地向您展示了如何使用它们。

### 使用 Pydantic 的 `exclude_unset` 参数

如果要接收部分更新，在Pydantic模型的 `.dict()` 中使用参数 `exclude_unset` 非常有用。

像是 `item.dict(exclude_unset=True)`.

这将生成仅包含创建 `item` 模型时设置的数据的 `dict` ，不包括默认值。

然后，您可以使用它生成仅包含已设置（在请求中发送）的数据的 `dict` ，而忽略默认值：

```Python hl_lines="34"
{!../../../docs_src/body_updates/tutorial002.py!}
```

### 使用 Pydantic 的 `update` 参数

现在，您可以使用 `.copy()` 创建现有模型的副本，并通过带有包含要更新数据的 `dict`的 `update` 参数。

像是 `stored_item_model.copy(update=update_data)`：

```Python hl_lines="35"
{!../../../docs_src/body_updates/tutorial002.py!}
```

### 部分更新总结

总之，要应用部分更新，您将：

* （可选）使用 `PATCH` 而不是 `PUT` 。
* 检索存储的数据。
* 将该数据放入Pydantic模型中。
* 从输入模型中生成一个没有默认值的 `dict` （使用 `exclude_unset` ）。
    * 这样，您可以仅更新用户实际设置的值，而不是覆盖已经与默认值一起存储在模型中的值。
* 创建存储模型的副本，使用接收到的部分更新（使用 `update` 参数）更新其属性。
* 将复制的模型转换为可以存储在数据库中的模型（例如，使用 `jsonable_encoder` ）。
    * 这类似于再次使用模型的 `.dict()` 方法，但是它可以确保值（并将其转换为可以转换为JSON的数据类型，例如，将 `datetime` 转换为 `str` ）。
* 将数据保存到数据库。
* 返回更新的模型。

```Python hl_lines="30 31 32 33 34 35 36 37"
{!../../../docs_src/body_updates/tutorial002.py!}
```

!!! 提示
    实际上，您可以对HTTP `PUT` 操作使用相同的技术。

    但是这里的示例使用了 `PATCH` ，因为它是为这些用例创建的。

!!! 注意
    请注意，输入模型仍处于验证状态。

    因此，如果要接收可以忽略所有属性的部分更新，则需要有一个模型，其中所有属性都标记为可选（具有默认值或 `None` ）。

    要与具有 **updates** 的所有可选值的模型和具有 **creation** 的必需值的模型区分开，可以使用[扩展模型](extra-models.md){.internal-link target=_blank}。
