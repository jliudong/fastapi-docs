# 子依赖

您可以创建具有 **子依赖** 的依赖。

它们可以像您需要的那样 **deep** 。

**FastAPI**将负责解决它们。

### 第一个依赖项“dependable”

您可以创建第一个依赖项（“dependable”），例如：

```Python hl_lines="6 7"
{!../../../docs_src/dependencies/tutorial005.py!}
```

它声明一个可选的查询参数 `q` 作为 `str` ，然后返回它。

这很简单（不是很有用），但是将帮助我们专注于子依赖项的工作方式。

### 第二个依赖项，"dependable" 和 "dependant"

然后，您可以创建另一个依赖项函数（一个“dependable”），同时声明自己的依赖项（因此它也是一个“ dependant”）：

```Python hl_lines="11"
{!../../../docs_src/dependencies/tutorial005.py!}
```

让我们关注声明的参数：

* 即使此函数本身是一个依赖项（“dependable”），它也声明了另一个依赖项（它“依赖”其他东西）。
    * 取决于 `query_extractor` ，并将它返回的值赋给参数 `q` 。
* 它还声明了一个可选的 `last_query` cookie，作为 `str` 。
    * 假设如果用户未提供任何查询 `q` ，我们将使用上次使用的查询，该查询之前已保存到cookie中。

### 使用依赖

然后，我们可以将依赖项用于：

```Python hl_lines="19"
{!../../../docs_src/dependencies/tutorial005.py!}
```

!!! 信息
    注意，我们仅在 *路径操作函数* 中声明一个依赖项，即 `query_or_cookie_extractor` 。

    但是**FastAPI**会知道它必须首先解决 `query_extractor` ，然后在调用它时将其结果传递给`query_or_cookie_extractor`。

```mermaid
graph TB

query_extractor(["query_extractor"])
query_or_cookie_extractor(["query_or_cookie_extractor"])

read_query["/items/"]

query_extractor --> query_or_cookie_extractor --> read_query
```

## 多次使用相同的依赖项

例如，如果为同一个 *path操作* 多次声明一个依赖项，则多个依赖项具有一个公共的子依赖项，因此，**FastAPI**将知道每个请求仅调用一次该子依赖项。

它将返回的值保存在<abbr title="一个实用程序/系统中，用于存储计算/生成的值，以重新使用它们，而不是再次计算它们。">“缓存”</abbr>并将其传递给所有人 在特定请求中需要它的“依赖项”，而不是针对同一请求多次调用依赖项。

在高级场景中，您知道需要在同一请求中的每个步骤（可能多次）上调用依赖项，而不是使用“ cached”值，可以在使用 `Depends` 时设置参数 `use_cache=False`：

```Python hl_lines="1"
async def needy_dependency(fresh_value: str = Depends(get_value, use_cache=False)):
    return {"fresh_value": fresh_value}
```

## 总结

除了这里使用的所有花哨的单词以外， **Dependency Injection** 系统非常简单。

看上去与 *path operation functions* 相同的功能。

但是，它非常强大，并且允许您声明任意深度嵌套的依赖关系“图形”（树）。

!!! 提示
    这些简单的示例似乎对所有这些都没有用。

    但是您会在有关 **security** 的章节中看到它的用处。

    您还将看到它可以为您节省的代码量。
