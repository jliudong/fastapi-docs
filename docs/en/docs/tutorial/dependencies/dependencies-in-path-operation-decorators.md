# 路径操作装饰器中的依赖项

在某些情况下，您实际上不需要 *path操作函数* 中的依赖项的返回值。

或依赖项不返回值。

但是您仍然需要执行/解决它。

在这种情况下，您可以在 *path operation decorator* 中添加 `dependencies` 的 `list`，而不是通过 `Depends` 声明 *path operation decorator* 参数。

## 在 *path operation decorator* 中添加`dependencies`

路径操作装饰器接收可选参数 `dependencies` 。

它应该是 `Depends()` 的`list`：

```Python hl_lines="17"
{!../../../docs_src/dependencies/tutorial006.py!}
```

这些依赖关系将以与普通依赖关系相同的方式执行/解决。 但是它们的值（如果它们返回任何值）将不会传递给您的 *path operation function* 。

!!! 提示
    一些编辑器检查未使用的功能参数，并将其显示为错误。

    在 *path operation decorator* 中使用这些 `dependencies` ，可以确保在避免编辑器/工具错误的情况下执行了它们。

    对于那些在代码中看到未使用的参数并认为不必要的新开发人员，这也可能有助于避免混淆。

## 相关性错误和返回值

您可以使用与通常使用的相同的依赖功能。

### 依赖性要求

他们可以声明请求要求（例如header）或其他子依赖项：

```Python hl_lines="6 11"
{!../../../docs_src/dependencies/tutorial006.py!}
```

### 抛出异常

这些依赖项可以 `raise` 异常, 与普通依赖项相同：

```Python hl_lines="8 13"
{!../../../docs_src/dependencies/tutorial006.py!}
```

### 返回值

而且它们可以返回值或不返回值，这些值将不会被使用。

因此，您可以重复使用已经在其他地方使用的普通依赖项（返回一个值），即使不使用该值，该依赖项也将被执行：

```Python hl_lines="9 14"
{!../../../docs_src/dependencies/tutorial006.py!}
```

## 一组 *path operations* 的依赖项

稍后，当阅读有关如何构建更大的应用程序([更大的应用程序-多个文件](../../tutorial/bigger-applications.md){.internal-link target=_blank})的结构时，您可能会发现多个文件 将学习如何为一组 *path operations* 声明单个 `dependencies` 参数。
