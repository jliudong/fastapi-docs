# 带有yield的依赖项

FastAPI支持的依赖项在<abbr title='有时也会调用 "exit", "cleanup", "teardown", "close", "context managers", ...'>完成后会执行一些额外的步骤</abbr>。

为此，请使用 `yield` 而不是 `return` ，然后再写一些额外的步骤。

!!! 提示
    确保一次使用 `yield` 。

!!! 信息
    为此，您需要使用 **Python 3.7**或更高版本，或者在**Python 3.6**中安装"backports"：

    ```
    pip install async-exit-stack async-generator
    ```

    在此可以安装 <a href="https://github.com/sorcio/async_exit_stack" class="external-link" target="_blank">async-exit-stack</a> and <a href="https://github.com/python-trio/async_generator" class="external-link" target="_blank">async-generator</a>.

!!! 请注意 "技术细节"
    任何可用于以下用途的功能：

    * <a href="https://docs.python.org/3/library/contextlib.html#contextlib.contextmanager" class="external-link" target="_blank">`@contextlib.contextmanager`</a> 或
    * <a href="https://docs.python.org/3/library/contextlib.html#contextlib.asynccontextmanager" class="external-link" target="_blank">`@contextlib.asynccontextmanager`</a>

    可以有效地用作**FastAPI**依赖项。

    实际上，FastAPI在内部使用这两个装饰器。

## 具有 `yield` 的数据库依赖项

例如，您可以使用它来创建数据库会话并在完成后将其关闭。

在发送响应之前，仅执行 `yield` 语句之前并包括在内的代码：

```Python hl_lines="2 3 4"
{!../../../docs_src/dependencies/tutorial007.py!}
```

yielded值是注入到 *path operations* 和其他依赖项中的值：

```Python hl_lines="4"
{!../../../docs_src/dependencies/tutorial007.py!}
```

传递响应后，将执行 `yield` 语句之后的代码：

```Python hl_lines="5 6"
{!../../../docs_src/dependencies/tutorial007.py!}
```

!!! 提示
    您可以使用`async`或普通函数。

    **FastAPI**会对每种方法做正确的事情，与正常的依赖项相同。

## 带有 `yield` 和 `try` 的依赖项

如果在具有yield的依赖项中使用 `try` 块，则将收到使用依赖项时引发的任何异常。

例如，如果某个代码在中间，其他依赖项或 *path operation* 中的某个时刻使数据库事务“回滚”或创建任何其他错误，则您将在依赖项中收到异常。

因此，您可以使用 `except SomeException` 除外在依赖项中查找该特定异常。

以同样的方式，无论是否存在异常，您都可以使用 `finally` 来确保执行退出步骤。

```Python hl_lines="3 5"
{!../../../docs_src/dependencies/tutorial007.py!}
```

## 带有 `yield` 的子依赖项

您可以具有任意大小和形状的子依赖关系和子依赖关系的“树”，并且它们中的任何一个或全部都可以使用`yield`。

**FastAPI**将确保每个带有 `yield` 的依赖项中的“退出代码”以正确的顺序运行。

例如，`dependency_c`可以依赖于`dependency_b`，而`dependency_b`可以依赖于`dependency_a`：

```Python hl_lines="4 12 20"
{!../../../docs_src/dependencies/tutorial008.py!}
```

他们所有人都可以使用 `yield`。

在这种情况下，`dependency_c`要执行其退出代码，需要`dependency_b`中的值（此处称为`dep_b`）仍然可用。

而且，`dependency_b`需要`dependency_a`中的值（此处称为`dep_a`）可用于其退出代码。

```Python hl_lines="16 17 24 25"
{!../../../docs_src/dependencies/tutorial008.py!}
```

用同样的方式，您可能会混用`yield`和`return`的依赖关系。

而且您可能只有一个依赖项，而其他一些依赖项也有`yield`等。

您可以具有所需的依赖关系的任意组合。

**FastAPI**将确保一切均以正确的顺序运行。

!!! 请注意 "技术细节"
    这要归功于Python的 <a href="https://docs.python.org/3/library/contextlib.html" class="external-link" target="_blank">Context Managers</a>.

    **FastAPI** 在内部使用它们来实现此目的。

## 带有 `yield` and `HTTPException` 的依赖项

您已经看到可以将依存关系与 `yield` 结合使用，并具有 `try` 块来捕获异常。

在 `yield` 之后，可能会在退出代码中引发 `HTTPException` 或类似的东西。但是 **it won't work** 。

具有 `yield` 依赖性的退出代码是在* [异常处理程序](../handling-errors.md#install-custom-exception-handlers){.internal-link target=_blank}之后执行的。在退出代码中（ `yield` 之后），您的依赖项不会引发任何捕获异常。

因此，如果在 `yield` 后面引发 `HTTPException` ，则捕获 `HTTPException` 并返回HTTP 400响应的默认（或任何自定义）异常处理程序将不再能够捕获该异常。

这就是允许在依赖项（例如，数据库会话）中设置的任何内容供后台任务使用的方法。

发送响应后后运行后台任务。因此，没有办法引发 `HTTPException` ，因为甚至没有办法改变“已经发送”的响应。

但是，如果后台任务产生了数据库错误，至少您可以回退或完全清除带有 `yield` 的依赖关系中的会话，并可以记录该错误或将其报告给远程跟踪系统。

如果您知道某些代码可能引发异常，请执行最normal/"Pythonic"的操作，并在该部分代码中添加一个 `try` 块。

如果在返回响应并可能修改响应之前（如果有）想要处理自定义异常，甚至可能引发HTTPException，请创建一个[自定义异常处理程序](../handling-errors.md#install-custom-exception-handlers){.internal-link target=_blank}。

!!! 提示
    您仍然可以在`yield`之 *前* 引发包括 `HTTPException` 在内的异常。但不是之 *后* 。

执行顺序或多或少类似于此图。时间从上到下流动。每列都是交互或执行代码的一部分。

```mermaid
sequenceDiagram

participant client as Client
participant handler as Exception handler
participant dep as Dep with yield
participant operation as Path Operation
participant tasks as Background tasks

    Note over client,tasks: Can raise exception for dependency, handled after response is sent
    Note over client,operation: Can raise HTTPException and can change the response
    client ->> dep: Start request
    Note over dep: Run code up to yield
    opt raise
        dep -->> handler: Raise HTTPException
        handler -->> client: HTTP error response
        dep -->> dep: Raise other exception
    end
    dep ->> operation: Run dependency, e.g. DB session
    opt raise
        operation -->> handler: Raise HTTPException
        handler -->> client: HTTP error response
        operation -->> dep: Raise other exception
    end
    operation ->> client: Return response to client
    Note over client,operation: Response is already sent, can't change it anymore
    opt Tasks
        operation -->> tasks: Send background tasks
    end
    opt Raise other exception
        tasks -->> dep: Raise other exception
    end
    Note over dep: After yield
    opt Handle other exception
        dep -->> dep: Handle exception, can't change response. E.g. close DB session.
    end
```

!!! 信息
    仅**一个响应**将发送给客户端。 它可能是错误响应之一，也可能是 *path操作* 的响应。

    发送这些响应之一后，就无法再发送其他响应。

!!! 提示
    该图显示了 `HTTPException`，但是您也可以引发其他任何异常，以创建其 [自定义异常处理程序](../handling-errors.md#install-custom-exception-handlers){.internal-link target=_blank}。 而且，该异常将由该自定义异常处理程序处理，而不是依赖关系退出代码处理。

    但是，如果引发了异常处理程序未处理的异常，则该异常将由依赖项的退出代码处理。

## 上下文管理

### 什么是 "上下文管理"

"上下文管理" 是使用了`with`语句的Python对象中的一个。

例如， <a href="https://docs.python.org/3/tutorial/inputoutput.html#reading-and-writing-files" class="external-link" target="_blank">可以使用 `with` 来读取文件</a>:

```Python
with open("./somefile.txt") as f:
    contents = f.read()
    print(contents)
```

在下面，`open("./somefile.txt")` 创建一个称为“上下文管理器”的对象。

当 `with` 块结束时，即使有异常，也要确保关闭文件。

当使用 `yield` 创建依赖项时，**FastAPI**会将其内部转换为上下文管理器，并将其与其他一些相关工具组合。

### 在带有`yield`的依赖中使用上下文管理

!!! 警告
    这或多或少是一个“先进”的想法。

    如果您只是从**FastAPI**开始，那么现在可能要跳过它。

在Python语言中，可以创建上下文管理 <a href="https://docs.python.org/3/reference/datamodel.html#context-managers" class="external-link" target="_blank">创建带有 `__enter__()` 和 `__exit__()` 方法的类</a>。

您还可以通过以下方式在带有 `yield` 的**FastAPI**依赖项中使用它们：
依赖函数内部的 `with` 或 `async with` 语句：

```Python hl_lines="1 2 3 4 5 6 7 8 9 13"
{!../../../docs_src/dependencies/tutorial010.py!}
```

!!! 提示
    创建上下文管理的另一种方法是：

    * <a href="https://docs.python.org/3/library/contextlib.html#contextlib.contextmanager" class="external-link" target="_blank">`@contextlib.contextmanager`</a> or 
    * <a href="https://docs.python.org/3/library/contextlib.html#contextlib.asynccontextmanager" class="external-link" target="_blank">`@contextlib.asynccontextmanager`</a>

    用它们来装饰一个带有单个yield的函数。

    这就是**FastAPI**在内部用于带有 `yield` 的依赖关系。

    但是您不必将装饰器用于FastAPI依赖项（也不应该）。

    FastAPI将在内部为您完成此任务。
