# 依赖关系 - 第一步

**FastAPI** 具有非常强大但直观的 **<abbr title="也称为组件，资源，提供程序，服务，可注入对象">依赖注入</abbr>** 系统。

它的设计使用起来非常简单，并使任何开发人员都可以非常轻松地将其他组件与 **FastAPI** 集成在一起。

## 什么是 "依赖注入"

在编程领域，**"依赖注入"** 意味着，您的代码（在此情况下，您的*路径操作函数*）有一种方法可以声明其需要工作和使用的东西：“依赖项”。

然后，该系统（在本例中为 **FastAPI**）将负责完成为代码提供所需依赖项的所有操作（“注入”依赖项）。

当您需要：

* 具有共享逻辑（一次又一次相同的代码逻辑）。
* 共享数据库连接。
* 强制执行安全性，身份验证，角色要求等。
* 还有很多其他事情...

所有这些，同时最大程度地减少了代码重复。

## 第一步

让我们来看一个非常简单的例子。它是如此简单，以至于目前它还不是很有用。

但是这能让我们聚焦于 **"依赖注入"** 是如何工作的。

### 创建一个依赖项，或 “dependable”

让我们首先关注依赖关系。

它只是一个函数，可以采用 *路径操作函数* 可以采用的所有相同参数：

```Python hl_lines="6 7"
{!../../../docs_src/dependencies/tutorial001.py!}
```

如上。

**第2行**。

它具有与所有 *path操作功能* 相同的形状和结构。

可以将其视为没有“装饰器”的 *path操作函数*（没有`@app.get("/some-path")`）。

它可以返回您想要的任何东西。

在这种情况下，此依赖项期望：

* 可选查询参数 `q` 是 `str` 。
* 可选的查询参数 `skip` 是一个 `int` ，默认情况下是 `0` 。
* 可选查询参数 `limit` 是 `int` ，默认情况下是 `100` 。

然后，它仅返回包含这些值的 `dict` 。

### 导入 `Depends`

```Python hl_lines="1"
{!../../../docs_src/dependencies/tutorial001.py!}
```

### 声明依赖项，在"dependant"中

与将 `Body`，`Query` 等用于 *path operation function* 参数的方式相同，将 `Depends` 用于新参数：

```Python hl_lines="11 16"
{!../../../docs_src/dependencies/tutorial001.py!}
```

尽管您在函数的参数中使用 `Depends` 的方式与使用 `Body`, `Query` 等的方式相同，但是 `Depends` 的工作方式却有所不同。

您只给`Depends`一个参数。

此参数必须类似于函数。

该函数采用与“路径操作函数”相同的方式获取参数。

!!! 提示
    在下一章中，您将看到除功能之外的其他“事物”还可以用作依赖项。

每当有新请求到达时，**FastAPI** 就会处理：

* 使用正确的参数调用您的依赖项（“可依赖的”）函数。
* 从函数中获取结果。
* 将该结果分配给 *path操作函数* 中的参数。

```mermaid
graph TB

common_parameters(["common_parameters"])
read_items["/items/"]
read_users["/users/"]

common_parameters --> read_items
common_parameters --> read_users
```

这样，您只需编写一次共享代码，然后 **FastAPI** 便会为您的 *path操作* 进行调用。

!!! 检查
    请注意，您不必创建特殊的类并将其传递到 **FastAPI** 来“注册”它或类似的东西。

    您只需将其传递给 `Depends` ，**FastAPI** 就知道如何进行其余操作。

## 要 `async` 还是不要 `async`

由于依赖项还将由 **FastAPI** 调用（与 *path操作函数* 相同），因此在定义函数时将应用相同的规则。

您可以使用 `async def` 或普通的 `def` 。

您可以在普通的def路径操作函数内部使用async def声明依赖关系，或者在async def路径操作函数内部使用def依赖声明，等等。

没关系 **FastAPI** 会知道该怎么做。

!!! 注意
    如果您不知道，请检查[异步：*“急吗？”*]（../../ async.md）{.internal-link target=_blank}部分，了解 `async` 和`await` 在文档中。

## 与OpenAPI集成

您的依存关系（和子依存关系）的所有请求声明，验证和要求都将集成在同一OpenAPI架构中。

因此，交互式文档也将从这些依赖项中获取所有信息：

<img src="/img/tutorial/dependencies/image01.png">


## 简单用法

如果您看一下，只要 *path* 和 *operation* 匹配，就会声明使用 *path操作函数*，然后 **FastAPI** 会使用正确的参数来调用该函数并使用响应。

实际上，所有（或大多数）Web框架都以相同的方式工作。

您永远不会直接调用这些函数。 它们由您的框架（在本例中为 **FastAPI**）调用。

使用依赖注入系统，您还可以告诉 **FastAPI** ，您的 *path操作函数* 也“依赖”在您的 *path操作函数* 之前应执行的其他操作，并且 **FastAPI** 会注意 执行并“注入”结果。

相同的“依赖注入”概念的其他通用术语是：

* 资源
* 提供者
* 服务
* 可注入的
* 组件

## **FastAPI** 插件

可以使用 **Dependency Injection** 系统来构建集成和“插件”。 但实际上，实际上 **无需创建“插件”** ，因为通过使用依赖项，可以声明无限数量的集成和交互，这些集成和交互可用于 *路径操作函数* 。

并且可以以非常简单和直观的方式创建依赖关系，从而使您可以导入所需的Python包，并将它们与API函数集成为几行代码，即 *从字面上* 。

在下一章中，您将看到有关关系数据库和NoSQL数据库，安全性等的示例。

## **FastAPI** 兼容性

依赖项注入系统的简单性使 **FastAPI** 兼容：

* 所有关系数据库
* NoSQL数据库
* 外部第三方包
* 外部API
* 认证和授权系统
* API使用情况监控系统
* 响应数据注入系统
* 等

## 简单而强大

尽管分层依赖注入系统的定义和使用非常简单，但是它仍然非常强大。

您可以定义依赖关系，而依赖关系又可以自己定义依赖关系。

最后，构建了层次结构的依赖关系树， **Dependency Injection** 系统负责为您解决所有这些依赖关系（及其子依赖关系），并在每个步骤中提供（注入）结果。

例如，假设您有4个API端点（ *路径操作* ）：

* `/items/public/`
* `/items/private/`
* `/users/{user_id}/activate`
* `/items/pro/`

那么您可以为每个添加依赖项和子依赖项的不同权限要求：

```mermaid
graph TB

current_user(["current_user"])
active_user(["active_user"])
admin_user(["admin_user"])
paying_user(["paying_user"])

public["/items/public/"]
private["/items/private/"]
activate_user["/users/{user_id}/activate"]
pro_items["/items/pro/"]

current_user --> active_user
active_user --> admin_user
active_user --> paying_user

current_user --> public
active_user --> private
admin_user --> activate_user
paying_user --> pro_items
```

## 与 **OpenAPI** 集成

所有这些依赖项在声明其要求的同时，还向“路径操作”添加了参数，验证等。

**FastAPI** 负责将其全部添加到OpenAPI架构中，以便在交互式文档系统中显示它。
