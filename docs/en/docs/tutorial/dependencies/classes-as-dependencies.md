# 类作为依赖

在深入研究 **Dependency Injection** 系统之前，让我们升级前面的示例。

## 上例中的 `dict`

在前面的示例中，我们从依赖项（“dependable”）中返回一个 `dict` ：

```Python hl_lines="7"
{!../../../docs_src/dependencies/tutorial001.py!}
```

但是然后我们在 *路径操作函数* 的参数 `commons` 中得到一个 `dict` 。

而且我们知道编辑器不能为 `dict` 提供很多支持（例如完成），因为他们不知道其键和值类型。

我们可以做得更好...

## 是什么导致依赖

到目前为止，您已经看到依赖项声明为函数。

但这不是声明依赖关系的唯一方法（尽管可能更常见）。

关键因素是依赖项应该是“可调用的”。

Python中的“**callable**”是Python可以像函数一样“调用”的任何东西。

因此，如果您有一个对象 `something` （可能不是一个函数），并且可以像下面这样“调用”它（执行）：

```Python
something()
```

或

```Python
something(some_argument, some_keyword_argument="foo")
```

那么它是一个“可调用的”。

## 类作为依赖

您可能会注意到，使用相同的语法来创建Python类的实例。

例如：

```Python
class Cat:
    def __init__(self, name: str):
        self.name = name


fluffy = Cat(name="Mr Fluffy")
```

在这种情况下，`fluffy` 是 `Cat` 类的一个实例。

要创建 `fluffy`，您就是“呼唤” `Cat` 。

因此，Python类也是 **callable** 。

然后，在**FastAPI**中，您可以使用Python类作为依赖项。

FastAPI实际检查的是它是“可调用的”（函数，类或其他任何东西），并且定义了参数。

如果您在“FastAPI”中传递“可调用”作为依赖项，它将分析该“可调用”的参数，并以与“路径操作函数”的参数相同的方式处理它们。 包括子依赖项。

这也适用于根本没有参数的可调用对象。 与没有参数的“路径操作函数”相同。

然后，我们可以将依赖项“dependable” `common_parameters` 从上面更改为类`CommonQueryParameters`：

```Python hl_lines="9 10 11 12 13"
{!../../../docs_src/dependencies/tutorial002.py!}
```

注意 `__init__` 方法在类被实例化创建时调用：

```Python hl_lines="10"
{!../../../docs_src/dependencies/tutorial002.py!}
```

...它有相同的参数同前面的 `common_parameters`：

```Python hl_lines="6"
{!../../../docs_src/dependencies/tutorial001.py!}
```

这些参数是**FastAPI**用来“解决”依赖关系的参数。

在这两种情况下，它都将具有：

* 可选的q查询参数。
* 一个 `skip` 查询参数，默认值为 `0`。
* 一个 `limit` 查询参数，默认值为 `100`。

在这两种情况下，数据都将在OpenAPI架构上进行转换，验证和记录等。

## 使用它

现在，您可以使用此类声明依赖项。

并且当**FastAPI**调用该类时，将作为 `commons` 传递给您的函数的值将成为该类的“实例”，因此您可以声明参数 `commons` 为该类的类型， `CommonQueryParams`。

```Python hl_lines="17"
{!../../../docs_src/dependencies/tutorial002.py!}
```

## 类型注释与 `Depends`

在上面的代码中，您将`commons'声明为：

```Python
commons: CommonQueryParams = Depends(CommonQueryParams)
```

最后 `CommonQueryParams`，在：

```Python
... = Depends(CommonQueryParams)
```

...是**FastAPI**实际用于了解依赖项的内容。

从这里开始，FastAPI将提取声明的参数，而这正是FastAPI实际调用的参数。

---

在这个示例中，第一个`CommonQueryParams`位于：

```Python
commons: CommonQueryParams ...
```

...对于**FastAPI**没有任何特殊含义。 FastAPI不会将其用于数据转换，验证等（因为它正在使用`= Depends（CommonQueryParams）`）。

```Python
commons = Depends(CommonQueryParams)
```

..位于：

```Python hl_lines="17"
{!../../../docs_src/dependencies/tutorial003.py!}
```

但是鼓励声明类型，因为这样您的编辑器将知道将作为参数 `commons` 传递的内容，然后它可以帮助您完成代码，进行类型检查等：

<img src="/img/tutorial/dependencies/image02.png">

## 快捷方式

但是您会看到我们在这里有一些代码重复，两次写了`CommonQueryParams`：

```Python
commons: CommonQueryParams = Depends(CommonQueryParams)
```

**FastAPI** 为这些情况提供了一种快捷方式，在这些情况下，*特别地* 依赖项是 **FastAPI** 将“调用”以创建该类本身实例的类。

对于这些特定情况，您可以执行以下操作：

而不是写：

```Python
commons: CommonQueryParams = Depends(CommonQueryParams)
```

...可以写：

```Python
commons: CommonQueryParams = Depends()
```

因此，您可以将依赖项声明为变量的类型，并使用 `Depends()` 作为该函数参数的“默认”值（ `=` 后的值），而无需任何参数，而不必编写完整的类，再次在 `Depends(CommonQueryParams)` 内部。

因此，相同的示例如下所示：

```Python hl_lines="17"
{!../../../docs_src/dependencies/tutorial004.py!}
```

...**FastAPI**将知道该怎么做。

!!! 提示
    如果所有这些看起来比有用的都令人困惑，请忽略它，您不需要它。
    
    这只是捷径。 因为 **FastAPI** 关心帮助您最大程度地减少代码重复。
