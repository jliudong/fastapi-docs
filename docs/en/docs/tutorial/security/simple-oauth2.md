# 带密码和Bearer的Simple OAuth2

现在让我们从上一章开始构建，并添加缺少的部分以获得完整的安全流。

##获取 `username` 和 `password`

我们将使用 **FastAPI** 安全实用程序来获取 `username` 和 `password`。

OAuth2指定当使用“密码流”（我们正在使用）时，客户机/用户必须发送一个 `username` 和 `password` 字段作为表单数据。

规范上说这些字段必须这样命名。所以`user-name` 或 `email`不起作用。

但别担心，你可以在前端向你的最终用户展示它。

而且数据库模型可以使用任何其他名称。

但是对于登录的*path操作*，我们需要使用这些名称来与规范兼容（并且能够使用集成的API文档系统）。

规范还规定，`username` 和 `password` 必须作为表单数据发送（因此，这里没有JSON）。

### `scope`

规范还规定，客户端可以发送另一个表单字段 `scope` 。

表单字段名是`scope`（单数），但它实际上是一个长字符串，“scopes”由空格分隔。

每个“scope”只是一个字符串（没有空格）。

它们通常用于声明特定的安全权限，例如：

* `users:read` 或 `users:write` 是通用的范例。
* `instagram_basic` 被应用于 Facebook / Instagram。
* `https://www.googleapis.com/auth/drive` 被应用于 Google。

!!! 信息
    在OAuth2中，“scope”只是一个字符串，它声明了所需的特定权限。

    它是否有其他字符，比如`：`或者它是一个URL，这都无关紧要。

    这些细节是具体实现的。

    对于OAuth2来说，它们只是字符串。

## 为获取 `username` and `password` 编写代码

现在让我们使用 **FastAPI** 提供的实用程序来处理这个问题。

### `OAuth2PasswordRequestForm`

首先，导入 `OAuth2PasswordRequestForm`，并将其用作路径`/token`的依赖项（使用`Depends`）：

```Python hl_lines="2  74"
{!../../../docs_src/security/tutorial003.py!}
```

`OAuth2PasswordRequestForm` 是一个类依赖项，它声明具有以下内容的表单体：

* `username`.
* `password`.
* 可选的 `scope` 字段作为一个大字符串，由用空格分隔的字符串组成。
* 可选的 `grant_type`.

!!! 提示
    OAuth2规范实际上 *需要* 一个固定值为`password`的字段`grant_type`，但是`OAuth2PasswordRequestForm`不强制它。

    如果需要强制执行，请使用`OAuth2PasswordRequestFormStrict`而不是`OAuth2PasswordRequestForm`。

* 可选的 `client_id` (在示例中并不需要)。
* 可选的 `client_secret` (在示例中并不需要)。

!!! 信息
    `OAuth2PasswordRequestForm`与`OAuth2PasswordBearer`不同，不是**FastAPI**的特殊类。

    `OAuth2PasswordBearer`让 **FastAPI** 知道这是一个安全方案。所以它是以这种方式添加到OpenAPI中的。

    但`OAuth2PasswordRequestForm`只是一个类依赖项，您可以自己编写，也可以直接声明 `Form` 参数。

    但是由于这是一个常见的用例，所以它是由**FastAPI**直接提供的，只是为了让它更容易。

### 使用表单数据

!!! 提示
    依赖类 `OAuth2PasswordRequestForm` 的实例不会有一个用空格分隔的长字符串的属性`scope`，而是有一个`scopes`属性，其中包含每个发送的作用域的实际字符串列表。

    在本例中，我们没有使用“scopes”，但是如果您需要，它的功能就在那里。

现在，使用表单字段中的 `username` 从（假）数据库中获取用户数据。

如果没有这样的用户，我们会返回一个错误，并提示“不正确的用户名或密码”。

对于错误，我们使用异常 `HTTPException`：

```Python hl_lines="1  75 76 77"
{!../../../docs_src/security/tutorial003.py!}
```

### 检查密码

现在我们有数据库中的用户数据，但还没有检查密码。

我们先把数据放到Pydantic `UserInDB` 模型中。

您不应该保存纯文本密码，因此，我们将使用（伪）密码哈希系统。

如果密码不匹配，则返回相同的错误。

#### 密码哈希处理

“散列”的意思是：将一些内容（在本例中是密码）转换成一个看起来乱七八糟的字节序列（只是一个字符串）。

每当你传递完全相同的内容（完全相同的密码）你就会得到完全相同的哈希字符串。

但你不能从哈希字符串转换回密码。

##### 为什么使用密码哈希处理

如果你的数据库被盗，窃贼就不会有你用户的明文密码，只有散列字符串密文。

因此，窃贼将无法尝试在另一个系统中使用相同的密码（因为许多用户在任何地方都使用相同的密码，这将是危险的）。

```Python hl_lines="78 79 80 81"
{!../../../docs_src/security/tutorial003.py!}
```

#### 关于 `**user_dict`

`UserInDB(**user_dict)` 意味着：

*直接将 `user_dict` 的键和值作为键值参数传递，相当于：*

```Python
UserInDB(
    username = user_dict["username"],
    email = user_dict["email"],
    full_name = user_dict["full_name"],
    disabled = user_dict["disabled"],
    hashed_password = user_dict["hashed_password"],
)
```

!!! 信息
    有关 `**user_dict` 的更完整解释，请返回 check back in [**扩展模型**文档](../extra-models.md#about-user_indict){.internal-link target=_blank}.

## 返回令牌

`token`端点的响应必须是JSON对象。

它应该有一个`token`类型。在我们的例子中，当我们使用“不记名”令牌时，令牌类型应该是`bearer`。

它应该有一个`access_token`，其中有一个包含我们的访问令牌的字符串。

对于这个简单的示例，仅仅返回与令牌相同的`username`，实际生产环境这是不安全的。

!!! 提示
    在下一章中，您将看到一个真正的安全实现，其中包含密码散列和JWT<abbr title="JSON Web Tokens">JWT<abbr>令牌。

    但现在，让我们关注我们需要的具体细节。

```Python hl_lines="83"
{!../../../docs_src/security/tutorial003.py!}
```

!!! 提示
    根据规范，您应该返回一个带有 `access_token` 和 `token_type` 的JSON，与本例相同。

    这是您必须在代码中自己做的事情，并确保使用这些JSON键。

    这几乎是你必须记住的唯一一件事，你必须正确地做自己，以符合规范。

    剩下的，**FastAPI**为您处理。

## 更新依赖项

现在我们要更新依赖项。

我们只想在该用户处于活动状态时获取 `current_user`。

因此，我们创建了一个额外的依赖项`get_current_active_user`，然后使用`get_current_user`作为依赖项。

如果用户不存在或处于非活动状态，这两个依赖项都将返回一个HTTP错误。

因此，在我们的端点中，只有当用户存在、经过正确身份验证并且处于活动状态时，我们才能获取用户：

```Python hl_lines="56 57 58 59 60 61 62 63 64 65  67 68 69 70  88"
{!../../../docs_src/security/tutorial003.py!}
```

!!! 信息
    我们在这里返回的附加头`WWW-Authenticate`和值`Bearer`也是规范的一部分。

    任何HTTP（错误）状态代码 401 “UNAUTHORIZED” 也应该返回一个 `WWW-Authenticate` 头。

    在不记名代币的情况下（我们的情况），该头的值应该是`Bearer`。

    你可以跳过额外的标题，它仍然可以工作。

    但这里提供的是符合规格的。

    此外，可能有一些工具（现在或将来）期望并使用它，并且现在或将来可能对您或您的用户有用。

    这就是标准的好处。。。

## 行动起来

打开交互文档： <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>.

### 认证

单击“授权”按钮。

使用凭据：

用户：`johndoe`

密码：`secret`

<img src="/img/tutorial/security/image04.png">

在系统中进行身份验证后，您将看到如下情况：

<img src="/img/tutorial/security/image05.png">

### 获取自己的用户数据

现在对`/users/me`路径使用 `GET` 操作。

可以类似如下获得用户的数据。

```JSON
{
  "username": "johndoe",
  "email": "johndoe@example.com",
  "full_name": "John Doe",
  "disabled": false,
  "hashed_password": "fakehashedsecret"
}
```

<img src="/img/tutorial/security/image06.png">

如果单击锁定图标并注销，然后再次尝试相同的操作，则会出现HTTP 401错误：

```JSON
{
  "detail": "Not authenticated"
}
```

### 非活动用户

现在尝试使用非活动用户，使用以下身份验证：

用户：`alice`

密码：`secret2`

并尝试对路径 `/users/me` 使用 `GET` 操作。

将出现“非活动用户”错误，例如：

```JSON
{
  "detail": "Inactive user"
}
```

## 总结

现在，您可以使用工具为您的API实现基于 `username` 和 `password` 的完整安全系统。

使用这些工具，您可以使安全系统与任何数据库以及任何用户或数据模型兼容。

唯一缺少的细节是它还没有真正“安全”。

在下一章中，您将看到如何使用安全密码哈希库和JWT令牌。
