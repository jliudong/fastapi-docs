# 安全 - 第一步

假设您在某个域中拥有**backend** API。

并且您在另一个域中或同一个域（或在移动应用程序中）的不同路径中拥有一个“前端”。

而且，您希望使用**用户名**和**密码**来让前端与后端进行身份验证。

我们可以使用**OAuth2**和**FastAPI**来构建它。

但是，让我们节省阅读完整的长期规范的时间，仅是查找所需的那些小信息。

让我们使用**FastAPI**提供的工具来处理安全性。

## 看起来如何

让我们首先使用代码并查看其工作方式，然后再回来了解发生了什么。

## 创建 `main.py`

将示例复制到文件main.py中：

```Python
{!../../../docs_src/security/tutorial001.py!}
```

## 运行

!!! 信息
    首先安装 <a href="https://andrew-d.github.io/python-multipart/" class="external-link" target="_blank">`python-multipart`</a>.

    如： `pip install python-multipart`.

    这是因为**OAuth2**使用“表单数据”发送“用户名”和“密码”。

使用以下示例运行示例：

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

## 查看结果

前往交互文档 <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>.

类似如下：

<img src="/img/tutorial/security/image01.png">

!!! 检查“授权按钮！”
    您已经有了一个闪亮的新“授权”按钮。

    而且，您的*路径操作*在右上角有一个小锁，您可以单击它。

如果单击它，您会得到一些授权，可以输入`username`和`password`（以及其他可选字段）：

<img src="/img/tutorial/security/image02.png">

!!! 注意
    不管您在表格中键入什么，都将无法使用。 但是我们会到达那里。

当然，这不是最终用户的前端，但它是一个很好的自动工具，可以交互式地记录您所有的API。

前端团队（也可以是您自己）可以使用它。

第三方应用程序和系统可以使用它。

您也可以自己使用它来调试，检查和测试同一应用程序。

## `password` 流程

现在让我们回到过去，了解所有内容。

`password` “流”是OAuth2中定义的用于处理安全性和身份验证的方式（“流”）之一。

OAuth2的设计使后端或API可以独立于对用户进行身份验证的服务器。

但是在这种情况下，相同的**FastAPI**应用程序将处理API和身份验证。

因此，让我们从简化的角度进行回顾：

* 用户在前端输入 `username` 和 `password` ，然后按 `Enter` 。
* 前端（在用户浏览器中运行）将 `username` 和 `password` 发送到我们API中的特定URL。
* API检查 `username` 和 `password` ，并以“令牌”响应。
    * “令牌”只是一个包含一些内容的字符串，我们稍后可以使用它来验证该用户。
    * 通常，令牌设置为在一段时间后过期。
        * 因此，用户稍后将不得不再次登录。
        * 并且如果令牌被盗，则风险较小。它不像将永久使用的永久密钥（在大多数情况下）。
* 前端将该令牌临时存储在某个地方。
* 用户单击前端可转到前端Web应用程序的另一部分。
* 前端需要从API中获取更多数据。
    * 但是它需要对该特定端点进行身份验证。
    * 因此，为了使用我们的API进行身份验证，它会发送标头`Authorization`，其值为`Bearer`，加上令牌。
    * 如果令牌包含`foobar`，则`Authorization`标头的内容为：`Bearer foobar`。

## **FastAPI**的`OAuth2PasswordBearer`

**FastAPI**提供了几种工具，它们在不同的抽象级别上可以实现这些安全功能。

在此示例中，我们将使用带有**Bearer**令牌的**OAuth2**和**Password**流。

!!! 信息
    "bearer"令牌不是唯一的选择。

    但这是我们用例的最佳选择。

    除非您是OAuth2专家并且确切知道为什么还有另一种更适合您的需求，否则它可能是大多数用例的最佳选择。

    在这种情况下，**FastAPI**还为您提供了构建它的工具。

`OAuth2PasswordBearer` 是一个我们创建的类，它传递URL的参数，客户端（在用户浏览器中运行的前端）可以在其中使用该参数发送 `username` 和 `password` 并获取令牌。

```Python hl_lines="6"
{!../../../docs_src/security/tutorial001.py!}
```

它不会创建该端点 / *path operation*，但会声明该URL是客户端应用于获取令牌的URL。 该信息在OpenAPI中使用，然后在交互式API文档系统中使用。

!!! 信息
    如果您是非常严格的“Pythonista”用户，则可能不喜欢参数名称 `tokenUrl` 而不是 `token_url` 的样式。

    这是因为它使用的名称与OpenAPI规范中的名称相同。 这样，如果您需要更多地研究这些安全方案中的任何一种，就可以复制并粘贴它以找到有关此方案的更多信息。

`oauth2_scheme` 变量是 `OAuth2PasswordBearer` 的实例，但它也是“可调用的”。

它可以被称为：

```Python
oauth2_scheme(some, parameters)
```

因此，它可以与`Depends`一起使用。

### 使用它

现在，您可以通过带有 `Depends` 的依赖项传递该 `oauth2_scheme` 。

```Python hl_lines="10"
{!../../../docs_src/security/tutorial001.py!}
```

这种依赖性将提供一个`str`，该`str`被分配给“路径操作函数”的参数 `token` 。

**FastAPI**将知道它可以使用此依赖关系在OpenAPI架构（和自动API文档）中定义“安全方案”。

!!! 信息“技术细节”
    **FastAPI**会知道它可以使用类 `OAuth2PasswordBearer`（在依赖项中声明）来定义OpenAPI中的安全性方案，因为它继承自`fastapi.security.oauth2.OAuth2`，而后者又继承自`fastapi.security.base.SecurityBase`。

    与OpenAPI（和自动API文档）集成的所有安全实用程序都继承自 `SecurityBase` ，这就是**FastAPI**知道如何将其集成到OpenAPI中的方式。

## 它能做什么

它将查找请求中的 `Authorization` 标头，检查该值是否为 `Bearer` 加上一些令牌，并将令牌作为`str`返回。

如果没有看到 `Authorization` 标头，或者该值没有 `Bearer` 令牌，它将直接以401状态码错误（`UNAUTHORIZED`）响应。

您甚至不必检查令牌是否存在即可返回错误。 您可以确定，如果执行了函数，则该令牌中将带有一个 `str` 。

您可以在交互式文档中尝试它：

<img src="/img/tutorial/security/image03.png">

我们尚未验证令牌的有效性，但这已经是一个开始。

## 总结

因此，仅需增加3或4行，您就已经具有某种原始的安全性形式。
