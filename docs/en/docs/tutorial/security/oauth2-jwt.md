# 带密码（和散列）的OAuth2，带JWT令牌的Bearer

现在我们已经拥有了所有的安全流，让我们使用<abbr title="JSON Web Tokens">JWT</abbr>令牌和安全密码散列，使应用程序真正安全。

这些代码实际上可以在应用程序中使用，在数据库中保存密码散列，等等。

我们将从上一章的剩余部分开始并增加它。

## 关于JWT

JWT的意思是“"JSON Web Tokens"”。

将JSON对象编码为一个长而密的字符串（不带空格）是一个标准。看起来是这样的：

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
```

它没有加密，所以任何人都可以从内容中恢复信息。

但已经签了名。所以，当你收到一个你发出的令牌时，你可以验证你是否真的发出了它。

这样，您就可以创建一个有效期为1周的令牌。然后，当用户第二天带着令牌回来时，您知道他/她仍然登录到您的系统中。

一周后，令牌将过期，用户将无权再登录以获取新令牌。如果用户（或第三方）试图修改令牌以更改过期时间，您将能够发现它，因为签名不匹配。

如果您想使用JWT令牌并查看它们是如何工作的，请查看<a href="https://jwt.io/" class="external-link" target="_blank">https://jwt.io</a>。

## 安装 `PyJWT`

需要安装 `PyJWT` 来生成并验证JWT令牌的有效性:

<div class="termy">

```console
$ pip install pyjwt

---> 100%
```

</div>

## 密码散列

“散列”意味着将一些内容（在本例中是密码）转换成一个看起来乱七八糟的字节序列（只是一个字符串）。

每当你传递完全相同的内容（完全相同的密码）你就会得到完全相同的密文。

但你不能再从密文转换回密码。

## 为什么使用密码散列

If your database is stolen, the thief won't have your users' plaintext passwords, only the hashes.

So, the thief won't be able to try to use that password in another system (as many users use the same password everywhere, this would be dangerous).

## 安装 `passlib`

PassLib是一个很好的处理密码散列的Python包。

它支持许多安全的散列算法和实用程序。

推荐的算法是“Bcrypt”。

<div class="termy">

```console
$ pip install passlib[bcrypt]

---> 100%
```

</div>

!!! 提示
    使用 `passlib` ，您甚至可以将其配置为能够读取由**Django**、**Flask**安全插件或许多其他插件创建的密码。

    因此，例如，您将能够与FastAPI应用程序共享数据库中Django应用程序的相同数据。或者使用相同的数据库逐步迁移Django应用程序。

    同时，您的用户可以从您的Django应用程序或**FastAPI**应用程序登录。

## 哈希和验证密码

需要从 `passlib` 导入工具。

创建 PassLib “上下文”。 这将用于散列和验证密码。

!!! 提示
    PassLib上下文还具有使用不同散列算法的功能，包括只允许验证旧的散列算法，等等。

    例如，可以使用它来读取和验证另一个系统（如Django）生成的密码，但可以使用不同的算法（如Bcrypt）散列任何新密码。

    同时与所有这些都兼容。

创建一个实用函数来散列用户的密码。

以及另一个实用程序来验证接收到的密码是否与存储的哈希匹配。

另一个用于验证和返回用户。

```Python hl_lines="7  48  55 56  59 60  69 70 71 72 73 74 75"
{!../../../docs_src/security/tutorial004.py!}
```

!!! 注释
    如果您检查新的（假）数据库 `fake_users_db` ，您将看到哈希密码现在的样子：`"$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW"`.

## 处理JWT令牌

导入已安装的模块。

创建一个随机密钥，用于对JWT令牌进行签名。

要生成安全随机密钥，请使用以下命令：

<div class="termy">

```console
$ openssl rand -hex 32

09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7
```

</div>

并将输出复制到变量 `SECRET_KEY` （不要使用示例中的密钥）。

使用用于签名JWT令牌的算法创建一个变量 `ALGORITHM` ，并将其设置为 `"HS256"` 。

为令牌过期创建变量。

定义将在响应的令牌终结点中使用的Pydantic模型。

创建实用程序函数以生成新的访问令牌。

```Python hl_lines="3  6  12 13 14  28 29 30  78 79 80 81 82 83 84 85 86"
{!../../../docs_src/security/tutorial004.py!}
```

## 更新依赖项

更新 `get_current_user` 以接收与之前相同的令牌，但这次是使用JWT令牌。

解码接收到的令牌，验证它，并返回当前用户。

如果令牌无效，请立即返回HTTP错误。

```Python hl_lines="89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106"
{!../../../docs_src/security/tutorial004.py!}
```

## 更新 `/token` *路径操作*

使用令牌的过期时间创建一个 `timedelta` 。

```Python hl_lines="115 116 117 118 119 120 121 122 123 124 125 126 127 128"
{!../../../docs_src/security/tutorial004.py!}
```

### JWT“对象”的技术细节

JWT规范指出，有一个带有令牌主题的key `sub`。

使用它是可选的，但这是您可以放置用户标识的地方，所以我们在这里使用它。

JWT除了可以识别用户并允许他直接在API上执行操作外，还可以用于其他事情。

例如，您可以识别“汽车”或“博客文章”。

然后可以添加对该实体的权限，例如“drive”（用于汽车）或“edit”（用于博客）。

然后，您可以将JWT令牌给用户（或bot），他可以使用它来执行这些操作（驾驶汽车或编辑博客文章），甚至无需拥有帐户，只需使用您的API为此生成的JWT令牌。

使用这些思想，JWT可以用于更复杂的场景。

在这些情况下，这些实体中的几个可能具有相同的ID，比如说 `foo` （用户 `foo` 、汽车 `foo` 和博客文章 `foo` ）。

因此，为了避免ID冲突，在为用户创建JWT令牌时，可以在“sub”键的值前面加上`username:`前缀。因此，在本例中，“sub”的值可以是：`username:johndoe`。

要记住的重要一点是，`sub` 键应该在整个应用程序中有一个唯一的标识符，并且应该是一个字符串。

## 查看结果

运行服务并打开自动化文档： <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank">http://127.0.0.1:8000/docs</a>.

可以看到用户接口类似如下：

<img src="/img/tutorial/security/image07.png">

以与以前相同的方式授权应用程序。

使用凭据：

Username: `johndoe`
Password: `secret`

!!! 检查
    注意，代码中没有明文密码 `secret`，我们只有散列版本。

<img src="/img/tutorial/security/image08.png">

调用端点 `/users/me/`，获取类似如下响应：

```JSON
{
  "username": "johndoe",
  "email": "johndoe@example.com",
  "full_name": "John Doe",
  "disabled": false
}
```

<img src="/img/tutorial/security/image09.png">

如果打开开发人员工具，您可以看到数据是如何发送的，并且只包括令牌，密码只在第一次请求中发送，以验证用户身份并获取该访问令牌，但之后不会：

<img src="/img/tutorial/security/image10.png">

!!! 注释
    注意标题 `Authorization` ，其值以 `Bearer ` 开头。

## 高级`scopes`作用域用法

OAuth2有“scopes”的概念。

您可以使用它们向JWT令牌添加特定的权限集。

然后，您可以将此令牌直接提供给用户或第三方，以便使用一组限制与您的API进行交互。

稍后，您可以在**高级用户指南**中学习如何使用它们以及如何将它们集成到**FastAPI**。

## 总结

根据您目前看到的情况，您可以使用OAuth2和JWT等标准设置一个安全的**FastAPI**应用程序。

在几乎任何框架中，处理安全性都会很快成为一个相当复杂的主题。

许多简化了它的包不得不在数据模型、数据库和可用特性方面做出许多让步。其中一些过于简化的包实际上有安全缺陷。

---

**FastAPI**不会与任何数据库、数据模型或工具妥协。

它给你所有的灵活性来选择最适合你的项目。

而且您可以直接使用许多维护良好且广泛使用的包，如 `passlib` 和 `pyjwt` ，因为**FastAPI**不需要任何复杂的机制来集成外部包。

但它为您提供了尽可能简化流程的工具，而不会影响灵活性、健壮性或安全性。

您还可以使用和实现安全的标准协议，比如OAuth2，方法相对简单。

您可以在**高级用户指南**中了解更多关于如何使用OAuth2“作用域”的信息，以便按照这些相同的标准来实现更细粒度的权限系统。带作用域的OAuth2是许多大型身份验证提供商（如Facebook、Google、GitHub、Microsoft、Twitter等）用来授权第三方应用程序代表其用户与其api交互的机制。
