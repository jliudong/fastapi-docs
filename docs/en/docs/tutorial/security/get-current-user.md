# 获取当前用户

在上一章中，安全系统（基于依赖项注入系统）为 *path操作函数* 提供了一个`token`作为`str`：

```Python hl_lines="10"
{!../../../docs_src/security/tutorial001.py!}
```

但这仍然没有用。

让它给我们当前的用户。

## 创建用户模型

首先，让我们创建一个Pydantic用户模型。

与使用Pydantic声明主体的方式相同，我们可以在其他任何地方使用它：

```Python hl_lines="5 12 13 14 15 16"
{!../../../docs_src/security/tutorial002.py!}
```

## 创建 `get_current_user` 依赖项

让我们创建一个依赖项`get_current_user`。

还记得依赖可以有子依赖吗？

`get_current_user` 将具有与我们之前创建的 `oauth2_scheme` 相同的依赖项。

与我们之前直接在 *path操作* 中所做的相同，我们新的依赖项 `get_current_user` 将从子依赖项`oauth2_scheme` 接收一个`token` 作为 `str` ：

```Python hl_lines="25"
{!../../../docs_src/security/tutorial002.py!}
```

## 获取指定用户

`get_current_user` 将使用我们创建的（伪）实用程序函数，该函数将令牌作为str并返回我们的Pydantic `User` 模式：

```Python hl_lines="19 20 21 22 26 27"
{!../../../docs_src/security/tutorial002.py!}
```

## 注入当前用户

因此，现在我们可以在 *path操作* 中将相同的`Depends` 与我们的`get_current_user`一起使用：

```Python hl_lines="31"
{!../../../docs_src/security/tutorial002.py!}
```

注意，我们将 `current_user` 的类型声明为Pydantic模型 `User` 。

这将帮助我们使用所有完成和类型检查功能。

!!! 提示
     您可能还记得，请求主体也使用Pydantic模型声明。

     这里的FastAPI不会混淆，因为您使用的是 `Depends` 。

!!! 检查
     这种依赖系统的设计方式使我们可以拥有不同的依赖（不同的“依赖”），它们都返回一个 `User` 模型。

     我们不仅限于只能返回一种类型的数据的依赖项。


## Other 模型

现在，您可以直接在 *path operation functions* 中获取当前用户，并使用 `Depends` 在**Dependency Injection**级别处理安全性机制。

并且您可以使用任何模型或数据来满足安全性要求（在这种情况下，为Pydantic模型 `用户` ）。

但是您不限于使用某些特定的数据模型，类或类型。

您是否要在模型中使用 `id` 和 `email` 而不使用任何用户名？ 当然。 您可以使用这些相同的工具。

您是否只想拥有一个`str`？ 还是只是一个 `dict` ？ 还是直接一个数据库类模型实例？ 这一切都以相同的方式进行。

您实际上没有用户登录到您的应用程序，而是只有访问令牌的机器人，机器人或其他系统吗？ 同样，它们的工作原理相同。

只需使用您的应用程序所需的任何模型，任何类，任何数据库即可。 **FastAPI**覆盖了依赖项注入系统。

## 代码大小

这个例子似乎很冗长。 请记住，我们在同一文件中混合了安全性，数据模型实用程序功能和 *path operations* 。

但这是关键。

安全性和依赖项注入内容只编写一次。

您可以根据需要使其变得复杂。 而且，它只能在一个地方写一次。 具有所有的灵活性。

但是，使用同一安全系统，您可以有数千个端点（*路径操作*）。

所有这些（或您想要的任何部分）都可以利用重新使用这些依赖项或您创建的任何其他依赖项的优势。

所有这数千个 *path操作* 可能只有3行：

```Python hl_lines="30 31 32"
{!../../../docs_src/security/tutorial002.py!}
```

## 总结

现在，您可以直接在*path operation function*中获取当前用户。

我们已经到了一半。

我们只需要为用户/客户端添加一个“路径操作”，即可实际发送`username` 和 `password`。

接下来。
