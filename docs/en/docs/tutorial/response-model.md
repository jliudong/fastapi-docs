# 响应模型

您可以在任何 *path operations* 中使用参数 `response_model` 声明用于响应的模型：

* `@app.get()`
* `@app.post()`
* `@app.put()`
* `@app.delete()`
* 等等...

```Python hl_lines="17"
{!../../../docs_src/response_model/tutorial001.py!}
```

!!! 注释
    注意， `response_model` 是“装饰器”方法的参数（`get` ，`post` 等）。 像所有参数和主体一样，不属于 *path operation function（路径操作函数）*。

它接收的类型与您为Pydantic模型属性声明的类型相同，因此它可以是Pydantic模型，但也可以是例如 一个Pydantic模型的 `list` ，例如 `List[Item]` 。

FastAPI将使用`response_model`来：

*将输出数据转换为其类型声明。
*验证数据。
*在OpenAPI *path operation（路径操作）*中为响应添加一个JSON模式。
*将被自动文档系统使用。

但最重要的是：

*将输出数据限制为模型的数据。 我们将在下面看到其重要性。

!!! 注释“技术细节”
    在此参数中声明响应模型，而不是将其声明为函数返回类型注释，因为path函数实际上可能不会返回该响应模型，而是返回 `dict` ，数据库对象或其他模型，然后使用 `response_model` 。 执行字段限制和序列化。

## 返回相同的输入数据

在这里，我们声明一个 `UserIn` 模型，它将包含一个纯文本密码：

```Python hl_lines="7 9"
{!../../../docs_src/response_model/tutorial002.py!}
```

我们正在使用此模型声明输入，并使用同一模型声明输出：

```Python hl_lines="15 16"
{!../../../docs_src/response_model/tutorial002.py!}
```

现在，每当浏览器使用密码创建用户时，API都会在响应中返回相同的密码。

在这种情况下，这可能不是问题，因为用户自己正在发送密码。

但是，如果我们对另一个 *path operation* 使用相同的模型，则可能会将用户的密码发送给每个客户端。

!!! 危险
    切勿存储用户的明文密码或将其发送给用户。

## 添加输出模型

我们可以改用纯文本密码创建输入模型，而没有明文密码则创建输出模型：

```Python hl_lines="7 9 14"
{!../../../docs_src/response_model/tutorial003.py!}
```

在这里，即使我们的 *path operation function* 返回的是包含密码的相同输入用户：

```Python hl_lines="22"
{!../../../docs_src/response_model/tutorial003.py!}
```

...我们将 `response_model` 声明为我们的模型 `UserOut` ，其中不包含密码：

```Python hl_lines="20"
{!../../../docs_src/response_model/tutorial003.py!}
```

因此，**FastAPI** 将负责过滤掉未在输出模型中声明的所有数据（使用Pydantic）。

## 在文档中来看

当您看到自动文档时，可以检查输入模型和输出模型是否都具有自己的JSON模式：

<img src="/img/tutorial/response-model/image01.png">

两种模型都将用于交互式API文档：

<img src="/img/tutorial/response-model/image02.png">

## 响应模型编码参数

您的响应模型可能具有默认值，例如：

```Python hl_lines="11 13 14"
{!../../../docs_src/response_model/tutorial004.py!}
```

* `description: str = None` 有默认值 `None`.
* `tax: float = 10.5` 有默认值 `10.5`.
* `tags: List[str] = []` 有默认值一个空列表: `[]`.

但是如果它们实际上没有存储，则可能要从结果中忽略它们。

例如，如果您的模型在NoSQL数据库中具有很多可选属性，但是您不想发送很长的JSON响应（包含默认值）。

### 使用 `response_model_exclude_unset` 参数

可以设置 *path operation decorator（路径操作装饰器）* 参数 `response_model_exclude_unset=True`:

```Python hl_lines="24"
{!../../../docs_src/response_model/tutorial004.py!}
```

而这些默认值将不包括在响应中，仅包含实际设置的值。

因此，如果您向 *path operation（路径操作）* 发送ID为 `foo` 的商品的请求，则响应（不包括默认值）将为：

```JSON
{
    "name": "Foo",
    "price": 50.2
}
```

!!! 信息
    FastAPI 使用Pydantic model的 `.dict()` 带有 <a href="https://pydantic-docs.helpmanual.io/usage/exporting_models/#modeldict" class="external-link" target="_blank">它的 `exclude_unset` 参数</a> 达到这个.

!!! 信息
    还可以使用：

    * `response_model_exclude_defaults=True`
    * `response_model_exclude_none=True`

    如 <a href="https://pydantic-docs.helpmanual.io/usage/exporting_models/#modeldict" class="external-link" target="_blank">Pydantic 文档</a> 对`exclude_defaults` 和 `exclude_none` 所述。

#### 具有默认值的字段的值的数据

但是，如果您的数据具有默认值的模型字段值，例如ID为 `bar` 的项：

```Python hl_lines="3 5"
{
    "name": "Bar",
    "description": "The bartenders",
    "price": 62,
    "tax": 20.2
}
```

它们将包含在响应中。

#### 具有与默认值相同的值的数据

如果数据具有与默认值相同的值，例如ID为 `baz` 的项：

```Python hl_lines="3 5 6"
{
    "name": "Baz",
    "description": None,
    "price": 50.2,
    "tax": 10.5,
    "tags": []
}
```

FastAPI足够聪明（实际上，Pydantic足够聪明）可以认识到，即使 `description`, `tax`, 和 `tags` 具有与默认值相同的值，它们也是显式设置的（而不是取自默认值） 。

因此，它们将包含在JSON响应中。

!!! 提示
    注意默认值可以是任意值，不仅仅是 `None`。

    可以是一个列表 (`[]`), 一个 `10.5` 的 `float` 等。

### `response_model_include` 和 `response_model_exclude`

您还可以使用 *path operation decorator（路径操作装饰器）* 参数  `response_model_include` 和 `response_model_exclude`。

它们使用带有属性名称 `str` 的 `set` 集合来包括（省略其余部分）或排除（包括其余部分）。

如果您只有一个Pydantic模型，并且想要从输出中删除一些数据，则可以将其用作快速捷径。

!!! 提示
    但是仍然建议使用上述想法，使用多个类而不是这些参数。

    这是因为即使使用 `response_model_include` 或 `response_model_exclude` 来省略某些属性，在应用程序的OpenAPI（和文档）中生成的JSON模式仍将是完整模型的JSON模式。

```Python hl_lines="29 35"
{!../../../docs_src/response_model/tutorial005.py!}
```

!!! 提示
    语法 `{"name", "description"}` 创建一个带有两个值的 `set` 。

    它等价于 `set(["name", "description"])`。

#### 使用 `list` 代替 `set`

如果您忘记使用 `set` ，而是使用 `list` 或 `tuple` ，FastAPI仍会将其转换为 `set` 并且可以正常工作：

```Python hl_lines="29 35"
{!../../../docs_src/response_model/tutorial006.py!}
```

## 总结

使用路径操作修饰符的参数 `response_model` 定义响应模型，尤其是确保私有数据被过滤掉。

使用 `response_model_exclude_unset` 仅返回显式设置的值。
