# 测试

感谢<a href="https://www.starlette.io/testclient/" class="external-link" target="_blank"> Starlette </a>，测试 **FastAPI** 应用程序变得轻松而愉快。

它基于<a href="http://docs.python-requests.org" class="external-link" target="_blank">请求</a>，因此非常熟悉和直观。

借助它，您可以直接将<a href="https://docs.pytest.org/" class="external-link" target="_blank"> pytest </a>与 **FastAPI** 一起使用。

## 使用 `TestClient`

导入 `TestClient`。

创建一个 `TestClient` 传到 **FastAPI**。

创建名称以 `test_` 开头的函数（这是标准的 `pytest` 约定）。

使用 `TestClient` 对象的方式与处理 `requests` 的方式相同。

用您需要检查的标准Python表达式编写简单的 `assert` 语句（再次，标准`pytest`）。

```Python hl_lines="2  12  15 16 17 18"
{!../../../docs_src/app_testing/tutorial001.py!}
```

!!! 提示
    注意，测试功能是普通的 `def`，而不是 `async def`。

    和客户端的调用也是普通调用，不使用 `await`。

    这使您可以直接使用 `pytest` 而不会有任何复杂性。

!!! 请注意“技术细节”
    您也可以使用 `from starlette.testclient import TestClient`。

    **FastAPI** 提供与 `fastapi.testclient` 相同的 `starlette.testclient`，以方便开发人员。 但它直接来自Starlette。

## 分离测试

在实际的应用程序中，您可能会将测试放在另一个文件中。

而且您的 **FastAPI** 应用程序也可能由多个文件/模块等组成。

### **FastAPI** 应用程序文件

假设您的 **FastAPI** 应用有一个文件 `main.py`：

```Python
{!../../../docs_src/app_testing/main.py!}
```

### 测试文件

然后，您可以将带有测试的文件 `test_main.py` 从 `main` 模块（`main.py`）导入 `app`：

```Python
{!../../../docs_src/app_testing/test_main.py!}
```

## 测试：扩展示例

现在，让我们扩展该示例并添加更多详细信息，以了解如何测试不同的部分。

### 扩展 **FastAPI** 应用程序文件

假设您的 **FastAPI** 应用有一个文件 `main_b.py`。

它具有 `GET` 操作，该操作可能返回错误。

它具有 `POST` 操作，可能会返回多个错误。

两个路径操作都需要一个 `X-Token` header。

```Python
{!../../../docs_src/app_testing/main_b.py!}
```

### 扩展测试文件

然后，您可以使用扩展测试获得一个与以前相同的 `test_main_b.py`：

```Python
{!../../../docs_src/app_testing/test_main_b.py!}
```

每当您需要客户在请求中传递信息而又不知道如何传递信息时，都可以在（`requests`）中搜索（Google）如何执行信息。

然后，您只需在测试中执行相同的操作即可。

例如：

* 要传递 *path* 或 *query* 参数，请将其添加到URL本身。
* 要传递JSON正文，请将Python对象（例如 `dict`）传递给参数 `json`。
* 如果您需要发送 *Form Data* 而不是JSON，请改用 `data` 参数。
* 要传递 *headers*，请在 `headers` 参数中使用 `dict`。
* 对于 *cookies*，在 `cookies` 参数中为 `dict`。

获取更多关于如何传递数据到后台 (使用 `requests` 或 `TestClient`) 查看 <a href="http://docs.python-requests.org" class="external-link" target="_blank">Requests documentation</a>。

!!! 信息
    请注意，`TestClient`接收的数据可以转换为JSON，而不是Pydantic模型。

    如果您在测试中有Pydantic模型，并且想要在测试过程中将其数据发送到应用程序，则可以使用[JSON兼容编码器](encoder.md){.internal-link target=_blank}中描述的`jsonable_encoder` 。

## 运行

之后，您只需要安装 `pytest`：

<div class="termy">

```console
$ pip install pytest

---> 100%
```

</div>

它将检测文件并自动进行测试，执行它们，并将结果报告给您。

使用以下命令运行测试：

<div class="termy">

```console
$ pytest

================ test session starts ================
platform linux -- Python 3.6.9, pytest-5.3.5, py-1.8.1, pluggy-0.13.1
rootdir: /home/user/code/superawesome-cli/app
plugins: forked-1.1.3, xdist-1.31.0, cov-2.8.1
collected 6 items

---> 100%

test_main.py <span style="color: green; white-space: pre;">......                            [100%]</span>

<span style="color: green;">================= 1 passed in 0.03s =================</span>
```

</div>
