# JSON兼容编码器

在某些情况下，您可能需要将数据类型（例如Pydantic模型）转换为与JSON兼容的数据（例如`dict`, `list`等）。

例如，如果您需要将其存储在数据库中。

为此 **FastAPI** 提供 `jsonable_encoder()` 函数.

## 使用 `jsonable_encoder`

假设有一个数据库 `fake_db` 仅仅接收json兼容编码数据。

例如，它不接收 `datetime` 对象，因为它们与JSON不兼容。

因此， `datetime` 对象必须转换为包含 <a href="https://en.wikipedia.org/wiki/ISO_8601" class="external-link" target="_blank">ISO format</a>数据格式的 `str` 对象中。

同样，该数据库将不会接收Pydantic模型（具有属性的对象），而只会接收 `dict`。

您可以使用 `jsonable_encoder` 。

它接收一个对象，例如Pydantic模型，并返回JSON兼容版本：

```Python hl_lines="4 21"
{!../../../docs_src/encoder/tutorial001.py!}
```

在此示例中，它将Pydantic模型转换为 `dict` ，将 `datetime` 转换为 `str` 。

调用它的结果是可以用Python标准编码的<a href="https://docs.python.org/3/library/json.html#json.dumps" class="external-link" target="_blank">`json.dumps()`</a>.

它不会返回包含JSON格式数据（字符串）的大型 `str` 。 它返回一个Python标准数据结构（例如 `dict`），其值和子值都与JSON兼容。

!!! 注释
    **FastAPI** 实际上在内部使用 `jsonable_encoder` 来转换数据。但这在许多其他情况下很有用。
