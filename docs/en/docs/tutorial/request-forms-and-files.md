# 请求表单和文件

您可以使用 `File` 和 `Form` 同时定义文件和表单字段。

!!! 信息
    要接收上传的文件和/或表单数据，请先安装 <a href="https://andrew-d.github.io/python-multipart/" class="external-link" target="_blank">`python-multipart`</a>。

    例如： `pip install python-multipart`.

## 从 `Form` 导入 `File`

```Python hl_lines="1"
{!../../../docs_src/request_forms_and_files/tutorial001.py!}
```

## 定义`File`和`Form`参数

创建文件和表单参数的方式与使用 `Body` 或 `Query` 的方式相同：

```Python hl_lines="8"
{!../../../docs_src/request_forms_and_files/tutorial001.py!}
```

文件和表单字段将作为表单数据上载，您将收到文件和表单字段。

您可以将某些文件声明为 `bytes` ，而另一些声明为 `UploadFile` 。

!!! 警告
    您可以在 *path operation* 中声明多个 `File` 和 `Form` 参数，但也不能声明希望以JSON形式接收的Body字段，因为请求的主体将使用 `multipart/form-data` 代替 `application/json`。

    这不是 **FastAPI** 的限制，它是HTTP协议的一部分。

## 总结

当您需要在同一请求中接收数据和文件时，可以同时使用 `File` 和 `Form` 。
