# 请求文件

您可以使用`File`定义客户端上传的文件。

!!! 信息
    要接收上传的文件，请先安装 <a href="https://andrew-d.github.io/python-multipart/" class="external-link" target="_blank">`python-multipart`</a>.

    例如 `pip install python-multipart`.

    这是因为上载的文件作为“表单数据”发送。

## 导入 `File`

从 `fastapi` 导入 `File` 和 `UploadFile` :

```Python hl_lines="1"
{!../../../docs_src/request_files/tutorial001.py!}
```

## 定义 `File` 参数

创建文件参数的方式与使用 `Body` 或 `Form` 的方式相同：
Create file parameters the same way you would for `Body` or `Form`:

```Python hl_lines="7"
{!../../../docs_src/request_files/tutorial001.py!}
```

!!! 信息
    `File` 是直接从 `Form` 继承的类。

    但是请记住，当您从 `fastapi` 导入 `Query`，`Path`，`File`和其他文件时，这些实际上是返回特殊类的函数。

!!! 提示
    要声明文件主体，您需要使用`File`，因为否则参数将被解释为查询参数或主体（JSON）参数。

这些文件将作为“表单数据”上传。

如果您将 *path operation function（路径操作函数）* 参数的类型声明为 `bytes`，则**FastAPI** 将为您读取文件，并且您将以 `bytes` 形式接收内容。

请记住，这意味着全部内容将存储在内存中。这将适用于小文件。

但是在某些情况下，您可以从使用UploadFile中受益。

## `File` 参数和 `UploadFile`

使用 `UploadFile` 定义 `File` 参数：

```Python hl_lines="12"
{!../../../docs_src/request_files/tutorial001.py!}
```

与`bytes`相比，使用`UploadFile`有几个优点：

* 它使用“假脱机”文件：
    * 存储在内存中的文件最大大小的限制，超过此限制后，它将存储在磁盘中。
* 这意味着它可以很好地用于大型文件，例如图像，视频，大型二进制文件等，而不会占用所有内存。
* 您可以从上传的文件中获取元数据。
* 它具有 <a href="https://docs.python.org/3/glossary.html#term-file-like-object" class="external-link" target="_blank">file-like</a> `async` 接口.
* 它公开了一个实际的Python <a href="https://docs.python.org/3/library/tempfile.html#tempfile.SpooledTemporaryFile" class="external-link" target="_blank">`SpooledTemporaryFile`</a> 对象，您可以将其直接传递给需要类似文件的对象的其他库。

### `UploadFile`

`UploadFile` 具有以下属性：

* `filename`: 具有上载原始文件名的`str`（例如`myimage.jpg`）。
* `content_type`: 具有内容类型（MIME类型/媒体类型）（例如，`image/jpeg`）的`str`。
* `file`: 一个 <a href="https://docs.python.org/3/library/tempfile.html#tempfile.SpooledTemporaryFile" class="external-link" target="_blank">`SpooledTemporaryFile`</a> (一个<a href="https://docs.python.org/3/glossary.html#term-file-like-object" class="external-link" target="_blank">file-like</a> 对象）。 这是实际的Python文件，您可以将其直接传递给需要“类文件”对象的其他函数或库。

`UploadFile`具有以下 `async` 方法。它们都调用下面的相应文件方法（使用内部的 `SpooledTemporaryFile`）。

* `write(data)`：将`data`（`str`或`bytes`）写入文件。
* `read(size)`：读取文件的`size`（`int`）字节/字符。
* `seek(offset)`：转到文件中的字节位置`offset`（`int`）。
    *例如， `await myfile.seek(0)` 将转到文件的开头。
    *如果您一次运行 `await myfile.read()` 然后需要再次读取内容，这将特别有用。
* `close()`：关闭文件。

由于所有这些方法都是 `async` 方法，因此您需要“等待”它们。

例如，在 `async` *path operation function（路径操作函数）* 内部，您可以使用以下命令获取内容：

```Python
contents = await myfile.read()
```

如果您在常规的`def` *path operation function（路径操作函数）*中，则可以直接访问`UploadFile.file`，例如：

```Python
contents = myfile.file.read()
```

!!! 请注意 "`async` 技术细节"
    当您使用`async`方法时，**FastAPI** 在线程池中运行文件方法并等待它们。

!!! 请注意 "Starlette 技术细节"
    **FastAPI** 的 `UploadFile` 直接继承自 **Starlette** 的 `UploadFile` ，但添加了一些必要的部分以使其与 **Pydantic** 和FastAPI的其他部分兼容。

## 什么是“表单数据”

HTML表单（`<form> </form>`）将数据发送到服务器的方式通常对该数据使用“特殊”编码，这与JSON不同。

**FastAPI** 将确保从正确的位置而不是JSON读取数据。

!!! 请注意“技术细节”
    表单中的数据不包含文件时，通常使用“媒体类型” `application/x-www-form-urlencoded` 进行编码。

    但是当表单包含文件时，它将被编码为 `multipart/form-data` 。如果您使用`File`，**FastAPI** 将知道它必须从body的正确部位获取文件。
    
    如果您想了解有关这些编码和表单字段的更多信息，请前往 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST" class="external-link" target="_blank"><abbr title="Mozilla Developer Network">MDN</abbr> web docs for <code>POST</code></a>.

!!! 警告
    您可以在 *path operation* 中声明多个 `File` 和 `Form` 参数，但也不能声明希望以JSON形式接收的 `Body` 字段，因为请求的主体将使用 `multipart/form-data` 代替 `application/json`。

    这不是 **FastAPI** 的限制，它是HTTP协议的一部分。

## 多个文件上传

可以同时上传多个文件。

它们将与使用“表单数据”发送的同一“表单字段”相关联。

要使用它，声明一个`bytes` 的 `List` 或 `UploadFile`：

```Python hl_lines="10 15"
{!../../../docs_src/request_files/tutorial002.py!}
```

您将收到声明一个`bytes` 的 `List` 或 `UploadFile`作为声明。

!!! 注释
    请注意，自2019年4月14日起，Swagger UI不支持在同一表单字段中上传多个文件。有关更多信息，请检查 <a href="https://github.com/swagger-api/swagger-ui/issues/4276" class="external-link" target="_blank">#4276</a> 和 <a href="https://github.com/swagger-api/swagger-ui/issues/3641" class="external-link" target="_blank">#3641</a>.

    不过，**FastAPI** 已经使用标准OpenAPI与之兼容。
    
    因此，只要Swagger UI支持多文件上传或任何其他支持OpenAPI的工具，它们都将与**FastAPI**兼容。

!!! 请注意“技术细节”
    您也可以使用 `from starlette.responses import HTMLResponse`.

    **FastAPI** 提供与 `fastapi.responses` 相同的 `starlette.responses` ，只是为开发人员提供了方便。但是大多数可用的响应直接来自Starlette。

## 总结

使用 `File` 声明要上传的文件作为输入参数（作为表单数据）。
