# 响应状态码

您可以使用指定响应模型的相同方式，也可以在任何 *path operations（路径操作）* 中使用参数`status_code` 声明用于响应的HTTP状态代码：

* `@app.get()`
* `@app.post()`
* `@app.put()`
* `@app.delete()`
* 等等

```Python hl_lines="6"
{!../../../docs_src/response_status_code/tutorial001.py!}
```

!!! 注释
    注意， `status_code` 是 “装饰器”方法的参数（ `get` ， `post` 等）。 像所有参数和主体一样，不属于 *path operation function（路径操作函数）*。

`status_code` 参数接收带有HTTP状态代码的数字。

!!! 信息
    `status_code` 也可以接收一个 `IntEnum`，例如Python的 <a href="https://docs.python.org/3/library/http.html#http.HTTPStatus" class="external-link" target="_blank">`http.HTTPStatus`</a>。

It will:

* Return that status code in the response.
* Document it as such in the OpenAPI schema (and so, in the user interfaces):

<img src="/img/tutorial/response-status-code/image01.png">

!!! 注意
    一些响应代码（请参阅下一节）指示响应没有主体。

    FastAPI知道这一点，并将生成表明没有响应正文的OpenAPI文档。

## 关于HTTP状态码

!!! 注释
    如果你已经知道HTTP状态码，可以跳过下面的部分。

在HTTP中，您将发送3位数的数字状态代码作为响应的一部分。

这些状态代码具有关联的名称以识别它们，但重要的是数字。

简单来说：

* `100` 及以上代表“信息”。您很少直接使用它们。具有这些状态代码的响应不能带有主体。
* **`200`** 及更高版本用于“成功”响应。这些是您最常使用的。
    * `200` 是默认状态代码，表示一切正常。
    * 另一个例子是 `201`，“已创建”。通常在数据库中创建新记录后使用。
    * 特殊情况是 `204`，“无内容”。当没有内容返回给客户端时使用此响应，因此该响应必须没有正文。
* **`300`** 及以上版本适用于“重定向”。具有这些状态代码的响应可能带有或可能没有主体，但“ 304”（未修改）除外，该主体不能有一个主体。
* **`400`** 及更高版本适用于“客户端错误”响应。这些是您可能最常使用的第二种类型。
    * 例如 `404` ，表示“未找到”响应。
    * 对于来自客户端的一般错误，您可以只使用`400`。
* `500` 或更高版本用于服务器错误。您几乎永远不会直接使用它们。当您的应用程序代码或服务器中的某些部分出现问题时，它将自动返回这些状态代码之一。

!!! 提示
    要了解有关每个状态代码以及适用于什么的代码的更多信息，请检查 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status" class="external-link" target="_blank"><abbr title="Mozilla Developer Network">MDN</abbr> HTTP状态码文档</a>。

## 记住名字的捷径

让我们再次看前面的例子：

```Python hl_lines="6"
{!../../../docs_src/response_status_code/tutorial001.py!}
```

`201` 是“已创建”的状态代码。

但是您不必记住每个代码的含义。

您可以使用来自 `fastapi.status` 的便捷变量。

```Python hl_lines="1 6"
{!../../../docs_src/response_status_code/tutorial002.py!}
```

它们只是一种方便，它们具有相同的编号，但是那样一来，您可以使用编辑器的自动完成功能来找到它们：

<img src="/img/tutorial/response-status-code/image02.png">

!!! 请注意“技术细节”
    您也可以使用 `from starlette import status`。

    **FastAPI** 与 `fastapi.status` 提供相同的 `starlette.status`，只是为开发人员提供了方便。 但它直接来自Starlette。

## 更改默认值

稍后，在[高级用户指南](../advanced/response-change-status-code.md){.internal-link target=_blank}中，您将看到如何返回不同于默认状态码的状态码 在这里声明。
