# 中间件

您可以将中间件添加到**FastAPI**应用程序中。

“中间件”是一个函数，它在每个**请求**被任何特定的*路径操作*处理之前都能工作。还有在返回之前的每个**响应**。

* 它接受您的应用程序中的每个**请求**。

* 然后它可以对那个**请求**做些什么，或者运行任何需要的代码。

* 然后它传递**请求**，由应用程序的其余部分（通过某些*路径操作*）处理。

* 然后它接受应用程序生成的**响应**（通过某些*路径操作*）。
  
* 它可以对**响应**做些什么，或者运行任何需要的代码。

* 然后返回**响应**。

!!! 请注意“技术细节”
    如果您与 `yield` 有依赖关系，那么退出代码将在中间件之后运行。

    如果有任何后台任务（稍后记录），它们将在所有中间件之后运行。

## 创建一个中间件

要创建中间件，请在函数顶部使用修饰符 `@app.middleware("http")` 。

中间件功能接收：

* `request`。
* 一个函数 `call_next`，它将接收 `request` 的参数。
    * 此功能会将 `request`传递给相应的* path操作*。
    * 然后，它返回由相应的“路径操作”生成的 `response` 。
* 然后，您可以在返回之前进一步修改 `response` 。

```Python hl_lines="8 9  11  14"
{!../../../docs_src/middleware/tutorial001.py!}
```

!!! 提示
    请记住，可以添加自定义专有标题 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers" class="external-link" target="_blank">使用 'X-' 前缀</a>。

    但是，如果您希望浏览器中的客户端能够看到自定义标头，则需要将其添加到CORS配置中 ([CORS (Cross-Origin Resource Sharing)](cors.md){.internal-link target=_blank}) 使用参数 `expose_headers` 文档位于 <a href="https://www.starlette.io/middleware/#corsmiddleware" class="external-link" target="_blank">Starlette's CORS docs</a>.

!!! 请注意“技术细节”
    您也可以使用`from starlette.requests import Request`。

    **FastAPI**为开发人员提供了便利。 但它直接来自Starlette。

### 响应之前和之后

您可以在任何“路径操作”接收到它之前添加要与`request`一起运行的代码。

以及在生成 `response` 之后，在返回之前。

例如，您可以添加一个自定义标头 `X-Process-Time` ，其中包含处理请求和生成响应所花费的时间（以秒为单位）：

```Python hl_lines="10  12  13"
{!../../../docs_src/middleware/tutorial001.py!}
```

## 其他中间件

稍后，您可以在[高级用户指南：高级中间件](../advanced/middleware.md){.internal-link target=_blank}中了解更多有关其他中间件的信息。

在下一部分中，您将了解如何使用中间件处理<abbr title="跨域资源共享">CORS</abbr>。
