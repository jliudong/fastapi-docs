# 额外模型

继续前面的示例，通常会有多个相关模型。

用户模型尤其如此，因为：

*  **input model** 必须具有密码。
* **output model** 不应包含密码。
* **database model**可能需要具有哈希密码。

!!! 危险
    切勿存储用户的明文密码。 始终存储“安全哈希”，然后可以进行验证。

    如果您不知道，您将在 [安全章节](security/simple-oauth2.md#password-hashing){.internal-link target=_blank}.

## 多模型

这是关于模型的密码字段和使用位置的大致概念：

```Python hl_lines="7 9  14   20 22  27 28  31 32 33  38 39"
{!../../../docs_src/extra_models/tutorial001.py!}
```

### 关于 `**user_in.dict()`

#### Pydantic 的 `.dict()`

`user_in` 是一个 Pydantic 模型的类 `UserIn`。

Pydantic 模型有一个 `.dict()` 方法，返回一个带有模型数据的 `dict` 。

因此，如果我们创建一个Pydantic 对象 `user_in` 类似如下：

```Python
user_in = UserIn(username="john", password="secret", email="john.doe@example.com")
```

然后我们调用：

```Python
user_dict = user_in.dict()
```

现在，我们有了一个 `dict` ，其数据位于变量 `user_dict` 中（它是一个 `dict` ，而不是Pydantic模型对象）。

如果调用：

```Python
print(user_dict)
```

我们可以得到一个Python的 `dict` ：

```Python
{
    'username': 'john',
    'password': 'secret',
    'email': 'john.doe@example.com',
    'full_name': None,
}
```

#### 解包 `dict`

如果我们采用 `user_dict` 之类的 `dict` 并将其传递给带有 `**user_dict` 的函数（或类），Python将对其进行“解包”。 它将直接传递 `user_dict` 的键和值作为键值参数。

因此，继续上面的 `user_dict` ，编写：

```Python
UserInDB(**user_dict)
```

会得到以下结果：

```Python
UserInDB(
    username="john",
    password="secret",
    email="john.doe@example.com",
    full_name=None,
)
```

更确切地说，直接使用 `user_dict` ，以及将来可能包含的任何内容：

```Python
UserInDB(
    username = user_dict["username"],
    password = user_dict["password"],
    email = user_dict["email"],
    full_name = user_dict["full_name"],
)
```

#### 来自另一个内容的Pydantic模型

在上面的例子中我们从`user_in.dict()`中得到`user_dict`，代码如下：

```Python
user_dict = user_in.dict()
UserInDB(**user_dict)
```

其等价于下面这样的代码：

```Python
UserInDB(**user_in.dict())
```

...因为 `user_in.dict()` 是 `dict` ，然后我们通过将Python传递给以 `**` 开头的 `UserInDB` 来使它“解包”。

因此，我们从另一个Pydantic模型中的数据中获得了Pydantic模型。

#### 解包 `dict` 和其他关键字

然后添加额外的关键字参数 `hashed_password=hashed_password`，例如：

```Python
UserInDB(**user_in.dict(), hashed_password=hashed_password)
```

...最终像：

```Python
UserInDB(
    username = user_dict["username"],
    password = user_dict["password"],
    email = user_dict["email"],
    full_name = user_dict["full_name"],
    hashed_password = hashed_password,
)
```

!!! 警告
    支持的附加功能只是为了演示可能的数据流，但是它们当然不能提供任何真正的安全性。

## 减少重复

减少重复代码是 **FastAPI** 的核心思想之一。

随着代码重复的增加，错误，安全性问题，代码不同步问题（当您在一个地方进行更新而不是在另一个地方进行更新）等问题的机会也将增加。

这些模型都共享大量数据，并复制属性名称和类型。

我们可以做得更好。

我们可以声明一个 `UserBase` 模型作为其他模型的基础。 然后，我们可以使该模型的子类继承其属性（类型声明，验证等）。

所有数据转换，验证，文档编制等仍将正常运行。

这样，我们可以只声明模型之间的差异（使用明文 `password` ，`hashed_password` 和不使用密码）：

```Python hl_lines="7  13 14  17 18  21 22"
{!../../../docs_src/extra_models/tutorial002.py!}
```

## `Union` 或 `anyOf`

您可以将响应声明为两种类型的 `Union` ，这意味着该响应将是两种类型中的任何一种。

将在OpenAPI中使用 `anyOf` 进行定义。

为此，请使用标准的Python类型提示 <a href="https://docs.python.org/3/library/typing.html#typing.Union" class="external-link" target="_blank">`typing.Union`</a>：

```Python hl_lines="1 14 15 18 19 20 33"
{!../../../docs_src/extra_models/tutorial003.py!}
```

## 模型列表

同样，您可以声明对象列表的响应。

为此，请使用标准的Python语言 `typing.List`：

```Python hl_lines="1 20"
{!../../../docs_src/extra_models/tutorial004.py!}
```

## 用任意 `dict` 响应

您还可以使用简单的任意 `dict` 声明响应，仅声明键和值的类型，而无需使用Pydantic模型。

如果您事先不知道有效的字段/属性名称（Pydantic模型需要此名称），这将很有用。

在这种情况下，您可以使用 `typing.Dict` ：

```Python hl_lines="1 8"
{!../../../docs_src/extra_models/tutorial005.py!}
```

## 总结

使用多个Pydantic模型，并针对每种情况自由继承。

如果每个实体必须能够具有不同的“状态”，则无需为每个实体拥有单个数据模型。 以用户“实体”为例，其状态包括 `password`, `password_hash` 和没有密码。
