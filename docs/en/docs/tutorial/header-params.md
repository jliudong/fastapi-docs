# Header 参数

您可以使用定义`Query`, `Path` 和 `Cookie`参数的方法来定义Header参数。

## 导入 `Header`

首先导入 `Header`:

```Python hl_lines="1"
{!../../../docs_src/header_params/tutorial001.py!}
```

## 声明 `Header` 参数

然后使用与`Path`，`Query`和`Cookie`相同的结构声明Header参数。

第一个值是默认值，您可以传递所有其他验证或注释参数：

```Python hl_lines="7"
{!../../../docs_src/header_params/tutorial001.py!}
```

!!! 请注意“技术细节”
    `Header` 是 `Path`，`Query`和`Cookie`的“姐妹”类。 它也继承自相同的通用 `Param` 类。

    但是请记住，当您从 `fastapi` 中导入 `Path`，`Query`，`Cookie`和其他时，这些实际上是返回特殊类的函数。

!!! 信息
     要声明header，您需要使用 `Header` ，否则参数将被解释为查询参数。

## 自动转换

`Header` 在`Path`，`Query`和`Cookie`的基础上具有一些额外的功能。

大多数标准标头由“连字符”符号（也称为“减号”）的（`-`）分隔。

但是像 `user-agent` 这样的变量在Python中无效。

因此，默认情况下，`Header` 会将参数名称字符从下划线（`_`）转换为连字符（`-`），以提取并记录header。

另外，HTTP标头不区分大小写，因此，您可以使用标准Python样式（也称为“snake_case”）声明它们。

因此，您可以像在Python代码中一样正常使用`user_agent`，而无须将首字母大写为`User_Agent`或类似名称。

如果出于某种原因需要禁用下划线自动转换为连字符，请将`Header`的参数 `convert_underscores`设置为`False`

```Python hl_lines="7"
{!../../../docs_src/header_params/tutorial002.py!}
```

!!! 警告
    在将 `convert_underscores` 设置为 `False` 之前，请记住，某些HTTP代理和服务器禁止使用带下划线的标头。


## 重复header

可能会收到重复的标题。也就是说，同一header具有多个值。

您可以使用类型声明中的列表来定义这些情况。

您将从重复header中以Python `list` 接收所有值。

例如，要声明可以多次出现的 `X-Token` 标头，可以编写：

```Python hl_lines="9"
{!../../../docs_src/header_params/tutorial003.py!}
```

如果您与 *path operation* 通信，则发送两个HTTP标头，例如：

```
X-Token: foo
X-Token: bar
```

响应将类似下面这样：

```JSON
{
    "X-Token values": [
        "bar",
        "foo"
    ]
}
```

## 总结

使用与`Path`，`Query`和`Cookie`相同的通用模式，用 `Header` 声明header。

不用担心变量中的下划线，**FastAPI** 将负责转换它们。
