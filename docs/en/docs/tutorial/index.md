# 教程 - 用户指南 - 介绍

本教程将引导您一步一步学习使用 **FastAPI** 的所有特性。

每个章节都是在前面章节的基础上一步一步构建的，但是每一章节的结构仍然是单独的主题，因此可以直接转到任何特定的章节主题来详细了解特定的API需求。

本教程也可以用于作为参考手册使用。因此就可以随时回看，用于精确解决API查询。

## 运行代码

本教程中所有的代码都可以拷贝并直接使用（它们实际上是经过测试的Python文件）。

执行示例代码时，可以将代码拷贝到 `main.py` 文件中，并通过 `uvicorn` 运行，类似效果如下:

<div class="termy">

```console
$ uvicorn main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
<span style="color: green;">INFO</span>:     Started reloader process [28720]
<span style="color: green;">INFO</span>:     Started server process [28722]
<span style="color: green;">INFO</span>:     Waiting for application startup.
<span style="color: green;">INFO</span>:     Application startup complete.
```

</div>

在您实际学习过程中，**强烈鼓励** 您自己动手编写或者是拷贝代码到本地环境中实际运行。

在您的编辑器中实际编写代码时您就能发现FastAPI的真正优势，可以发现只需要编写少量代码、类型检查、代码自动补全等等。

---

## 安装 FastAPI

安装 FastAPI 的第一步。

对于本教程，您可能需要安装所有可选的依赖项和功能：

<div class="termy">

```console
$ pip install fastapi[all]

---> 100%
```

</div>

...当然还包括 `uvicorn`，这样启动服务来实际运行程序。

!!! 注意
    您也可以分步来安装。

    要将应用程序部署到生产环境需要执行以下操作：

    ```
    pip install fastapi
    ```

    还需要安装 `uvicorn` 到生产环境：

    ```
    pip install uvicorn
    ```

    对于您要使用的每个可选依赖项都是一样的。

## 进阶用户指南

在您学习完 **教程 - 用户指南** 之后，还可以继续学习 **进阶用户指南**。

**进阶用户指南** 是以本教程为基础，采用相同的理念，介绍一些扩展特性。

但是您最好还是先学习 **教程 - 用户指南** （您现在正在阅读的内容）。

整体来看， **教程 - 用户指南** 的设计目标是让学员在完成学习后可以构建完整的应用程序。在完成本教程的学习后，可以通过学习 **进阶用户指南** 获得一些额外的编程思路。
