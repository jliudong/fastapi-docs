# 大型应用 - 多文件

如果要构建应用程序或Web API，则很少将所有内容都放在一个文件中。

**FastAPI**提供了一种便捷工具，可在保持所有灵活性的同时构建应用程序。

!!! 信息
    如果您来自Flask，那将相当于Flask的蓝图。

## 文件结构示例

假设您的文件结构如下：

```
.
├── app
│   ├── __init__.py
│   ├── main.py
│   └── routers
│       ├── __init__.py
│       ├── items.py
│       └── users.py
```

!!! 提示
    有两个 `__init__.py` 文件：每个目录或子目录中的一个。

    这就是允许将代码从一个文件导入另一个文件的原因。

    例如，在 `app/main.py` 中，您可以有如下一行：

    ```
    from app.routers import items
    ```

* `app` 目录包含所有内容。
* 这个 `app` 目录有一个空文件 `app/__init__.py` 。
    * 因此，`app` 目录是Python软件包（“Python模块”的集合）。
* `app` 目录中还有一个 `app/main.py` 文件。
    * 由于它位于Python软件包目录中（因为存在文件 `__init__.py`），因此它是该软件包的“模块”：`app.main`。
* 有一个子目录 `app/routers/` 。
* 子目录 `app/routers` 中还有一个空文件 `__init__.py`。
    * 因此，它是一个“ Python子包”。
* 文件 `app/routers/items.py` 位于 `app/routers/__init__.py` 旁边。
    * 因此，它是一个子模块：`app.routers.items`。
* 文件 `app/routers/users.py` 位于 `app/routers/__init__.py` 旁边。
    * 因此，它是一个子模块：`app.routers.users`。

## `APIRouter`

假设专门用于处理用户的文件是 `/app/routers/users.py` 中的子模块。

您希望将与用户相关的 *path operations* 与其余代码分开，以使其井井有条。

但它仍然是相同的 **FastAPI** application/web API 的一部分（它是同一“ Python包”的一部分）。

您可以使用“APIRouter”为该模块创建 *path operations*。

### 导入 `APIRouter`

您可以导入它并以与 `FastAPI` 类相同的方式创建一个“实例”：

```Python hl_lines="1 3"
{!../../../docs_src/bigger_applications/app/routers/users.py!}
```

### 带有 `APIRouter` 的 *Path operations* 

然后，您可以使用它来声明 *path operations* 。

以与使用 `FastAPI` 类相同的方式使用它：

```Python hl_lines="6 11 16"
{!../../../docs_src/bigger_applications/app/routers/users.py!}
```

您可以将 `APIRouter` 视为“迷你 `FastAPI` ”类。

支持所有相同的选项。

所有相同的参数，响应，依赖性，标签等。

!!! 提示
    在此示例中，该变量称为 `router` ，但您可以根据需要命名。

我们将在主 `FastAPI` 应用中包含该 `APIrouter` ，但首先，让我们添加另一个 `APIrouter` 。

## 带有 `APIRouter` 的另一个模块

假设您在模块 `app/routers/items.py` 中还具有专用于处理应用程序中“项目”的端点。

您具有 *path operations* ：

* `/items/`
* `/items/{item_id}`

与 `app/routers/users.py` 的结构相同。

但是，可以说这一次我们比较懒。

而且，我们不需要在每个 *path operation* 中显式地键入 `/items/` 和 `tags=["items"]`（稍后我们将可以这样做）：

```Python hl_lines="6 11"
{!../../../docs_src/bigger_applications/app/routers/items.py!}
```

### 添加一些自定义的 `tags`, `responses`, 和 `dependencies`

我们没有添加前缀 `/items/` 或 `tags=["items"]` 以便以后添加它们。

但是我们可以添加自定义的 `tags` 和 `responses` ，将其应用于特定的 *path operation*：

```Python hl_lines="18 19"
{!../../../docs_src/bigger_applications/app/routers/items.py!}
```

## 主要的 `FastAPI`

现在，让我们在 `app/main.py` 看到模块。

在这里导入和使用类 `FastAPI` 。

这将是应用程序中将所有内容捆绑在一起的主文件。

### 导入 `FastAPI`

导入并创建一个 `FastAPI` 类：

```Python hl_lines="1 5"
{!../../../docs_src/bigger_applications/app/main.py!}
```

### 导入 `APIRouter`

但是这一次我们不是直接在 `FastAPI` `app` 中添加 *path operations* 。

我们导入其他具有 `APIRouter` 的子模块：

```Python hl_lines="3"
{!../../../docs_src/bigger_applications/app/main.py!}
```

由于文件 `app/routers/items.py` 是同一Python包的一部分，因此我们可以使用“点符号”将其导入。

### 导入方式

这部分：

```Python
from .routers import items, users
```

意思是：

* 从与此模块（文件 `app/main.py`）所在的相同包（目录 `app/`）开始...
* 寻找子包 `routers`（目录位于 `app/routers/`）...
* 然后从其中导入子模块 `items`（位于 `app/routers/items.py` 的文件）和`users`（位于 `app/routers/users.py` 的文件）...

模块 `items` 将具有一个变量 `router`（`items.router`）。 这与我们在文件 `app/routers/items.py` 中创建的相同。 这是一个 `APIRouter` 。 用户模块也一样。

我们也可以像这样导入它们：

```Python
from app.routers import items, users
```

!!! 信息
    第一个版本是“相对导入”。

    第二个版本是“绝对导入”。

    要了解有关Python包和模块的更多信息，请阅读 <a href="https://docs.python.org/3/tutorial/modules.html" class="external-link" target="_blank"> Python官方文档 关于模块</a>。

### 避免名称冲突

我们将直接导入子模块 `items` ，而不是仅导入其变量 `router` 。

这是因为我们在子模块 `users` 中还有另一个名为 `router` 的变量。

如果我们一个接一个地导入，例如：

```Python
from .routers.items import router
from .routers.users import router
```

来自 `users` 的 `router` 将覆盖 `items` 中的 `router` ，我们将无法同时使用它们。

因此，为了能够在同一个文件中使用它们，我们直接导入子模块：

```Python hl_lines="3"
{!../../../docs_src/bigger_applications/app/main.py!}
```

### 包含一个 `APIRouter`

现在，让我们包括来自用户模块 `users` 的 `router` ：

```Python hl_lines="13"
{!../../../docs_src/bigger_applications/app/main.py!}
```

!!! 信息
    `users.router` 包含在 `app/routers/users.py` 文件中的 `APIRouter` 。

使用 `app.include_router()` ，我们可以向主 `FastAPI` 应用程序中添加 `APIRouter` 。

它将包括来自该路由器的所有路由作为其一部分。

!!! 请注意“技术细节”
    实际上，它将为内部在 `APIRouter` 中声明的每个路径操作创建一个路径操作。

    因此，在幕后，它实际上将像一切都是同一单个应用程序一样工作。

!!! 检查
    包括路由器时，您不必担心性能。

    这将需要几微秒，并且只会在启动时发生。

    因此，它不会影响性能。

### 包含一个 `APIRouter` 带有 `prefix`, `tags`, `responses`, and `dependencies`

现在，让我们包括来自 `items` 子模块的路由器。

但是，请记住我们很懒，没有为所有 *path operations* 添加 `/items/` 或 `tags` 吗？

我们可以使用 `app.include_router()` 的参数 `prefix` 为所有路径操作添加前缀。

由于每个 *path operations* 的路径都必须以`/`开头，例如：

```Python hl_lines="1"
@router.get("/{item_id}")
async def read_item(item_id: str):
    ...
```

...前缀不得包含最后的 `/` 。

因此，这种情况下的前缀为 `/items` 。

我们还可以添加一个 `tags` 列表，该列表将应用于此路由器中包括的所有 *path operations* 。

我们还可以添加预定义的 `responses` ，这些响应也将包含在所有 *path operations* 中。

我们可以添加一个 `dependencies` 列表，该列表将被添加到路由器中的所有 *path operations* 中，并将针对对它们的每个请求执行/解决。 请注意，就像 *path operation decorators* 中的依赖项一样，任何值都不会传递给 *path operation function* 。

```Python hl_lines="8 9 10 14 15 16 17 18 19 20"
{!../../../docs_src/bigger_applications/app/main.py!}
```

最终结果是项目路径现在为：

* `/items/`
* `/items/{item_id}`

...按照我们的意图。

* 它们将被标记为包含单个字符串 `"items"` 的标签列表。
* 声明了 `"custom"` 标签的 *path operation* 将同时具有标签 `items` 和 `custom`。
    * 这些“标签”对于自动交互式文档系统（使用OpenAPI）特别有用。
* 所有这些都将包括预定义的 `responses`。
* 声明了自定义 `403` 响应的 *path operation* 将同时具有预定义的响应（ `404` ）和直接声明的 `403` 。
* 所有这些 *path operations* 将在它们之前评估/执行的 `dependencies` 列表。
    * 如果您还在特定的 *path operation* 中声明依赖项，**它们也将被执行**。
    * 首先执行路由器依赖性，然后执行 [装饰器中的 `dependencies`](dependencies/dependencies-in-path-operation-decorators.md){.internal-link target=_blank}，然后是常规参数依赖性。
    * 您还可以通过[`Security` dependencies with `scopes`](../advanced/security/oauth2-scopes.md){.internal-link target=_blank}。

!!! 提示
    例如，可以使用在装饰器中具有 `dependencies` 来要求对整个 *path operations* 组进行身份验证。 即使没有将依赖项单独添加到每个依赖项中。

!!! 检查
    `prefix`, `tags`, `responses` 和 `dependencies` 参数（在许多其他情况下）仅是 **FastAPI** 中的一项功能，可帮助您避免代码重复。

!!! 提示
     您也可以直接添加路径操作，例如，使用 `@app.get(...)` 。

    在同一个 **FastAPI** 应用中，除了 `app.include_router()`。

    它仍然可以正常工作。

!!! 信息“非常详细的技术细节”
    **注意**：这是一个非常技术性的细节，您可能可以**跳过**。

     ---

    `APIRouter` 没有被“挂载”，它们没有与应用程序的其余部分隔离。

    这是因为我们要在OpenAPI架构和用户界面中包括它们的 *path operations*。

    由于我们不能仅仅隔离它们并独立于其余部分来“装载”它们，因此 *path operations* 是“克隆的”（重新创建），而不是直接包含在内。

## 检查自动API文档

现在，使用模块 `app.main` 和变量 `app` 运行 `uvicorn`：

<div class="termy">

```console
$ uvicorn app.main:app --reload

<span style="color: green;">INFO</span>:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
```

</div>

并在<a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank"> http://127.0.0.1:8000/docs </a>上打开文档 。

您将看到使用正确的路径（和前缀）和正确的标记的自动API文档，包括来自所有子模块的路径：

<img src="/img/tutorial/bigger-applications/image01.png">

## 多次使用不同的前缀添加同一路由器

您也可以将 *same* 路由器使用不同的前缀多次使用 `.include_router()` 。

例如，在不同的前缀下公开相同的API，例如，`/api/v1` 和 `/api/latest`。

这是您可能真正不需要的高级用法，但是如果您有需要，可以使用。
