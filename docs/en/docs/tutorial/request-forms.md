# 表单数据

当您需要接收表单字段而不是JSON时，可以使用 `Form` 。

!!! 信息
    要使用表单需要先安装 <a href="https://andrew-d.github.io/python-multipart/" class="external-link" target="_blank">`python-multipart`</a>.

    例如 `pip install python-multipart`.

## 导入 `Form`

从 `fastapi` 导入 `Form`:

```Python hl_lines="1"
{!../../../docs_src/request_forms/tutorial001.py!}
```

## 定义 `Form` 参数

创建表单参数的方法与使用 `Body` 或 `Query` 相同:

```Python hl_lines="7"
{!../../../docs_src/request_forms/tutorial001.py!}
```

例如，以一种可以使用OAuth2规范的方式（称为“密码流”），要求发送 `username` 和 `password` 作为表单字段。

<abbr title="specification">规范</abbr> 要求字段必须准确命名为 `username` 和 `password` ，并作为表单字段（而不是JSON）发送。

使用 `Form` 可以声明与 `Body` 相同的元数据和验证（以及`Query`, `Path`, `Cookie`）。

!!! 信息
    `Form` 是直接继承自 `Body` 的类。

!!! 提示
    要声明表单主体，您需要显式使用 `Form` ，因为没有它，参数将被解释为查询参数或主体（JSON）参数。

## 关于 "表单字段"

HTML表单（`<form> </form>`）将数据发送到服务器的方式通常对该数据使用“特殊”编码，这与JSON不同。

**FastAPI** 将确保从正确的位置而不是JSON读取数据。

!!! 请注意“技术细节”
    来自表单的数据通常使用“媒体类型” `application/x-www-form-urlencoded` 进行编码。

    但是当表单包含文件时，它将被编码为 `multipart/form-data` 。 您将在下一章中了解有关处理文件的信息。
    
    可以通过下面的链接了解更多关于表单字段和编码的信息 <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST" class="external-link" target="_blank"><abbr title="Mozilla Developer Network">MDN</abbr> web docs for <code>POST</code></a>.

!!! 警告
    您可以在* path操作*中声明多个 `Form` 参数，但也不能声明希望以JSON形式接收的 `Body` 字段，因为请求将使用`application/x-www-form-urlencoded`，而不是`application/json`。

    这不是**FastAPI**的限制，它是HTTP协议的一部分。

## 总结

使用`Form`声明表单数据输入参数。
